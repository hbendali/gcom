<?php

// Include FireWorks lib
require_once('fw.php');

$username = isset($_POST['username'])? sql_inj($_POST['username']) : null;
$password = isset($_POST['password'])? base64_encode(sql_inj($_POST['password'])) : null;

if($username != null AND $password != null)
{

  $result = $fw->fetchAll("SELECT * FROM utilisateur WHERE username='$username' AND password='$password' AND acl like '%\"enable\":true%'");


  if (count($result)>0)
  {
    $result = $result[0];
    $result->acl = json_decode($result->acl); 
    unset($result->password);
    //$result->acl = explode(',', $result->acl);
    $_SESSION['user'] = $result;
    //echo '<pre>';print_r($_SESSION);
    //exit;
    header('Location:../');
    exit;
  }else{
    //echo "SELECT * FROM utilisateur WHERE username='$username' AND password='$password' AND acl like '%\"enable\":true%'";

    //print_r($result);
    //exit;
  }
}

?>
<!DOCTYPE HTML>
<html lang="fr-FR" dir="ltr">
<head>
  <title>COSIDER/CONSTRUCTION</title>

  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- CSS -->
  <link rel="shortcut icon" href="../img/cosider.ico">
  <link rel="stylesheet"    href="../css/semantic-ui/semantic.min.css">
  <link rel="stylesheet"    href="../css/default.css">
  
  <!-- JS -->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/angular/angular.min.js"></script>
  <script src="../js/angular/angular-sanitize.min.js"></script>
  <!--   <script src="../js/app.js"></script> -->
  <script src="../css/semantic-ui/semantic.min.js"></script>


  <style type="text/css">
    body {
      background-color: #fff;
      background-image: url("../img/wallpaper<?=rand(1,12); //date('g'); //rand(0,17);?>.jpg");
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-size: cover;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .image.ico {
      -webkit-transform: rotate(-20deg);
      -moz-transform: rotate(-20deg);
      -ms-transform: rotate(-20deg);
      -o-transform: rotate(-20deg);
      transform: rotate(-20deg);
    }
    .column {
      max-width: 350px;
    }
  </style>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            email: {
              identifier  : 'username',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your username'
                },
                {
                  type   : 'length[1]',
                  prompt : 'Please enter a valid username'
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your password'
                },
                {
                  type   : 'length[1]',
                  prompt : 'Your password must be at least 4 characters'
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>

</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui inverted image header">
      <img src="../img/g.svg" class="image ico">
      <div class="content">
        <h2 class="ui header">COSIDER CONSTRUCTION</h2>
      </div>
    </h2>
    <form class="ui large form" method="POST" action="">
      <div class="ui piled segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="username" placeholder="Utilisateur" class="uppercase" tabindex="1">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Mot de pass" tabindex="2">
          </div>
        </div>

        <!--div class="ui fluid large red submit button">Connexion</div-->

        <button class="ui fluid large labeled submit button" type="submit" tabindex="3">
          <div class="ui red button">
            <i class="cloud icon"></i> 
          </div>
          <a class="ui fluid large basic red left pointing label">
            CONNEXION
          </a>
        </button>
      </div>

      <div class="ui error message"></div>

    </form>

    <!--div class="ui message">
      New to us? <a href="#">Sign Up</a>
    </div-->
  </div>
</div>

<div style="
  background-color: #fff9;
  padding: 10px 40px;
  position: fixed;
  bottom: 30px;
  left: 0;
  right: 0;
">
  <h1>
    LOGICIEL DE CALCUL DE PRIX ET ESTIMATION DES OFFRES
  </h1>
</div>
    
  </body>
</html>