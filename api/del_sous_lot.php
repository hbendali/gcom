<?php
session_start();

$Id_sous_lot = intval($_POST['Id_sous_lot']);

$msg=null;
 
if(isset($Id_sous_lot) AND $Id_sous_lot > 0)
{
  include '../modele/conne.php';

  $req = "DELETE FROM sous_lot WHERE Id_sous_lot=$Id_sous_lot";
  
  $reponse = $bdd->prepare($req);
  $donnee=$reponse->execute();
  if($donnee)
  {
    $msg['msg']='ok';
  }
  else
  {
    $msg['msg']='Erreur non definie';
    $msg['sql']=$req;
    $msg['debug']=$reponse->errorInfo();
  }
}
else
{
  $msg['msg']='Erreur non definie';
  $msg['debug']=$_POST;
}

echo json_encode($msg);