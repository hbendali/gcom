<?php
//print_r($_SESSION['user']);
//exit;
$user_id = $_SESSION["user"]->id_user;
$r   = new \stdClass;
$frm = $fw->get_json(true);

// MAIN DOEUVRE //////////////////////////////////////////////////////////////////////////////
if($_GET['update'] == 'main_doeuvre')
{
  $titel        = sql_inj($frm->titel);
  $code_poste   = sql_inj($frm->code_poste);
  $libelle      = sql_inj($frm->libelle);
  $cout_horaire = floatval(sql_inj($frm->cout_horaire));

  if ($code_poste && $libelle && $cout_horaire)
  {
    $code_poste = strtoupper($code_poste);
    $r->res = 'done!';
    if ($titel == "Ajouter") // insert
    {
      $r->pdo = $fw->fetchAll("INSERT INTO main_doeuvre (code_poste, libelle, cout_horaire) VALUES ('$code_poste', '$libelle', $cout_horaire)",true,true);
    }
    else // update
    {
      $r->pdo = $fw->fetchAll("UPDATE main_doeuvre SET libelle='$libelle', cout_horaire='$cout_horaire' WHERE code_poste='$code_poste'",true,true);

      // UPDATE ALL TACHE WITH NEW VALUE /////////////////////////////////////////////////////
      include 'tache_recalc.php';
      tache_recalc('mo', $code_poste, true);

    }
  }else{
    $r->res = 'Verifier CODE POSTE, LIBELLE POSTE, COUT HORAIRE';
    $r->dbg = $frm;
  }
}

// MATERIELS /////////////////////////////////////////////////////////////////////////////////
else if($_GET['update'] == 'materiel')
{
  $titel        = sql_inj($frm->titel);
  $code_mat     = sql_inj($frm->code_mat);
  $libelle      = sql_inj($frm->libelle);
  $cout_horaire = floatval(sql_inj($frm->cout_horaire));
  $valeur       = floatval(sql_inj($frm->valeur));
  $frais_g      = floatval(sql_inj($frm->frais_g));
  $longevite    = floatval(sql_inj($frm->longevite));
  $interet      = floatval(sql_inj($frm->interet));
  $frais_e      = floatval(sql_inj($frm->frais_e));
  $duree_t      = floatval(sql_inj($frm->duree_t));

  if ($code_mat && $libelle && $cout_horaire)
  {
    $code_mat = strtoupper($code_mat);
    $r->res = 'done!';
    if ($titel == "Ajouter") // insert
    {
      $r->pdo = $fw->fetchAll("INSERT INTO materiel (code_mat, libelle, cout_horaire, valeur, frais_g, longevite, interet, frais_e, duree_t ) VALUES ('$code_mat', '$libelle', $cout_horaire, $valeur, $frais_g, $longevite, $interet, $frais_e, $duree_t)",true,true);
    }
    else // update
    {
      $r->pdo = $fw->fetchAll("UPDATE materiel SET libelle = '$libelle', cout_horaire = $cout_horaire, valeur = $valeur, frais_g = $frais_g, longevite = $longevite, interet = $interet, frais_e = $frais_e, duree_t = $duree_t WHERE code_mat='$code_mat'",true,true);

      // UPDATE ALL TACHE WITH NEW VALUE /////////////////////////////////////////////////////
      include 'tache_recalc.php';
      tache_recalc('mat', $code_mat, true);
    }
  }else{
    $r->res = 'Verifier CODE MATERIEL, LIBELLE MATERIEL, COUT HORAIRE';
    $r->dbg = $frm;
  }
}


// FOURNITURE /////////////////////////////////////////////////////////////////////////////////
else if($_GET['update'] == 'fourniture')
{
  $titel           = sql_inj($frm->titel);
  $code_fourniture = sql_inj($frm->code_fourniture);
  $libelle         = sql_inj($frm->libelle);
  $cout_revient    = floatval(sql_inj($frm->cout_revient));
  $origine         = sql_inj($frm->origine,'');
  $fam             = sql_inj($frm->fam);
  $fam             = ($fam!="") ? "'$fam'" : "NULL";
  $um              = sql_inj($frm->um);
  $um              = ($um!="")  ? "'$um'"  : "NULL";

  $prix_achat      = floatval(sql_inj($frm->prix_achat));
  $transport       = floatval(sql_inj($frm->transport));
  $dechargement    = floatval(sql_inj($frm->dechargement));
  $perte           = floatval(sql_inj($frm->perte));

  if ($code_fourniture && $libelle && $cout_revient)
  {
    $code_fourniture = strtoupper($code_fourniture);
    $r->res = 'done!';
    if ($titel == "Ajouter") // insert
    {
      $r->pdo = $fw->fetchAll("INSERT INTO fourniture ( code_fourniture, libelle, cout_revient, origine, fam, um, prix_achat, transport, dechargement, perte ) VALUES ( '$code_fourniture', '$libelle', $cout_revient, '$origine', $fam, $um, $prix_achat, $transport, $dechargement, $perte )",true,true);
    }
    else // update
    {
      $r->pdo = $fw->fetchAll("UPDATE fourniture SET libelle = '$libelle', cout_revient = $cout_revient, origine = '$origine', fam = $fam, um = $um, prix_achat = $prix_achat, transport = $transport, dechargement = $dechargement, perte = $perte WHERE code_fourniture = '$code_fourniture'",true,true);

      // UPDATE ALL TACHE WITH NEW VALUE /////////////////////////////////////////////////////
      include 'tache_recalc.php';
      tache_recalc('four', $code_fourniture, true);
    }
  }else{
    $r->res = 'Verifier CODE FOURNITURE, LIBELLE FOURNITURE, COUT DE REVIENT';
    $r->dbg = $frm;
  }
}


// SOUS LOT /////////////////////////////////////////////////////////////////////////////////
else if($_GET['update'] == 'sous_lot')
{
  $titel        = sql_inj($frm->titel);
  $num_lot      = sql_inj($frm->num_lot);
  $num_sous_lot = sql_inj($frm->num_slot);
  $nom_sous_lot = sql_inj($frm->nom_slot);

  if ($num_lot && $num_sous_lot && $nom_sous_lot)
  {
    $num_sous_lot = strtoupper($num_sous_lot);
    $r->res = 'done!';
    if ($titel == "Ajouter") // insert
    {
      $r->pdo = $fw->fetchAll("INSERT INTO sous_lot ( num_lot, num_slot, nom_slot ) VALUES ( '$num_lot', '$num_sous_lot', '$nom_sous_lot' )",true,true);
    }
    else // update
    {
      $r->pdo = $fw->fetchAll("UPDATE sous_lot SET nom_slot = '$nom_sous_lot' WHERE num_slot = '$num_sous_lot'",true,true);
    }
  }else{
    $r->res = 'Verifier CODE SOUS LOT, LIBELLE SOUS LOT';
    $r->dbg = $frm;
  }
}

// CLIENT /////////////////////////////////////////////////////////////////////////////////
else if($_GET['update'] == 'client')
{
  $titel        = sql_inj($frm->titel);
  $num_client   = sql_inj($frm->num_client);
  $nom_client   = sql_inj($frm->nom_client);
  $responsable  = sql_inj($frm->responsable);
  $tel          = sql_inj($frm->tel);
  $email        = sql_inj($frm->email);
  $adresse      = sql_inj($frm->adresse);

  if ($num_client && $nom_client)
  {
    $num_client = strtoupper($num_client);
    $r->res = 'done!';
    if ($titel == "Ajouter") // insert
    {
      $r->pdo = $fw->fetchAll("INSERT INTO client ( num_client, nom_client, responsable, tel, email, adresse ) VALUES ( '$num_client', '$nom_client', '$responsable', '$tel', '$email', '$adresse'  )",true,true);
    }
    else // update
    {
      $r->pdo = $fw->fetchAll("UPDATE client SET nom_client = '$nom_client', responsable = '$responsable', tel = '$tel', email = '$email', adresse = '$adresse' WHERE num_client = '$num_client'",true,true);
    }
  }else{
    $r->res = 'Verifier CODE CLIENT, NOM CLIENT';
    $r->dbg = $frm;
  }
}

// DEVIS Etape 0 ////////////////////////////////////////////////////////////////////////
else if($_GET['update'] == 'step0')
{
  //print_r($frm);
  $query        = sql_inj($frm->query);
  $num_devis    = strtoupper(sql_inj($frm->num_devis));
  $nom_devis    = sql_inj($frm->nom_devis);
  $obj_devis    = sql_inj($frm->obj_devis);
  $num_client   = sql_inj($frm->num_client);
  $adresse      = sql_inj($frm->adresse);
  $mode_pass    = sql_inj($frm->mode_pass);
  //$nature_prj   = sql_inj($frm->nature_prj);
  $nature_prj   = $fw->getGroup($user_id);

  $r->res = 'done!';
  if ($num_devis && $nom_devis && $query == "insert"){

    $r->pdo = $fw->fetchAll("INSERT INTO projet ( num_devis, nom_devis, obj_devis, num_client, adresse, mode_pass, utilisateur, etat, nature_prj ) VALUES ( '$num_devis', '$nom_devis', '$obj_devis', '$num_client', '$adresse', '$mode_pass', $user_id, 3, '$nature_prj' )",true,true);
    if ($r->pdo['errorCode']) $r->res = 'error!';

  }else if ($num_devis && $nom_devis){

    $r->pdo = $fw->fetchAll("UPDATE projet SET nom_devis = '$nom_devis', obj_devis = '$obj_devis', num_client = '$num_client', adresse = '$adresse', mode_pass='$mode_pass', utilisateur=$user_id, etat=3 WHERE num_devis = '$num_devis'",true,true);
    if ($r->pdo['errorCode']) $r->res = 'error!';

  }else{
    $r->res = 'VERIFIER NUMERO PROJET, NOM PROJET';
    $r->dbg = $frm;
  }
}


// DEVIS Etape 1 ////////////////////////////////////////////////////////////////////////
else if($_GET['update'] == 'step1')
{
  //print_r($frm);
  //$titel                 = sql_inj($frm->titel);


  $num_devis = strtoupper( sql_inj($frm->devis->num_devis) );
  $nom_devis             = sql_inj($frm->devis->nom_devis);
  $obj_devis             = sql_inj($frm->devis->obj_devis);
  $num_client            = sql_inj($frm->devis->num_client);
  $adresse               = sql_inj($frm->devis->adresse);
  $mode_pass             = sql_inj($frm->devis->mode_pass);
  $nsub_devis            = sql_inj($frm->devis->nsub_devis);

  $num_sdevis = strtoupper(sql_inj($frm->sdevis->num_devis) );
  if (strpos($num_sdevis, '-')===false)
    $num_sdevis = $num_devis.'-'.$num_sdevis;

  $obj_sdevis            = sql_inj($frm->sdevis->obj_devis);
  $tva                   = sql_inj($frm->sdevis->tva);
  $delai_reali           = sql_inj($frm->sdevis->delai_reali);
  $nbr_logs              = sql_inj($frm->sdevis->nbr_logs,0);
  $surface_hab           = sql_inj($frm->sdevis->surface_hab,0);
  $query                 = sql_inj($frm->sdevis->query);

  $r->res = 'done!';
  if ($num_sdevis && $query == 'insert'){
    $g_user = json_encode(array($user_id));
    $r->pdo = $fw->fetchAll("INSERT INTO devis ( num_devis, nom_devis, obj_devis, num_client, adresse, delai_reali, nbr_logs, surface_hab, tva, mode_pass, utilisateur, group_utilisateur, etat ) VALUES ( '$num_sdevis', '$obj_sdevis', '$obj_sdevis', '$num_client', '$adresse', '$delai_reali', $nbr_logs, $surface_hab, '$tva', '$mode_pass', $user_id, '$g_user', 2 )",true,true);
    if ($r->pdo['errorCode']) $r->res = 'error!';

  }else if ($num_sdevis){

    $r->pdo = $fw->fetchAll("UPDATE devis SET nom_devis = '$obj_sdevis', obj_devis = '$obj_sdevis', num_client = '$num_client', adresse = '$adresse', delai_reali = '$delai_reali', nbr_logs=$nbr_logs, surface_hab=$surface_hab, tva='$tva', mode_pass='$mode_pass', utilisateur=$user_id, etat=2 WHERE num_devis = '$num_devis'",true,true);
    if ($r->pdo['errorCode']) $r->res = 'error!';

  }else{
    $r->res = 'VERIFIER NUMERO PROJET, NOM PROJET';
    $r->dbg = $frm;
  }
}

// DEVIS Etape 2 ////////////////////////////////////////////////////////////////////////
else if($_GET['update'] == 'step2' && isset($_GET['xls']) && $_GET['xls']!="")
{
  $r->res = 'error!';
  $num_devis = sql_inj($frm->sdevis->num_devis);
  if ($num_devis)
  {
    $fichier_excel = sql_inj($_GET['xls'],'');
    $r->pdo = $fw->fetchAll("UPDATE devis SET fichier_excel='$fichier_excel' WHERE num_devis = '$num_devis'",true,true);
  }
  if (isset($r->pdo['sqlState']) && $r->pdo['sqlState'] = '00000' ) $r->res = 'done!';
}
else if($_GET['update'] == 'step2')
{
  $r->res = 'error!';
  $num_devis = sql_inj($frm->sdevis->num_devis);
  if ($num_devis)
  {
    $r->pdo = $fw->fetchAll("UPDATE devis SET etat=3 WHERE num_devis = '$num_devis'",true,true);
  }
  if (isset($r->pdo['sqlState']) && $r->pdo['sqlState'] = '00000' ) $r->res = 'done!';
}

// DEVIS Etape 3 ////////////////////////////////////////////////////////////////////////
else if($_GET['update'] == 'step3')
{
  $r->res = 'done!';
  $num_devis = sql_inj($frm->num_devis);
  if ($num_devis)
  {
    $r->pdo = $fw->fetchAll("UPDATE projet SET etat=4 WHERE num_devis = '$num_devis'",true,true);
    $r->pdo = $fw->fetchAll("UPDATE devis SET etat=4 WHERE num_devis like '$num_devis-%'",true,true);
  }
  if (!$num_devis || !$r->pdo || $r->pdo['errorCode']) $r->res = 'error!';
}


// DEVIS Dernier Etape /////////////////////////////////////////////////////////////////
else if($_GET['update'] == 'ofr_win') // Gagnée
{
  $r->res = 'done!';
  $num_devis    = sql_inj($frm->num_devis);
  if ($num_devis)
  {
    $r->pdo = $fw->fetchAll("UPDATE devis SET etat=6 WHERE num_devis = '$num_devis'",true,true);
  }
  if (!$num_devis || !$r->pdo || $r->pdo['errorCode']) $r->res = 'error!';
}
else if($_GET['update'] == 'ofr_lost') // Perdue
{
  $r->res = 'done!';
  $num_devis    = sql_inj($frm->num_devis);
  if ($num_devis)
  {
    $r->pdo = $fw->fetchAll("UPDATE devis SET etat=7 WHERE num_devis = '$num_devis'",true,true);
  }
  if (!$num_devis || !$r->pdo || $r->pdo['errorCode']) $r->res = 'error!';
}


// SI NON ////////////////////////////////////////////////////////////////////////////////////
else
{
  $r->res = 'Formulaire non definie';
  $r->dbg = $frm;
}

echo json_encode($r, JSON_PRETTY_PRINT);
