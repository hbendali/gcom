<?php

$num_lot   = sql_inj($_GET['lot']);
$num_slot  = sql_inj($_GET['slot']);
$num_tache = sql_inj($_GET['tache']);

$agree     = !isallow("admin") && !isallow("programmer") && !isallow("tc") ? "AND agree='1'" : "" ;



//======================================================================================== lot
$where_lot = ($num_lot && $num_lot!="") ? "WHERE num_lot = '$num_lot'" : "";
//print_r($_GET);
//exit;
$lot = $fw->fetchAll("
  SELECT * FROM lot $where_lot
");
foreach ($lot as $line_lot=>$v1) {
  //======================================================================================== slot
  $line_num_lot = $v1->num_lot;
  $where_slot = ($num_slot && $num_slot!="") ? "AND num_slot = '$num_slot'" : "";
  $lot[$line_lot]->slot = $fw->fetchAll("
    SELECT * FROM sous_lot WHERE num_lot = '$line_num_lot' $where_slot
  ");
  foreach ($lot[$line_lot]->slot as $line_slot =>$v2){
    //======================================================================================== tache
    $line_num_slot = $v2->num_slot;
    $where_tache = ($num_tache && $num_tache!="") ? "AND num_tache = '$num_tache'" : "";
    $lot[$line_lot]->slot[$line_slot]->tache = $fw->fetchAll("
      SELECT * FROM tache WHERE num_slot = '$line_num_slot' $where_tache $agree
    ");

    foreach ($lot[$line_lot]->slot[$line_slot]->tache as $line_tache =>$v3){
      //======================================================================================== Main doeuvre
      $curent_num_tache = $v3->num_tache;
      $lot[$line_lot]->slot[$line_slot]->tache[$line_tache]->main_doeuvre = $fw->fetchAll("
        SELECT * FROM `tache_mo`,`main_doeuvre` WHERE `num_tache` = '$curent_num_tache' AND `main_doeuvre`.`code_poste` = `tache_mo`.`code_poste`
      ");
      //======================================================================================== Materiel
      $lot[$line_lot]->slot[$line_slot]->tache[$line_tache]->materiel = $fw->fetchAll("
        SELECT * FROM `tache_mat`,`materiel` WHERE `num_tache` = '$curent_num_tache' AND `materiel`.`code_mat` = `tache_mat`.`code_mat`
      ");
      //======================================================================================== Fourniture
      $lot[$line_lot]->slot[$line_slot]->tache[$line_tache]->fourniture = $fw->fetchAll("
        SELECT * FROM `tache_four`,`fourniture` WHERE `num_tache` = '$curent_num_tache' AND `fourniture`.`code_fourniture` = `tache_four`.`code_fourniture`;
      ");
      $lot[$line_lot]->slot[$line_slot]->tache[$line_tache]->compo = json_decode($lot[$line_lot]->slot[$line_slot]->tache[$line_tache]->compo);
    }
  }
}

echo json_encode($lot, JSON_PRETTY_PRINT);
