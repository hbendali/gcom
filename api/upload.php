<?php

chdir('../uploads');

$r   = new \stdClass;
$json = $fw->get_json(true);

//print_r(explode('?',$_SERVER['HTTP_REFERER'])[0]);die();

$dir = sql_inj($_GET['upload'],'tmp')."/";

if (isset($_GET['user'])){
	$dir .= $_SESSION['user']->id_user.'/';
	if (!dir($dir)){
		mkdir($dir, 0777, true);
		chmod($dir, 0777);
	}
}

if (isset($_GET['list']) && $dir!='bpu/' && $dir!='draft/' && $dir!='xls/' && $dir!='/'){
	$r = [];
	$d = dir($dir);
	while (false !== ($entry = $d->read())) {
		if ($entry != "." && $entry != ".."){
	   		
			if (isset($_GET['full'])){
				array_push($r, file_info($dir.$entry));
			}else{
				array_push($r, $dir.$entry);
			}
		}
	}
	$d->close();

}else if (isset($_FILES['file']) && $_FILES['file']['error'] == 0) {

// uploads image in the folder images
    $temp = explode(".", $_FILES["file"]["name"]);
    $newbasename = substr(md5(time()), 0, 10);
    $newfilename = $newbasename . '.' . end($temp);
    move_uploaded_file($_FILES['file']['tmp_name'], $dir . $newfilename);

// give callback to your angular code with the image src name
    /*
    $r->basename = $newbasename;
    $r->filename = $newfilename;
    $r->dir      = $dir;
    $r->fullpath = "./uploads/$dir$newfilename";
    $r->fullurl  = explode('?',$_SERVER['HTTP_REFERER'])[0]."uploads/$dir$newfilename";

	$r->file     = $dir.$newfilename;
	$r->size     = stat('../uploads/'.$dir.$newfilename)[7];
	$r->date     = date("H:i:s d/M/Y", stat('../uploads/'.$dir.$newfilename)[9]);
	$r->ext      = pathinfo($dir.$newfilename)['extension'];
	*/
	$r = file_info($dir.$newfilename);

}

echo json_encode($r, JSON_PRETTY_PRINT);



function file_info($path)
{
	$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]/gcom/uploads/";
	return [
		'file'      => $path,
		'filename'  => pathinfo($path)['filename'],
		'basename'  => pathinfo($path)['basename'],
		'extension' => pathinfo($path)['extension'],
		'size'      => stat($path)[7],
		'dir'       => pathinfo($path)['dirname'],
		'date'      => date("H:i:s d/M/Y", stat($path)[9]),
		'url'       => $url.$path,
		'fullpath'  => "./uploads/$path"

	];
}