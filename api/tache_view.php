<?php

$r   = new \stdClass;
$num_tache = sql_inj($_GET['tache_view'],null);
$AND_num_tache = $num_tache ? "AND tache.num_tache='$num_tache'" : "";
$sql = "
  SELECT
    *
  FROM 
    tache,
    sous_lot,
    lot 
  WHERE tache.num_slot = sous_lot.num_slot
  AND sous_lot.num_lot = lot.num_lot
  $AND_num_tache
";

if ($num_tache){
  $r = $fw->fetch($sql);
  $r->main_doeuvre = $fw->fetchAll("SELECT * FROM `tache_mo`,`main_doeuvre` WHERE `num_tache` = '$num_tache' AND `main_doeuvre`.`code_poste` = `tache_mo`.`code_poste`");
  $r->materiel = $fw->fetchAll("SELECT * FROM `tache_mat`,`materiel` WHERE `num_tache` = '$num_tache' AND `materiel`.`code_mat` = `tache_mat`.`code_mat`");
  $r->fourniture = $fw->fetchAll("SELECT * FROM `tache_four`,`fourniture` WHERE `num_tache` = '$num_tache' AND `fourniture`.`code_fourniture` = `tache_four`.`code_fourniture`");
  
}else{
  $r = $fw->fetchAll($sql);
  foreach ($r as $key => $value) {
    $value->main_doeuvre = $fw->fetchAll("SELECT * FROM `tache_mo`,`main_doeuvre` WHERE `num_tache` = '$value->num_tache' AND `main_doeuvre`.`code_poste` = `tache_mo`.`code_poste`");
    $value->materiel = $fw->fetchAll("SELECT * FROM `tache_mat`,`materiel` WHERE `num_tache` = '$value->num_tache' AND `materiel`.`code_mat` = `tache_mat`.`code_mat`");
    $value->fourniture = $fw->fetchAll("SELECT * FROM `tache_four`,`fourniture` WHERE `num_tache` = '$value->num_tache' AND `fourniture`.`code_fourniture` = `tache_four`.`code_fourniture`");
  }
}


echo json_encode($r, JSON_PRETTY_PRINT);