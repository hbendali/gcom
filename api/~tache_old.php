<?php
session_start();

if (!isset($_SESSION["user"]))
  exit;
else
  header('Content-Type: application/json; charset=utf-8');

include '../vue/fw.php';
include '../modele/conne.php';



//======================================================================================== lot
$req = "
SELECT
  l1.libelle_lot,
  sous1.libelle_sous_lot,       
  mo1.*,
  fourn1.*,
  equip1.*,
  deb1.debourse_sec,
  t1.*
FROM
  lot l1,sous_lot sous1,tache t1,debourse_tache deb1,equipe_ouvrier mo1,ensemble_equipement equip1,ensemble_fourniture fourn1 
WHERE
  deb1.Id_tache = t1.Id_tache
AND
  deb1.date_ajout_debourse_tache = (
    SELECT
      max(deb2.date_ajout_debourse_tache) 
    FROM
      debourse_tache deb2 
    WHERE
      deb2.Id_debourse_tache = deb1.Id_debourse_tache
    AND deb2.Id_tache = deb1.Id_tache
  )
AND
  mo1.Id_tache = t1.Id_tache
AND
  mo1.date_ajout_equipe_ouvrier = (
    SELECT
      max(e2.date_ajout_equipe_ouvrier) 
    FROM
      equipe_ouvrier e2
    WHERE
      e2.Id_equipe_mo = mo1.Id_equipe_mo
    AND
      e2.Id_tache = mo1.Id_tache
  )
AND
  fourn1.Id_tache = t1.Id_tache
AND
  fourn1.date_ajout_ensemble_fourn = (
    SELECT
      max(fourn2.date_ajout_ensemble_fourn) 
    FROM
      ensemble_fourniture fourn2
    WHERE
      fourn2.Id_tache = fourn1.Id_tache
    AND
      fourn2.Id_ensemble_fourniture = fourn1.Id_ensemble_fourniture
  )
AND
  equip1.Id_tache = t1.Id_tache
AND
  equip1.date_ajout_ensemble_equip = (
    SELECT
      max(equip2.date_ajout_ensemble_equip) 
    FROM
      ensemble_equipement equip2
    WHERE
      equip1.Id_equipe_equip = equip2.Id_equipe_equip
    AND
      equip1.Id_tache = equip2.Id_tache
  )
AND
  sous1.Num_lot = l1.Num_lot                                                    
AND
  l1.Num_lot = t1.Num_lot
AND
  l1.date_ajout_lot = (
    SELECT
      max(l2.date_ajout_lot) 
    FROM
      lot l2 
    WHERE
      l2.Num_lot = l1.Num_lot
  )
AND
  l1.supprimer_lot = 0                             
AND
  sous1.date_ajout_sous_lot = (
    SELECT
      max(s2.date_ajout_sous_lot) 
    FROM
      sous_lot s2
    WHERE
      s2.Num_sous_lot = sous1.Num_sous_lot
    AND
      s2.Num_lot = sous1.Num_lot
  )                                       
AND
  sous1.supprimer_sous_lot = 0
AND
  sous1.Num_sous_lot = t1.Num_sous_lot
AND
  t1.date_ajout_tache = (
    SELECT
      max(t2.date_ajout_tache) 
    FROM
      tache t2
    WHERE
      t2.Id_tache = t1.Id_tache
    AND
      t2.Num_tache = t1.Num_tache
    AND
      t2.Id_banque = t1.Id_banque
  )
AND
  t1.Id_banque = 1
AND
  t1.supprimer_tache = 0    
ORDER BY
  t1.Id_tache,t1.Num_tache
";

$reponse = $bdd->prepare($req);
$reponse->execute();
$tache = $reponse->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($tache);


