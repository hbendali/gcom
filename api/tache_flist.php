<?php

$AND_TACHE = sql_inj($_GET['tache_flist']);
if ($AND_TACHE) $AND_TACHE = "AND tache.num_tache='$AND_TACHE'";

$r = $fw->fetchAll("
  SELECT
    lot.num_lot, 
    sous_lot.num_slot, 
    tache.num_tache, 
    tache.nom_tache,
    tache.um, 
    tache.deb_sec,
    tache.agree
  FROM 
    tache,
    sous_lot,
    lot 
  WHERE 
    tache.num_slot = sous_lot.num_slot
  AND
    sous_lot.num_lot = lot.num_lot
  $AND_TACHE;
");

echo json_encode($r, JSON_PRETTY_PRINT);