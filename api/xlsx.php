<?php

require '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet;

$r   = new \stdClass;
$sub_devis = $fw->get_json(true);

$file = sql_inj($_GET['xlsx'],null);
$WorkSheet = sql_inj($_GET['WorkSheet'],null);
$WorkSheetName = sql_inj($_GET['WorkSheetName'],'');

if (!file_exists($file))
{
  if (file_exists(".".$file))
    $file = ".".$file;
  else
    $file = "../uploads/xls/".$file.".xlsx";
}

if (file_exists($file))
{
  if (isset($_GET['export'])){

    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
    $spreadsheet->setActiveSheetIndex($WorkSheet ? $WorkSheet : 0);

    $export_to = sql_inj($_GET['exto'],null);
    $export_to = $export_to ?  preg_replace( '/[^a-z0-9]+/', '-', strtolower( $export_to ) ) : uniqid();
    
    $Cpru = $sub_devis->xls_header->prx_u ? chr(65 + intval($sub_devis->xls_header->prx_u)) : null ;
    $Cqte = $sub_devis->xls_header->qte   ? chr(65 + intval($sub_devis->xls_header->qte))   : null ;
    $Cmnt = $sub_devis->xls_header->mnt   ? chr(65 + intval($sub_devis->xls_header->mnt))   : null ; 
    $lot  = '';
    $slot = '';
    $totalL = '';
    $totalSL = '';
    $total_realisation = '=SUM(';

    if (isset($sub_devis) && isset($sub_devis->xls))
    foreach ($sub_devis->xls as $ele) {

      if ( isset($ele->matching) && $ele->matching!='IGNORER' ){

        $Line       = intval($ele->nLine) +1;
        $kv         = isset($ele->kv) ? $ele->kv : 1;
        $prx_u      = floatval($ele->deb_sec) * $kv;


        if (  $ele->matching!='LOT' && 
              $ele->matching!='SOUS LOT' && 
              $ele->matching!='TOTAL LOT' && 
              $ele->matching!='TOTAL SOUS LOT' &&
              $ele->matching!='ETUDES' &&
              $ele->matching!='ETUDES ESQUISSE' &&
              $ele->matching!='ETUDES AVANT PROJET' &&
              $ele->matching!='ETUDES PROJET EXECUTION' &&
              $ele->matching!='ETUDES PLAN DE RECOLLEMENT' &&
              $ele->matching!='ETUDES PERMIS DE CONSTRUIRE' &&
              $ele->matching!='INSTALLATION CHANTIER' &&
              $ele->matching!='TOTAL ETUDES HT' &&
              $ele->matching!='TOTAL ETUDES TVA' &&
              $ele->matching!='TOTAL ETUDES TTC' &&
              $ele->matching!='TOTAL ETUDES ET REALISATION HT' &&
              $ele->matching!='TOTAL ETUDES ET REALISATION TVA' &&
              $ele->matching!='TOTAL ETUDES ET REALISATION TTC' &&
              $ele->matching!='TOTAL REALISATION HT' &&
              $Cpru && $Cqte && $Cmnt
            ){

          $spreadsheet->getActiveSheet()
                      ->getCell( $Cpru . $Line )
                      ->setValue( isset($prx_u) ? $prx_u : 0 );

          $spreadsheet->getActiveSheet()
                      ->getCell( $Cmnt . $Line )
                      ->setValue( "=$Cpru$Line*$Cqte$Line" );

          $totalL  .= '+'.$Cmnt . $Line;
          $totalSL .= '+'.$Cmnt . $Line;

         
        }else if ($ele->matching=='LOT'){
          $ln = $Line+1;
          $lot = $Cmnt.$ln;
          $totalL = '';

        }else if ($ele->matching=='SOUS LOT'){
          $ln = $Line+1;
          $slot = $Cmnt.$ln;
          $totalSL = '';

        }else if ($ele->matching=='TOTAL LOT'){
          $ln = $Line-1;
          $spreadsheet->getActiveSheet()
                      ->getCell( $Cmnt.$Line )
                      ->setValue( "=".$totalL );
                      //->setValue( "=SUM($lot:$Cmnt$ln)" );

          $total_realisation .= $Cmnt.$Line.',';

        }else if ($ele->matching=='TOTAL SOUS LOT'){
          $ln = $Line-1;
          $spreadsheet->getActiveSheet()
                      ->getCell( $Cmnt . $Line )
                      ->setValue( "=".$totalSL );
                      //->setValue( "=SUM($slot:$Cmnt$ln)" );

        
        }else if ($ele->matching=='TOTAL REALISATION HT'){
          $total_realisation = substr($total_realisation, 0, -1).')';
          $spreadsheet->getActiveSheet()
                      ->getCell( $Cmnt . $Line )
                      ->setValue( $total_realisation );
        

        }else{
          if (isset($ele->mnt)){
            $spreadsheet->getActiveSheet()
                        ->getCell( $Cmnt . $Line )
                        ->setValue( $ele->mnt );
          }
        }

      }


    }

    $spreadsheet->getActiveSheet()->getProtection()->setSheet(true);

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
    $writer->save("../uploads/tmp/$export_to.xlsx");

    $r = [
      'file' => "./uploads/tmp/$export_to.xlsx"
    ];

  }else if (isset($_GET['exportBpu'])){

    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
    $spreadsheet->setActiveSheetIndexByName($WorkSheetName);

    $uid_file = uniqid();

    $Cint = chr(65 + intval($sub_devis->xls_header->qte));
    $Cstr = chr(65 + intval($sub_devis->xls_header->qte +1));

    if (isset($sub_devis) && isset($sub_devis->xls))
    foreach ($sub_devis->xls as $ele) {
      $Line = strval(intval($ele->nLine)+1);
      
      $spreadsheet->getActiveSheet()
                  ->getCell( $Cint . $Line )
                  ->setValue( isset($ele->bpuMnt) ? $ele->bpuMnt : '' );

      $spreadsheet->getActiveSheet()
                  ->getCell( $Cstr . $Line )
                  ->setValue( isset($ele->bpuStr) ? $ele->bpuStr : '' );
    }

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
    $writer->save("../uploads/tmp/$uid_file.xlsx");

    $r = [
      'file' => "./uploads/tmp/$uid_file.xlsx"
    ];


  // 
  }else{    

    /**  Define a Read Filter  */
    class MyReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
    {
      public function readCell($column, $row, $worksheetName = '') {
        if (in_array($column,range('A','Z'))) {
          return true;
        }
        return false;
      }
    }

    //$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $reader->setReadDataOnly(true);
    //$reader->setLoadSheetsOnly($selectSheet);
    $reader->setReadFilter( new MyReadFilter() );
    $spreadsheet = $reader->load($file);

    if ($WorkSheet)
      $sheetData = $spreadsheet->getSheet($WorkSheet)->toArray();
    else if ($WorkSheetName)
      $sheetData = $spreadsheet->getSheetByName($WorkSheetName)->toArray();
    else
      $sheetData = $spreadsheet->getSheet(0)->toArray();
    //$sheetData = $spreadsheet->getActiveSheet()->toArray();

    $header      = null;
    $n           = null;
    $designation = null;
    $um          = null;
    $qte         = null;
    $prx_u       = null;
    $montant     = null;

    //$arrayHeader = array();
    $arrayMatching = array();
    
    foreach ($sheetData as $key => $value) {
      $line = $sheetData[$key];
      
      
      if ($header === null){  // Check if header isFound
        
        foreach ($line as $k => $v){
          $v = strtolower($v);
          if ( stristr($v,"signation") || stristr($v,"libell")  || stristr($v,"scription") ){ // search designation
            $n = $k-1; // N.Ref c tout jour avant designation
            $designation = $k;
          }elseif ( stristr($v,"qt") || stristr($v,"qu") ){ // search quantite
            $qte = $k;
          }elseif ( stristr($v,"um") || stristr($v,"u.m") || stristr($v,"mesur") ){ // search unité de mesure
            $um = $k;
          }elseif ( stristr($v,"montant") || stristr($v,"mnt") ){ // search montant
            $montant = $k;
          }elseif ( stristr($v,"pu") || stristr($v,"p/u") || stristr($v,"p / u") || stristr($v,"prix") ){ // search prix unitaire if exist
            $prx_u = $k;
          }          
        }
        
        if ($designation && $qte && $montant){
          
          if (!$prx_u && ($montant - $qte) > 1) 
          $prx_u = $qte + 1;

          if (!$um && $qte)
            $um = $qte-1;
          
          $header = $key;

          //print_r($line);

        }else{
          $n           = null;
          $designation = null;
          $um          = null;
          $qte         = null;
          $prx_u       = null;
          $montant     = null;
        }



      } else {
        // skip this line if no N, Designation, Qte
        if ( $line[$n]!=null || $line[$designation]!=null || $line[$um]!=null || $line[$qte]!=null ) {
          $l              = new \stdClass;
          $l->nLine       = $key;
          $l->nArticle    = $line[$n];
          $l->designation = $line[$designation];
          $l->um          = $line[$um];
          $l->qte         = (!$line[$n] && !$line[$qte]) ? '' : (float)filter_var($line[$qte], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

          array_push($arrayMatching, $l);
        }
      }
    }
    $r = [
      'filePath' => $file,
      'header' => [
          'ref'   => $n,
          'des'   => $designation,
          'um'    => $um,
          'qte'   => $qte,
          'prx_u' => $prx_u,
          'mnt'   => $montant
      ],
      'listSheetNames' => $spreadsheet->getSheetNames(),
      'selectedSheet' => ($WorkSheet?$WorkSheet:($WorkSheetName?$WorkSheetName:0)),
      'xls' => $arrayMatching
    ];

    //if (sql_inj($_GET['source'],null))
    //  $r += ['source'=> $sheetData];

  }

}else{
  $r = [];
}

echo json_encode($r, JSON_PRETTY_PRINT);