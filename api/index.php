<?php 

// Include FireWorks lib
require_once('../bin/fw.php');

if (!$fw->signupdate())
{
  header('Location: ../bin/login.php');
}
else
{
  header('Content-Type: application/json; charset=utf-8');
  if (isset($_GET))
  {
    include key($_GET).'.php';
  }
}
