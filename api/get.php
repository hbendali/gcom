<?php

$r       = new \stdClass;
$json    = $fw->get_json(true);
$select  = sql_inj($_GET['get']);
$id_uset = $_SESSION['user']->id_user;

switch ($select) {
	case '987535': // get list of project and sub project and user access controle
		$nature_prj = $fw->getGroup($id_uset);
		$admin = isallow("programmer") || isallow("admin") ? "OR 1" : "";

		$r = $fw->fetchAll("
			SELECT
				projet.*,
				CONCAT(utilisateur.nom, ' ', utilisateur.pnom) as fullname,
				utilisateur.avatar,
				utilisateur.ch as department,
				client.nom_client

			FROM
				projet, utilisateur, client
			WHERE
			(
				`projet`.`nature_prj`='$nature_prj'
			$admin
			)
			AND
				`projet`.`utilisateur` = `utilisateur`.`id_user`
			AND
				`projet`.`num_client` = `client`.`num_client`
			AND
				`projet`.`etat` < 6
		
		");

		foreach ($r as $key => $value) {
			$sd = $fw->fetchAll("
				SELECT
					devis.*,
					CONCAT(utilisateur.nom, ' ', utilisateur.pnom) as fullname,
					utilisateur.avatar,
					utilisateur.ch as department

				FROM
					devis, utilisateur
				WHERE
					`devis`.`num_devis` LIKE '$value->num_devis-%'
				AND
					`devis`.`utilisateur` = `utilisateur`.`id_user`
				AND
					`devis`.`etat` < 99
			");
			foreach ($sd as $sub_key => $sub_value) {
				$sub_value->group_utilisateur = json_decode($sub_value->group_utilisateur);
			}
			$r[$key]->sub_devis = $sd;
		}
		break;

	case '982175': // [ add | remove ] user to devis
		$devis = $fw->fetch("SELECT `group_utilisateur` FROM `devis` WHERE `num_devis`='$json->id_devis'");
		if ($devis){

			if ( !$devis->group_utilisateur || $devis->group_utilisateur=="" ) $devis->group_utilisateur="[]";
			$list = json_decode($devis->group_utilisateur);

			//print_r($list);
			if ( !in_array($json->id_user, $list) && isset($_GET['add'])){
				array_push($list, $json->id_user);
				$fw->fetch("UPDATE `devis` SET `group_utilisateur`='".json_encode($list)."' WHERE `num_devis`='$json->id_devis'");
				$r=1;
			
			}else if ( in_array($json->id_user, $list) && isset($_GET['remove'])){
				array_splice($list, array_search($json->id_user, $list), 1);
				$fw->fetch("UPDATE `devis` SET `group_utilisateur`='".json_encode($list)."' WHERE `num_devis`='$json->id_devis'");
				$r=1;
			
			}else{
				$r=0;
			}
		}else{
			$r=-1;
		}
		break;

	case '524892': // [ delete_logic ] project ( hide from list )
		if ($json->prj_dv == "projet" || $json->prj_dv == "devis"){
			$fw->fetch("UPDATE `$json->prj_dv` SET `etat`='99' WHERE `num_devis`='$json->num_devis'");
			$r=1;
		}else{
			$r=0;
		}
		break;

	case '3d442f': // 
		$r=$fw->fetchAll("SELECT tache.*,sous_lot.num_lot FROM `tache`,`sous_lot` WHERE `sous_lot`.`num_slot`=`tache`.`num_slot`");
		break;

	default:
		break;
}

echo json_encode($r);
