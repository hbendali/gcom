<?php

$r   = new \stdClass;

$lot          = $fw->get_json(true);
$slot         = $lot->slot;
$tache        = $slot->tache;
$main_doeuvre = $tache->main_doeuvre;
$materiel     = $tache->materiel;
$fourniture   = $tache->fourniture;

$num_lot      = sql_inj($lot->num_lot);
$num_slot     = sql_inj($slot->num_slot);

$num_tache    = sql_inj($tache->num_tache);
$nom_tache    = sql_inj($tache->nom_tache);
$um           = sql_inj($tache->um);
$tmp_u        = floatval(sql_inj($tache->tmp_u));
$qte          = floatval(sql_inj($tache->qte));
$prx_unitaire = floatval(sql_inj($tache->prx_unitaire));
$observation  = sql_inj($tache->observation);
$descriptif   = sql_inj($tache->descriptif);
$uploadFile   = sql_inj($tache->uploadFile);

$vol_h_mo     = floatval(sql_inj($tache->vol_h_mo));
$prx_h_mo     = floatval(sql_inj($tache->prx_h_mo));
$cout_mo      = floatval(sql_inj($tache->cout_mo));
$prx_h_mat    = floatval(sql_inj($tache->prx_h_mat));
$cout_mat     = floatval(sql_inj($tache->cout_mat));
$prx_fourn    = floatval(sql_inj($tache->prx_fourn));
$deb_sec      = floatval(sql_inj($tache->deb_sec));

$compo        = json_encode(sql_inj($tache->compo));

$agree        = sql_inj($tache->agree);
$saved        = $tache->saved;

//$target_dir  = "../uploads/images/";

if (  $num_lot != ""
  AND $num_slot != ""
  AND $num_tache != ""
  AND $nom_tache != ""
  AND $um != null
  AND ($tmp_u != 0 OR  $prx_unitaire != 0) 
  )
{

  // INSERT TACHE if new //////////////////////////////////////////////////////////
  if (!$saved)
    $r = $fw->fetchAll("INSERT INTO tache ( num_tache, nom_tache, num_slot, um, tmp_u, qte, prx_unitaire, observation, descriptif, uploadFile, compo, vol_h_mo, prx_h_mo, cout_mo, prx_h_mat, cout_mat, prx_fourn, deb_sec, agree ) VALUES ( '$num_tache', '$nom_tache', '$num_slot', '$um', $tmp_u, $qte, $prx_unitaire, '$observation', '$descriptif', '$uploadFile', '$compo', $vol_h_mo, $prx_h_mo, $cout_mo, $prx_h_mat, $cout_mat, $prx_fourn, $deb_sec, '$agree' )",true,true);

  // UPDATE TACHE if insert ///////////////////////////////////////////////////////
  else
    $r = $fw->fetchAll("UPDATE tache SET nom_tache='$nom_tache', um='$um', tmp_u='$tmp_u', qte='$qte', prx_unitaire='$prx_unitaire', observation='$observation', descriptif='$descriptif', uploadFile='$uploadFile', compo='$compo', vol_h_mo=$vol_h_mo, prx_h_mo=$prx_h_mo, cout_mo=$cout_mo, prx_h_mat=$prx_h_mat, cout_mat=$cout_mat, prx_fourn=$prx_fourn, deb_sec=$deb_sec, agree='$agree' WHERE num_slot='$num_slot' AND num_tache='$num_tache'",true,true);

  // if tache saved goto save MO MATERIEL FOURNITURE /////////////////////////////
  if ($r['errorCode'] == null)
  {
    // DELETE old MO if existe then REINSERT ///////////////////////////////////// 
    $fw->fetchAll("DELETE FROM tache_mo WHERE num_tache='$num_tache';",true,true);
    if ($main_doeuvre)
      foreach ($main_doeuvre as $line_mo => $this_mo) {
        $r = $fw->fetchAll("INSERT INTO tache_mo ( num_tache, code_poste, nombre, amplitude ) VALUES ( '$num_tache', '$this_mo->code_poste', '$this_mo->nombre', '$this_mo->amplitude' )",true,true);
        if ($r['errorCode']) { $r->msg = "Erreur de Main doeuvre !"; break; }
      }

    // DELETE old MATERIEL if existe then REINSERT ///////////////////////////////////// 
    $fw->fetchAll("DELETE FROM tache_mat WHERE num_tache='$num_tache';",true,true);
    if ($materiel)
      foreach ($materiel as $line_mat => $this_mat) {
        $r = $fw->fetchAll("INSERT INTO tache_mat ( num_tache, code_mat, nombre, amplitude ) VALUES ( '$num_tache', '$this_mat->code_mat', '$this_mat->nombre', '$this_mat->amplitude' )",true,true);
        if ($r['errorCode']) { $r->msg = "Erreur de Materiel !"; break; }
      }

    // DELETE old FOURNITURE if existe then REINSERT ///////////////////////////////////// 
    $fw->fetchAll("DELETE FROM tache_four WHERE num_tache='$num_tache';",true,true);
    if ($fourniture)
      foreach ($fourniture as $line_f => $this_f) {
        $r = $fw->fetchAll("INSERT INTO tache_four ( num_tache, code_fourniture, qte ) VALUES ( '$num_tache', '$this_f->code_fourniture', '$this_f->qte' )",true,true);
        if ($r['errorCode'] == 1062) { $r->msg = "Erreur de Fourniture !"; break; }
      }

  }
  else if ($r['errorCode'] == 1062)
  {
    $r['msg'] = "Code de tache existe déja !";
  }
  else
  {
    $r['msg'] = "Erreur de tache !";
  }
}
else
{
  $r['msg'] = "Remplir tous les champs du formulaire en rouje";
  $r['sql'] = null;
  $r['dbg'] = $_POST;;
}

echo json_encode($r, JSON_PRETTY_PRINT);