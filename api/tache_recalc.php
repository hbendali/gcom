<?php

if (!isset($table))
  $table  = sql_inj($_GET['d'],null);             // DETAIL = [ mo, mat, four ]

if (!isset($code))
  $code   = sql_inj($_GET['c'],null);             // M043

if (!isset($update))
  $update = isset($_GET['update'])? true: false; // UPDATE TACHE TABLE WITH DesSec

if (!isset($json))
  $json   = isset($_GET['json'])? true: false;   // RETURN JSON RESULT

if ( $table AND $code )
  tache_recalc($table, $code, $update, $json );


 //////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

function tache_recalc($table_ = null, $code_ = null, $update_ = false, $json_ = false )
{
  global $fw;
  $result_  = array();

  if ($table_ == 'mo'){
    $field_ = "code_poste";
  }else if ($table_ == 'mat'){
    $field_ = "code_mat";
  }else if ($table_ == 'four'){
    $field_ = "code_fourniture";
  }

  if ( $table_ AND $field_ AND $code_ )
  {
    // SELECT ALL TACHE HOW THER ARE UPDATE ////////////////////////////////////
    $select_tache = $fw->fetchAll("SELECT tache.num_tache, tache.tmp_u, tache.qte, tache.prx_unitaire FROM tache, tache_$table_ WHERE tache_$table_.$field_='$code_' AND tache.num_tache=tache_$table_.num_tache;");
    
    // FOR ALL SELECTED TACHE ////////////////////////////////////////////////
    foreach ($select_tache as $sel_tache)
    {
      $th = new \stdClass;

      $num_tache        = $sel_tache->num_tache;
      $th->tmp_u        = $sel_tache->tmp_u;
      $th->qte          = $sel_tache->qte;
      $th->prx_unitaire = $sel_tache->prx_unitaire;
      $th->vol_horaire  = $th->tmp_u * $th->qte; // vol_horaire = tmp_u

      // MAIN DOEUVRE //////////////////////////////////////////////////////////
      $mo = $fw->fetchAll("SELECT nombre, amplitude, cout_horaire FROM tache_mo, main_doeuvre WHERE tache_mo.num_tache='$num_tache' AND main_doeuvre.code_poste=tache_mo.code_poste;");
      
      $th->t_montant_mo      = 0;
      $th->vol_h_mo          = 0;
      $th->prx_h_mo          = 0;
      $th->cout_mo           = 0;
      
      foreach ($mo as $sel_mo) {
        $sel_mo->montant     = $sel_mo->nombre * $sel_mo->amplitude * $sel_mo->cout_horaire;
        $th->t_montant_mo    = $th->t_montant_mo + $sel_mo->montant;
        $th->vol_h_mo        = $th->vol_h_mo + ($sel_mo->nombre * $sel_mo->amplitude);
        $th->prx_h_mo        = $th->t_montant_mo / $th->vol_h_mo;
        $th->cout_mo         = $th->prx_h_mo * $th->vol_horaire;
      }

      // MATERIEL //////////////////////////////////////////////////////////////
      $mat = $fw->fetchAll("SELECT nombre, amplitude, cout_horaire FROM tache_mat, materiel WHERE tache_mat.num_tache='$num_tache' AND materiel.code_mat=tache_mat.code_mat;");
      
      $th->t_montant_mat     = 0;
      $th->prx_h_mat         = 0;
      $th->cout_mat          = 0;
      
      foreach ($mat as $sel_mat) {
        $sel_mat->montant    = $sel_mat->nombre * $sel_mat->amplitude * $sel_mat->cout_horaire;
        $th->t_montant_mat   = $th->t_montant_mat + $sel_mat->montant;
        $th->prx_h_mat       = $th->t_montant_mat / $th->vol_h_mo;
        $th->cout_mat        = $th->prx_h_mat * $th->vol_horaire;
      }

      // FOURNITURE ////////////////////////////////////////////////////////////
      $fourn = $fw->fetchAll("SELECT qte, cout_revient FROM tache_four, fourniture WHERE tache_four.num_tache='$num_tache' AND fourniture.code_fourniture=tache_four.code_fourniture;");
      
      $th->prx_u_fourn       = 0; // <= total du montant fourniture
      $th->prx_fourn         = 0;
      
      foreach ($fourn as $sel_fourn) {
        $sel_fourn->montant  = $sel_fourn->qte * $sel_fourn->cout_revient; 
        $th->prx_u_fourn     = $th->prx_u_fourn + $sel_fourn->montant;
        $th->prx_fourn       = $th->prx_u_fourn * $th->qte;
      }

      // DEBOURSE SEC //////////////////////////////////////////////////////////
      $th->deb_sec = ($th->prx_unitaire * $th->qte)  + $th->prx_fourn + $th->cout_mat + $th->cout_mo;
      
      if ($update_)
      {
        // UPDATE TACHE WITH NEW DEBOURSE SEC ////////////////////////////////////
        $fw->fetchAll("UPDATE tache SET 
              vol_h_mo  = $th->vol_h_mo, 
              prx_h_mo  = $th->prx_h_mo, 
              cout_mo   = $th->cout_mo, 
              prx_h_mat = $th->prx_h_mat, 
              cout_mat  = $th->cout_mat, 
              prx_fourn = $th->prx_fourn, 
              deb_sec   = $th->deb_sec 
        WHERE num_tache = '$num_tache';");
      }
      array_push($result_, $th);
    }
  }

  if ($json_)
    echo json_encode($result_, JSON_PRETTY_PRINT);
  else
    return $result_;
}