<?php
session_start();

$Id_sous_lot = intval($_POST['Id_sous_lot']);
$libelle_sous_lot = htmlspecialchars($_POST['libelle_sous_lot']);

$msg=null;
 
if(isset($libelle_sous_lot) AND isset($Id_sous_lot)){
  if($libelle_sous_lot != '' AND $Id_sous_lot > 0)
  {
    include '../modele/conne.php';
  
    $req = "UPDATE sous_lot SET libelle_sous_lot='$libelle_sous_lot' WHERE Id_sous_lot=$Id_sous_lot";
    
    $reponse = $bdd->prepare($req);
    $donnee=$reponse->execute();
    if($donnee)
    {
      $msg['msg']='ok';
    }
    else
    {
      $msg['msg']='Erreur non definie';
      $msg['sql']=$req;
      $msg['debug']=$reponse->errorInfo();
    }
  }
  else
  {
    $msg['msg']='Erreur non definie';
    $msg['debug']=$_POST;
  }
}
else
{
  $msg['msg']='Nom ou Groupe non definie';
  $msg['debug']=$_POST;
}

echo json_encode($msg);