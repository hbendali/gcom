// JavaScript Document
/**************************************************************************
_________________________________________________________________________ *
  Auteur    : BENDALI BRAHAM Hammza                                       *
  Mail      : hbendali@ya.ru                                              *
  Tél       : +213 552 83 06 77                                           *
  Copyright   : march  2018                                               *
_________________________________________________________________________ *

*/

function UmToLetter( UniteM ){
  var ret='';
  if (UniteM){
    var um = UniteM.toLowerCase().split("/");
    um.forEach( function(element, index) {
      if (index>0) ret += "par ";
      switch( element ){
        case 'boite': ret += "Boite";          break;
        case 'ens':   ret += "Ensemble";       break;
        case 'k':     ret += "Kilo";           break;
        case 'kg':    ret += "Kilogramme";     break;
        case 'km':    ret += "Kilomètre";      break;
        case 'l':     ret += "Litre";          break;
        case 'm':     ret += "Mètre";          break;
        case 'm²':    ret += "Mètre carré";    break;
        case 'm2':    ret += "Mètre carré";    break;
        case 'm³':    ret += "Mètre cube";     break;
        case 'm3':    ret += "Mètre cube";     break;
        case 'ml':    ret += "Mètre lineaire"; break;
        case 't':     ret += "Tonne";          break;
        case 'u':     ret += "Unité";          break;
      }
    });
  }
  return ret ? ret+' ' : '' ;
}//-----------------------------------------------------------------------
