<h1>Recalc All TACHE</h1>
view console F12.
<ul id='log'></ul>
<script language="javascript">
  console.log('Recalc All TACHE');
  // http://127.0.0.1/gcom/api/?bd&lot=CES&slot=ELEC&tache=ELECT004

  // api/?tache_flist   <- GET All list of tache
  $.get( "api/?tache_flist", function( allData ) {

    allData.forEach(function (ele) {

      // api/?db&lot=...   <- GET tache one by one and recalculate
      $.get( "api/?bd&lot="+ele.num_lot+"&slot="+ele.num_slot+"&tache="+ele.num_tache, function( data ) {

        data[0].slot[0].tache = data[0].slot[0].tache[0];
        data[0].slot = data[0].slot[0];
        let tache = data[0];

        tache.slot.tache.saved = true;

        console.log(tache);
        tache.slot.tache = recalc(tache.slot.tache);
        $('#log').append("<li><b>"+ele.num_tache+"</b>: "+tache.slot.tache.deb_sec+"</li>");

        // SAVE 
        $.ajax({
          url: 'api/?tache_save',
          type: 'POST',
          data: JSON.stringify(tache),
          contentType: 'application/json; charset=utf-8'
        });

      });
    });
  });

  let recalc = function(th){
    if (!th.qte) th.qte = 1;
    if (!th.prx_unitaire){
      th.prx_unitaire = 0;
    }

    th.vol_horaire = th.tmp_u * th.qte; // vol_horaire = tmp_u

    // Main doeuvre //////////////////////////////////////////////////////////
    th.t_montant_mo      = 0;
    th.vol_h_mo          = 0;
    th.prx_h_mo          = 0;
    th.cout_mo           = 0;
    th.main_doeuvre.some(function(e1){
      e1.montant         = e1.nombre * e1.amplitude * e1.cout_horaire;
      th.t_montant_mo    = th.t_montant_mo + e1.montant;
      th.vol_h_mo        = th.vol_h_mo + (e1.nombre * e1.amplitude);
      th.prx_h_mo        = th.t_montant_mo / th.vol_h_mo;
      th.cout_mo         = th.prx_h_mo * th.vol_horaire;
    });

    // Materiel //////////////////////////////////////////////////////////////
    th.t_montant_mat     = 0;
    //th.t_vol_horaire     = 0;
    th.prx_h_mat         = 0;
    th.cout_mat          = 0;
    th.materiel.some(function(e1){
      e1.montant         = e1.nombre * e1.amplitude * e1.cout_horaire;
      th.t_montant_mat   = th.t_montant_mat + e1.montant;
      th.prx_h_mat       = th.t_montant_mat / th.vol_h_mo;
      th.cout_mat        = th.prx_h_mat * th.vol_horaire;
    });     

    // Fourniture ////////////////////////////////////////////////////////////
    th.prx_u_fourn       = 0; // <= total du montant fourniture
    th.prx_fourn         = 0;
    th.fourniture.some(function(e1){
      e1.montant         = e1.qte * e1.cout_revient; 
      th.prx_u_fourn     = th.prx_u_fourn + e1.montant;
      th.prx_fourn       = th.prx_u_fourn * th.qte;
    });

    // DEBOURSE SEC //////////////////////////////////////////////////////////
    th.deb_sec = (th.prx_unitaire * th.qte)  + th.prx_fourn + th.cout_mat + th.cout_mo;
    console.log('recalc deb sec:', th.deb_sec);
    return th;
  }

</script>
