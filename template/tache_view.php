<!DOCTYPE html>
<html lang="fr-FR" dir="ltr" ng-app="App">
<head>

  <link rel="stylesheet"    href="../css/semantic-ui/semantic.css">
  <link rel="stylesheet"    href="../css/default.css">

  <script src="../js/jquery.min.js"></script>
  <script src="../js/angular/angular.min.js"></script>
  <script src="../js/angular/angular-sanitize.min.js"></script>
  <script src="../js/angular/i18n/angular-locale_fr-dz.js"></script>
  <script src="../js/showdown.min.js"></script>
  <script src="../js/NumberToLetter.js"></script>
  <script src="../js/polyfiller.js"></script>
  <script src="../css/semantic-ui/semantic.js"></script>

</head>
<body>

<div style="border-bottom: solid 2px #000; padding: 10px;" class="print_only">
  <img src="../img/logo.svg" height="70px" style="float: right;"><br/>
  <div style="width: 50%;">
    <p style="text-align: right; font-family: Cairo, sans-serif;">
      كوسيدار للبناء شريكة بلأسهم<br/>رأس المال الإجتماعي : 3.279.000.000 دج – اس ت 00 .ب 0012858<br/>المقرالإجتماعي : طريق دارالبيضاء – الجزائر
    </p>
    <p>
      Cosider Construction Spa<br/>Capital social : 3.279.000.000 DA - RC 00. B 0012858<br/>Siège social : Route de DAR EL-BEIDA – Alger
    </p>
  </div>
</div>

<div ng-controller="TodoCtrl"  class="ui container">

  <div class="ui basic segment form" ng-cloak>

    <h1 class="ui dividing header">Fiche de Sous Détail de Prix</h1>
    <div class="equal width fields">
      <div class="field">
        <label>Nom Lot</label>
        <input type="text"  ng-model="tache.nom_lot" readonly>
      </div>
      <div class="field">
        <label>Nom sous Lot</label>
        <input type="text"  ng-model="tache.nom_slot" readonly>
      </div>
    </div>

    <div class="equal width fields">
      <div class="three wide field" class="uppercase">
        <label>Code</label>
        <input type="text" ng-model="tache.num_tache" class="uppercase" readonly>
      </div>
      <div class="field">
        <label>Nom du sous detail</label>
        <input type="text" ng-model="tache.nom_tache" readonly>
      </div>
    </div>
    <div class="equal width fields">
      <div class="field">
        <label>Unite de mesure</label>
        <input type="text" ng-model="tache.um" readonly>
      </div>

      <div class="field">
        <label>Temps Unitaire</label>
        <input type="text" min="0" ng-model="tache.tmp_u" readonly>
      </div>
      <div class="field">
        <label>Quantité</label>
        <input type="text" ng-model="tache.qte" readonly>
      </div>
      <div class="field">
        <label>Volume Horaire</label>
        <input type="text" ng-model="tache.vol_horaire = tache.tmp_u * tache.qte" readonly>
      </div>
      <div class="field">
        <label>Prix Unitaire</label>
        <input type="text" ng-model="tache.prx_unitaire" readonly>
      </div>
    </div>
    <div class="field">
      <label>Observation</label>
      <textarea rows="2" ng-model="tache.observation" readonly></textarea>
    </div>
    <div class="field">
      <label>Descriptif</label>
      <textarea rows="2" ng-model="tache.descriptif" readonly></textarea>
    </div>

    <div class="field print_ignore" ng-show="tache.uploadFile">
      <label>Fichier attaché</label>
      <div style="border: 1px #ddd dashed; text-align: center;">
        <a href="{{tache.uploadFile}}" target="_blank">
          <img src="{{tache.uploadFile}}" style="max-height: 200px; max-width: 100%" alt="">
        </a>
      </div>
    </div>

    <div ng-show="!tache.prx_unitaire || tache.prx_unitaire == '0'">
     
      <div class="no_break">
        <h3><i class='circular inverted users icon'></i> Main D'oeuvre</h3>
        
        <table  class="ui yellow selectable very compact table" ng-init="tache.t_montant_mo = 0">
          <thead>
            <tr>
              <th width="">Code</th>
              <th width="100%">Qualification</th>
              <th width="" class="center aligned">Nombre</th>
              <th width="" class="right aligned">Amplitude</th>
              <th width="" class="right aligned">Cout horaire</th>
              <th width="" class="right aligned">Montant</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat=" line_mo in tache.main_doeuvre">
              <td>{{line_mo.code_poste}}</td>
              <td>{{line_mo.libelle}}</td>
              <td class="center aligned">{{line_mo.nombre | number:0}}</td>
              <td class="right aligned">{{line_mo.amplitude | number:0}}</td>
              <td class="right aligned">{{line_mo.cout_horaire | number:2}}</td>
              <td class="right aligned" ng-init="tache.t_montant_mo = tache.t_montant_mo + (line_mo.nombre * line_mo.amplitude * line_mo.cout_horaire)">{{line_mo.montant = line_mo.nombre * line_mo.amplitude * line_mo.cout_horaire | number:2}}</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" class="right aligned">
                Total montant Main D'oeuvre: <b>{{tache.t_montant_mo | number:2}}</b><br>
                Volume horaire de l'equipe: <b>{{tache.vol_h_mo | number:0}}</b><br>
                Prix horaire de l'equipe: <b>{{tache.prx_h_mo | number:2}}</b><br>
                Cout de la main d'oeuvre: <b>{{tache.cout_mo | number:2}}</b><br>
              </th>
              <th class="right aligned" style="background-color: #eee" colspan="2">
                <div class="ui tiny statistic left aligned">
                  <div class="value">
                    {{tache.cout_mo | number:2}}
                  </div>
                </div>
              </th>
            </tr>
          </tfoot>
        </table>
      </div>

      <div class="no_break">
        <h3><i class='circular inverted truck icon'></i> Matériels</h3>

        <table  class="ui yellow selectable compact table">
          <thead>
            <tr>
              <th width="">Code</th>
              <th width="100%">Designation</th>
              <th class="center aligned">Nombre</th>
              <th class="right aligned">Amplitude</th>
              <th class="right aligned">Cout horaire</th>
              <th class="right aligned">Montant</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="line_mat in tache.materiel">
              <td>{{line_mat.code_mat}}</td>
              <td>{{line_mat.libelle}}</td>
              <td class="center aligned">{{line_mat.nombre | number:0}}</td>
              <td class="right aligned">{{line_mat.amplitude | number:0}}</td>
              <td class="right aligned">{{line_mat.cout_horaire | number:2}}</td>
              <td class="right aligned" ng-init="tache.t_montant_mat = tache.t_montant_mat + (line_mat.nombre * line_mat.amplitude * line_mat.cout_horaire)">{{line_mat.montant = line_mat.nombre * line_mat.amplitude * line_mat.cout_horaire | number:2}}</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" class="right aligned">
                Total montant Materiel: <b>{{tache.t_montant_mat | number:2}}</b><br>
                Prix horaire du materiel: <b>{{tache.prx_h_mat | number:2}}</b><br>
                Cout du materiel: <b>{{tache.cout_mat | number:2}}</b><br>
              </th>
              <th class="right aligned" style="background-color: #eee" colspan="2">
                <div class="ui tiny statistic left aligned">
                  <div class="value">
                    {{tache.cout_mat | number:2}}
                  </div>
                </div>
              </th>
            </tr>
          </tfoot>
        </table>
      </div>

      <div class="no_break">
        <h3><i class='circular inverted puzzle piece icon'></i> Fournitures</h3>

        <table class="ui yellow selectable compact table" ng-init="tache.vol_horaire_total_equip = 0">
          <thead>
            <tr>
              <th width="">Code</th>
              <th width="100%">Désignation</th>
              <th class="center aligned">Unité</th>
              <th class="right aligned">Quantité</th>
              <th class="right aligned">Cout revient</th>
              <th class="right aligned">Montant</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="line_fourniture in tache.fourniture">
              <td>{{line_fourniture.code_fourniture}}</td>
              <td>{{line_fourniture.libelle}}</td>
              <td class="center aligned">{{line_fourniture.um}}</td>
              <td class="right aligned">{{line_fourniture.qte | number:2}}</td>
              <td class="right aligned">{{line_fourniture.cout_revient | number:2}}</td>
              <td class="right aligned" ng-init="tache.prx_u_fourn = tache.prx_u_fourn + (line_fourniture.qte * line_fourniture.cout_revient)">{{line_fourniture.montant = line_fourniture.qte * line_fourniture.cout_revient | number:2}}</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" class="right aligned">
                Total montant fourniture: <b>{{tache.prx_u_fourn | number:2}}</b><br>
                Prix de la fourniture: <b>{{tache.prx_fourn | number:2}}</b><br>
              </th>
              <th class="right aligned" style="background-color: #eee" colspan="2">
                <div class="ui tiny statistic left aligned">
                  <div class="value">
                    {{ tache.prx_fourn | number:2 }}
                  </div>
                </div>
              </th>
            </tr>
          </tfoot>
        </table>
      </div>

    </div>

    <div class="ui raised segment right aligned">
      <div class="ui horizontal statistic">
        <div class="label">
          TOTAL DEBOURSE SEC
        </div>
        <div class="value">
          &nbsp;
          {{tache.deb_sec | number:2}}
        </div>
      </div>
      <br>
      <div class="ui horizontal statistic">
        <div class="label">
          DEBOURSE SEC ARRONDI
        </div>
        <div class="value">
          &nbsp;
          {{tache.deb_sec | number:0}}
        </div>
      </div>
    </div>

    <div class="ui right aligned container print_ignore">
      <div class="ui button print_ignore" onclick="print()"><i class='print icon'></i> Imprimer</div>
    </div>
  </div>



</div>

<script language="javascript">

  var app = angular
    .module('App', [])
    .controller('TodoCtrl', function($scope, $filter, $http) {
      $http.get('../api/?tache_view<?=isset($_GET['tache_view'])?"=$_GET[tache_view]":"";?>')
        .then(function(res){
          $scope.tache = res.data;
          console.log('load',$scope.tache);
        })
      ;
    });

</script>

</body>
</html>
