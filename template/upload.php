<div ng-controller="TodoCtrl"  class="ui container">
  
  <label class="ui labeled green icon button" tabindex="0" for="file">
    <i class="paperclip icon" ></i> Upload File
  </label>

  <input  type="file"
          id="file"
          name="file"
          ng-model="file"
          onchange="angular.element(this).scope().upload(this)"
          accept="image/*"
          style="visibility: hidden;" 
          >

  <p>&nbsp;</p>

<div class="ui five column grid">
  <a class="ui column label" ng-repeat="(key, value) in list" href="uploads/{{value.file}}" target="_blanc" style="margin: 5px 10px;">
    <img class="ui right spaced tiny image" src="uploads/{{value.file}}">
    {{value.basename}} {{value.date}} 
  </a>
</div>




  
</div>

<script language="javascript">
  app.controller('TodoCtrl', function($scope, $filter, $http) {

    // upload file whene input change 
    $scope.upload = function(el){
      var fileData = new FormData();
      fileData.append('file', el.files[0]);
      
      //console.log(fileData);
      $http.post('api/?upload=repo&user',fileData, {
        headers: {
          'Content-Type': undefined
        },
        transformResponse: angular.identity
      })
      .then(function(r){
        $scope.loadlist();
      });
    }

    $scope.loadlist = function(){
      $http.get('api/?upload=repo&user&full&list')
        .then(function(res){
          $scope.list=res.data;
        })
      ;
    }

    $scope.loadlist();
  });
</script>