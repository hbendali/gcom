<?php
  if( !isallow("admin") && !isallow("cet")  && !isallow("tc") ){
    echo "<script type='text/javascript'>location='?p=err&err=accès non autoriser&msg=Vous n\'avez pas l\'autorisation d\'accéder a cette rubrique, contactez l\'administrateur';</script>";
    exit;
  }

  if( !isset($_GET["lot"]) || $_GET["lot"]=="" || !isset($_GET["slot"]) || $_GET["slot"]=="" ){
    echo "<script type='text/javascript'>location='?p=err&err=Erreur&msg=Numéro de LOT ou de SOUS LOT non définie';</script>";
    exit;
  }

?>

<div ng-controller="TodoCtrl"  class="ui container">

<datalist id="um"                ><option ng-repeat="ele in um"           value="{{ele.um}}"                ></datalist>
<datalist id="code_poste"        ><option ng-repeat="ele in main_doeuvre" value="{{ele.code_poste}}"        ></datalist>
<datalist id="libelle_mo"        ><option ng-repeat="ele in main_doeuvre" value="{{ele.libelle}}"           ></datalist>
<datalist id="code_mat"          ><option ng-repeat="ele in materiel"     value="{{ele.code_mat}}"          ></datalist>
<datalist id="libelle_mat"       ><option ng-repeat="ele in materiel"     value="{{ele.libelle}}"           ></datalist>
<datalist id="code_f"            ><option ng-repeat="ele in fourniture"   value="{{ele.code_fourniture}}"   ></datalist>
<datalist id="libelle_f"         ><option ng-repeat="ele in fourniture"   value="{{ele.libelle}}"           ></datalist>

<datalist id="tache_list"        ><option ng-repeat="ele in tache_list"   value="{{ele.nom_tache}}">{{ele.num_tache}}</option></datalist>

  <!-- Ajouter / Composi une tache -->
  <div class="ui piled segment" ng-cloak>
    <form autocomplete="off" class="ui form" id="my-section">

      <h2 class="header">Fiche de Sous Détail de Prix</h2>
      <div class="equal width fields">
        <div class="field">
          <label>Nom Lot</label>
          <input type="text"  ng-model="lot.nom_lot" readonly="">
          <input type="hidden" ng-model="lot.num_lot">
        </div>
        <div class="field">
          <label>Nom sous Lot</label>
          <input type="text"  ng-model="lot.slot.nom_slot" readonly="">
          <input type="hidden" ng-model="lot.slot.num_slot">
        </div>
      </div>

      <hr class="style3 print_ignore"></hr>

      <div class="equal width fields">
        <div class="three wide field" class="uppercase" ng-class="{'error':!lot.slot.tache.num_tache}">
          <label>Code</label>
          <input type="text" ng-model="lot.slot.tache.num_tache" class="uppercase" required>
        </div>
        <div class="field" ng-class="{'error':!lot.slot.tache.nom_tache}">
          <label>Nom du sous detail</label>
          <input type="text" ng-model="lot.slot.tache.nom_tache" required>
        </div>
      </div>
      <div class="equal width fields">
        <div class="field" ng-class="{'error':!lot.slot.tache.um}">
          <label>Unite de mesure</label>
          <!--input type="text"  list="um" required-->

          <select class="ui search dropdown" ng-model="lot.slot.tache.um" required>
            <option ng-repeat="ele in um">{{ele.um}}</option>
          </select>

        </div>

        <div class="field" ng-class="{'error':!lot.slot.tache.tmp_u}">
          <label>Temps Unitaire</label>
          <input type="number" min="0" ng-model="lot.slot.tache.tmp_u" ng-change="recalc()" toNumber required>
        </div>
        <div class="field" ng-class="{'error':!lot.slot.tache.qte}">
          <label>Quantité</label>
          <input type="number" ng-model="lot.slot.tache.qte" ng-change="recalc()">
        </div>
        <div class="field" ng-class="{'error':!lot.slot.tache.vol_horaire}">
          <label>Volume Horaire</label>
          <input type="number" ng-model="lot.slot.tache.vol_horaire" readonly>
        </div>
        <div class="field">
          <label>Prix Unitaire</label>
          <input type="number" ng-model="lot.slot.tache.prx_unitaire" min="0" step="any" ng-change="recalc()">
        </div>

        <div class="field print_ignore">
          <label>Type de tache</label>
          <div ng-show="!lot.slot.tache.compo" ng-click="tog_comp()" class=""><i class='circular inverted yellow tag  link icon'></i> <b>Semple</b></div>
          <div ng-show=" lot.slot.tache.compo" ng-click="tog_comp()" class=""><i class='circular inverted pink   tags link icon'></i> <b>Composer</b></div>
        </div>



      </div>
      <div class="field">
        <label>Observation</label>
        <textarea rows="2" ng-model="lot.slot.tache.observation"></textarea>
      </div>
      <div class="field">
        <label>Descriptif</label>
        <textarea rows="2" ng-model="lot.slot.tache.descriptif"></textarea>
      </div>

      <div class="field print_ignore">
        <label>Fichier attaché</label>
        <input  type="file"
                id="file"
                name="file"
                ng-model="file"
                onchange="angular.element(this).scope().upload(this)"
                accept="image/*"
                >
      </div>

      <div class="field print_ignore">
        <div style="border: 1px #ddd dashed; text-align: center;">
          <a href="{{lot.slot.tache.uploadFile}}" target="_blank">
            <img src="{{lot.slot.tache.uploadFile}}" style="max-height: 200px; max-width: 100%" alt="">
          </a>
        </div>
      </div>

      <div ng-show="!lot.slot.tache.prx_unitaire">

        <div ng-show="lot.slot.tache.compo">
          <h3><i class='circular inverted tasks pink icon'></i> Tache Composer</h3>
          <table class="ui pink selectable very compact table">
            <thead>
              <tr>
                <th width="100px">Code</th>
                <th>Tache</th>
                <th width="100px">Qte</th>
                <th width="100px">Cout revient</th>
                <th width="100px">Montant</th>
                <th class="right aligned print_ignore" width="1">
                  <div class="ui mini red icon button"
                    data-tooltip="Ajouter une TACHE"
                    data-inverted=""
                    data-position="left center"
                    ng-click="add_th();"
                    >
                    <i class='add icon'></i>
                  </div> 
                </th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="th in lot.slot.tache.compo">
                <td class="collapsing"><b>{{th.code}}</b></td>
                <td>
                  <div class="ui fluid transparent input">
                    <input type="text" ng-model="th.libelle" list="tache_list" ng-change="srh_comp()" >
                  </div>
                </td>
                <td><div class="ui fluid transparent input"><input type="number" min="0" step="any" ng-model="th.qte" ng-change="calc_mnt()"></div></td>
                <td>{{th.deb_sec | number:2}}</td>
                <td>{{th.montant | number:2}}</td>
                <td class="right aligned print_ignore"><i class="x link icon print_ignore" ng-click="del_th()"></i></td>
              </tr>
            </tbody>
            <<tfoot>
              <tr>
              <th colspan="4" class="right aligned">
                Total montant des tache: <br><br>
              </th>
              <th colspan="2" class="right aligned" style="background-color: #eee">
                <div class="ui tiny statistic left aligned">
                  <div class="value">
                    {{lot.slot.tache.deb_sec | number:2}}</th>
                  </div>
                </div>
              </tr>
            </tfoot>
          </table>

        </div>

        <div  ng-show="!lot.slot.tache.compo">
          <div class="no_break">
            <h3><i class='circular inverted users yellow icon'></i> Main D'oeuvre</h3>
            
            <table  class="ui yellow selectable very compact table">
              <thead>
                <tr>
                  <th width="">Code</th>
                  <th width="100%">Qualification</th>
                  <th width="" class="center aligned">Nombre</th>
                  <th width="" class="right aligned">Amplitude</th>
                  <th width="" class="right aligned">Cout horaire</th>
                  <th width="" class="right aligned">Montant</th>
                  <th width="" class="right aligned print_ignore">
                    <div class="ui mini red icon button"
                      ng-class="{'disabled':!lot.slot.tache.tmp_u}"
                      data-tooltip="Ajouter une MAIN OEUVRE"
                      data-inverted=""
                      data-position="left center"
                      ng-click="add_upd_Main_oeuvre(null);"
                      >
                      <i class='add icon'></i>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat=" line_mo in lot.slot.tache.main_doeuvre" ng-init="recalc()">
                  <td><b>{{line_mo.code_poste}}</b></td>
                  <td>{{line_mo.libelle}}</td>
                  <td class="center aligned">{{line_mo.nombre | number:0}}</td>
                  <td class="right aligned">{{line_mo.amplitude | number:0}}</td>
                  <td class="right aligned">{{line_mo.cout_horaire | number:2}}</td>
                  <td class="right aligned">{{line_mo.montant | number:2}}</td>
                  <td class="right aligned print_ignore">
                    <div class="ui mini icon button"
                      ng-class="{'disabled':!lot.slot.tache.tmp_u}"
                      data-tooltip="Modifier {{line_mo.libelle}}"
                      data-inverted=""
                      data-position="left center"
                      ng-click="add_upd_Main_oeuvre(this.line_mo,$index);">
                      <i class='pencil icon'></i>
                    </div>
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <th colspan="5" class="right aligned">
                    Total montant Main D'oeuvre: <b>{{lot.slot.tache.t_montant_mo | number:2}}</b><br>
                    Volume horaire de l'equipe: <b>{{lot.slot.tache.vol_h_mo | number:0}}</b><br>
                    Prix horaire de l'equipe: <b>{{lot.slot.tache.prx_h_mo | number:2}}</b><br>
                    Cout de la main d'oeuvre: <b>{{lot.slot.tache.cout_mo | number:2}}</b><br>
                  </th>
                  <th class="right aligned" style="background-color: #eee" colspan="2">
                    <div class="ui tiny statistic left aligned">
                      <div class="value">
                        {{lot.slot.tache.cout_mo | number:2}}
                      </div>
                    </div>
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>

          <div class="no_break">
            <h3><i class='circular inverted truck yellow icon'></i> Matériels</h3>

            <table  class="ui yellow selectable compact table">
              <thead>
                <tr>
                  <th width="">Code</th>
                  <th width="100%">Designation</th>
                  <th class="center aligned">Nombre</th>
                  <th class="right aligned">Amplitude</th>
                  <th class="right aligned">Cout horaire</th>
                  <th class="right aligned">Montant</th>
                  <th class="right aligned print_ignore">
                    <div class="ui mini red icon button"
                      ng-class="{'disabled':!lot.slot.tache.tmp_u}"
                      data-tooltip="Ajouter une MATÉRIEL"
                      data-inverted=""
                      data-position="left center"
                      ng-click="add_upd_Materiel(null);"
                      >
                      <i class='add icon'></i>
                    </div>
                  </th>

                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="line_mat in lot.slot.tache.materiel" ng-init="recalc()">
                  <td><b>{{line_mat.code_mat}}</b></td>
                  <td>{{line_mat.libelle}}</td>
                  <td class="center aligned">{{line_mat.nombre | number:0}}</td>
                  <td class="right aligned">{{line_mat.amplitude | number:0}}</td>
                  <td class="right aligned">{{line_mat.cout_horaire | number:2}}</td>
                  <td class="right aligned">{{line_mat.montant | number:2}}</td>
                  <th class="right aligned print_ignore">
                    <div class="ui mini icon button"
                      ng-class="{'disabled':!lot.slot.tache.tmp_u}"
                      data-tooltip="Modifier {{line_mat.libelle}}"
                      data-inverted=""
                      data-position="left center"
                      ng-click="add_upd_Materiel(this.line_mat,$index);"
                      >
                      <i class='pencil icon'></i>
                    </div>
                  </th>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <th colspan="5" class="right aligned">
                    Total montant Materiel: <b>{{lot.slot.tache.t_montant_mat | number:2}}</b><br>
                    Prix horaire du materiel: <b>{{lot.slot.tache.prx_h_mat | number:2}}</b><br>
                    Cout du materiel: <b>{{lot.slot.tache.cout_mat | number:2}}</b><br>
                  </th>
                  <th class="right aligned" style="background-color: #eee" colspan="2">
                    <div class="ui tiny statistic left aligned">
                      <div class="value">
                        {{lot.slot.tache.cout_mat | number:2}}
                      </div>
                    </div>
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>

          <div class="no_break">
            <h3><i class='circular inverted puzzle piece yellow icon'></i> Fournitures</h3>

            <table class="ui yellow selectable compact table" ng-init="lot.slot.tache.vol_horaire_total_equip = 0">
              <thead>
                <tr>
                  <th width="">Code</th>
                  <th width="100%">Désignation</th>
                  <th class="center aligned">Unité</th>
                  <th class="right aligned">Quantité</th>
                  <th class="right aligned">Cout revient</th>
                  <th class="right aligned">Montant</th>
                  <th class="right aligned print_ignore">
                    <div class="ui mini red icon button"
                      ng-class="{'disabled':!lot.slot.tache.tmp_u}"
                      data-tooltip="Ajouter une FOURNITURE"
                      data-inverted=""
                      data-position="left center"
                      ng-click="add_upd_Fourniture(null);"
                      >
                      <i class='add icon'></i>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="line_fourniture in lot.slot.tache.fourniture" ng-init="recalc()">
                  <td><b>{{line_fourniture.code_fourniture}}</b></td>
                  <td>{{line_fourniture.libelle}}</td>
                  <td class="center aligned">{{line_fourniture.um}}</td>
                  <td class="right aligned">{{line_fourniture.qte | number:2}}</td>
                  <td class="right aligned">{{line_fourniture.cout_revient | number:2}}</td>
                  <td class="right aligned">{{line_fourniture.montant | number:2}}</td>
                  <td class="right aligned print_ignore">
                    <div class="ui mini icon button"
                      ng-class="{'disabled':!lot.slot.tache.tmp_u}"
                      data-tooltip="Modifier {{line_fourniture.libelle}}"
                      data-inverted=""
                      data-position="left center"
                      ng-click="add_upd_Fourniture(this.line_fourniture,$index);"
                      >
                      <i class='pencil icon'></i>
                    </div>
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <th colspan="5" class="right aligned">
                    Total montant fourniture: <b>{{lot.slot.tache.prx_u_fourn | number:2}}</b><br>
                    <!--Prix unitaire de la fourniture: <b>{{lot.slot.tache.prx_u_fourn | number:2}}</b><br-->
                    Prix de la fourniture: <b>{{lot.slot.tache.prx_fourn | number:2}}</b><br>
                  </th>
                  <th class="right aligned" style="background-color: #eee" colspan="2">
                    <div class="ui tiny statistic left aligned">
                      <div class="value">
                        {{lot.slot.tache.prx_fourn | number:2}}
                      </div>
                    </div>
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!--hr class="style3"></hr-->

        </div>
      </div>
      <p></p>
      <div class="ui raised segment right aligned" ng-dblclick="recalc()">
        <div class="ui horizontal statistic">
          <div class="label">
            TOTAL DEBOURSE SEC
          </div>
          <div class="value">
            &nbsp;
            {{lot.slot.tache.deb_sec | number:2}}
          </div>
        </div>
        <br>
        <div class="ui horizontal statistic">
          <div class="label">
            DEBOURSE SEC ARRONDI
          </div>
          <div class="value">
            &nbsp;
            {{lot.slot.tache.deb_sec | number:0}}
          </div>
        </div>
      </div>

      <div class="item print_ignore">
        <div class="ui checkbox">
          <input type="checkbox" id="etudes" ng-model="lot.slot.tache.agree" ng-true-value="'1'" ng-false-value="'0'">
          <label for="etudes" ng-cloak>
            <span ng-show="lot.slot.tache.agree == '1'">Validée ( visible pour tout le monde )</span>
            <span ng-show="lot.slot.tache.agree != '1'">Non validée ( n'ai pas visible pour les autre )</span>
          </label>
        </div>
      </div>

      <div class="ui negative message print_ignore" ng-show="err">
        <div class="header">
          Erreur d'enregistrement de tache!
        </div>
        <ul class="list">
          <li ng-bind-html="ele.msg" ng-repeat="ele in err"></li>
        </ul>
      </div>


      <div class="ui right aligned container print_ignore">
        <div class="ui icon button " ng-click="copy_tache()"  data-tooltip="Copy" ><i class='cut icon'></i></div>
        <div class="ui icon button " ng-click="paste_tache()" data-tooltip="Coller" ><i class='paste icon'></i></div>
        <div class="ui icon button " onclick="print()" data-tooltip="Imprimer" ><i class='print icon'></i></div>

        <div  class="ui black icon button"
              ng-class="delete"
              ng-show="lot.slot.tache.num_tache"
              ng-class="lot.slot.tache.delete"
              ng-click="delete_this()"><i class='trash icon'></i></div>

        <div  class="ui black button"
              onclick="top.history.back();"><i class='remove icon'></i>Annuler</div>

        <div  class="ui positive button" 
              ng-click="save()"
              ng-class="{
                'disabled':
                     !lot.slot.tache.num_tache
                  || !lot.slot.tache.nom_tache
                  || !lot.slot.tache.um
              }"
              ><i class='save icon'></i>Enregistrer</div>
      </div>
    </form>
  </div>

  <!-- Ajouter Main oeuvre -->
  <form autocomplete="off">
    <div class="ui modal Main_oeuvre">
      <div class="header">
        {{titel}} MAIN D'OEUVRE
      </div>

      <div class="ui form content">
        <div class="equal width fields">

          <div class="six wide field" ng-class="{'error':!comp_mo.code_poste}">
            <label>Code</label>
            <input  type="text" class="uppercase" 
                    list="code_poste"
                    ng-model="comp_mo.code_poste"
                    ng-change="search('code_poste')" >
          </div>

          <div class="field" ng-class="{'error':!comp_mo.libelle}">
            <label>Libelle poste</label>
            <input  type="text"
                    list="libelle_mo"
                    ng-model="comp_mo.libelle"
                    ng-change="search('libelle_mo')">
          </div>

          <div class="four wide field" ng-class="{'error':!comp_mo.nombre}">
            <label>Nombre</label>
            <input type="number" min="0" step="1" ng-model="comp_mo.nombre" >
          </div>

          <div class="four wide field" ng-class="{'error':!comp_mo.amplitude}">
            <label>Amplitude</label>
            <input type="number" min="0" step="1" ng-model="comp_mo.amplitude" >
          </div>

          <div class="four wide field" ng-class="{'error':!comp_mo.cout_horaire}">
            <label>Cout_horaire</label>
            <input type="number" ng-model="comp_mo.cout_horaire" disabled>
          </div>
        </div>
      </div>

      <div class="actions right aligned">
        <div class="ui black deny button">
          <i class='remove icon'></i>
          Annuler
        </div>
        <div class="ui black button"  ng-class="delete" ng-show="comp_mo.$index != null" ng-click="del_Main_oeuvre()">
          <i class='trash icon'></i>
          Supprimer
        </div>
        <div  class="ui green button"
              ng-click="set_Main_oeuvre()"
              ng-class="{
                'disabled':
                     !comp_mo.code_poste
                  || !comp_mo.libelle
                  || !comp_mo.nombre
                  || !comp_mo.amplitude
              }"
          >
          <i class='checkmark icon'></i>
          {{titel}}
        </div>
      </div>
    </div>
  </form>


  <!-- Ajouter MATERIEL -->

  <form autocomplete="off">
    <div class="ui longer modal Materiel">
      <div class="header">
        {{titel}} MATERIEL
      </div>

      <div class="ui form content">
        <div class="equal width fields">
          <div class="three wide field" ng-class="{'error':!comp_mat.code_mat}">
            <label>Code</label>
            <input  type="text" list="code_mat" ng-model="comp_mat.code_mat" ng-change="search('code_mat')" class="uppercase">
          </div>
          <div class="ten wide field" ng-class="{'error':!comp_mat.libelle}">
            <label>Designation Materiel</label>
            <input  type="text" list="libelle_mat" ng-model="comp_mat.libelle" ng-change="search('libelle_mat')">
          </div>

          <div class="three wide field" ng-class="{'error':!comp_mat.nombre}">
            <label>Nombre</label>
            <input type="number" min="0" step="1" ng-model="comp_mat.nombre" >
          </div>

          <div class="three wide field" ng-class="{'error':!comp_mat.amplitude}">
            <label>Amplitude</label>
            <input type="number" min="0" step="1" ng-model="comp_mat.amplitude" >
          </div>

          <div class="four wide field" ng-class="{'error':!comp_mat.cout_horaire}">
            <label>Cout horaire</label>
            <div class="ui action input">
              <input
                type="number" min="0" step="0.01" toNumber
                ng-model="comp_mat.cout_horaire" disabled>
              <button class="ui icon button" ng-click="comp_mat.show = !comp_mat.show;" disabled>
                <i class="options icon"></i>
              </button>
            </div>
          </div>
        </div>

        <div ng-show="comp_mat.show">
          <hr class="style3"></hr>
          
          <div class="equal width fields">
            <div class="three wide field" ng-class="{'error':!comp_mat.valeur}">
              <label>Valeur</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.valeur">
                <div class="ui basic label">
                  DA
                </div>
              </div>
            </div>

            <div class="three wide field" ng-class="{'error':!comp_mat.frais_g}">
              <label>Frais Gestion</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.frais_g">
                <div class="ui basic label">
                  %
                </div>
              </div>
            </div>

            <div class="three wide field" ng-class="{'error':!comp_mat.longevite}">
              <label>Longevité</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.longevite">
                <div class="ui basic label">
                  U
                </div>
              </div>
            </div>
            <div class="three wide field" ng-class="{'error':!comp_mat.interet}">
              <label>Interet</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.interet">
                <div class="ui basic label">
                  %
                </div>
              </div>
            </div>
            <div class="three wide field" ng-class="{'error':!comp_mat.frais_e}">
              <label>Frais Entretient</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.frais_e">
                <div class="ui basic label">
                  %
                </div>
              </div>
            </div>
            <div class="three wide field" ng-class="{'error':!comp_mat.duree_t}" >
              <label>Durée Travail</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.duree_t"
                  >
                <div class="ui basic label">
                  H
                </div>
              </div>
            </div>
          </div>
          <button class="ui button" ng-click="calc_Materiel_detail()">
            <i class="calculator icon"></i>
            Calculer
          </button>

        </div>
      </div>

      <div class="actions">
        <div class="ui black deny button">
          <i class='remove icon'></i>
          Annuler
        </div>
        <div class="ui black button"  ng-class="delete" ng-show="comp_mat.$index != null" ng-click="del_Materiel()">
          <i class='trash icon'></i>
          Supprimer
        </div>
        <div  class="ui green button"
              ng-click="set_Materiel()"
              ng-class="{
                'disabled':
                     !comp_mat.code_mat
                  || !comp_mat.libelle
                  || !comp_mat.nombre
                  || !comp_mat.amplitude
              }"
            >
          <i class='checkmark icon'></i>
          {{titel}}
        </div>
      </div>
    </div>
  </form>

  <!-- Ajouter / Modifier Fourniture -->
  <form autocomplete="off">
    <div class="ui longer modal Fourniture">
      <div class="header">
        {{titel}} FOURNITURE
      </div>

      <div class="ui form content">
        
        <div class="equal width fields">

          <div class="three wide field" ng-class="{'error':!comp_f.code_fourniture}">
            <label>Code</label>
            <input  type="text" list="code_f" ng-model="comp_f.code_fourniture" ng-change="search('code_fourniture')" class="uppercase">
          </div>

          <div class="field" ng-class="{'error':!comp_f.libelle}">
            <label>Designation</label>
            <input  type="text" list="libelle_f" ng-model="comp_f.libelle" ng-change="search('libelle_fourniture')">
          </div>


          <div class="six wide field" ng-class="{'error':!comp_f.cout_revient}">
            <label>Cout revient</label>
            <div class="ui right labeled input">
              <input
                type="number" min="0" step="0.01" ng-model="comp_f.cout_revient" toNumber disabled>
              <div class="ui basic label">
                DA
              </div>
            </div>
          </div>
          <div class="one wide field">
            <label>&nbsp;</label>
            <button class="ui icon button" ng-click="comp_f.show = !comp_f.show;" disabled>
              <i class="options icon"></i>
            </button>
          </div>

        </div>

        <div class="equal width fields">

          <div class="field">
            <label>Origine</label>
            <input type="text" ng-model="comp_f.origine">
          </div>

          <div class="four wide field" ng-class="{'error':!comp_f.um}">
            <label>U Mesure</label>
            <input type="text" ng-model="comp_f.um" list="um" required>
          </div>

          <div class="four wide field" ng-class="{'error':!comp_f.qte}">
            <label>Quantité</label>
            <input type="number" min="0" step="any" ng-model="comp_f.qte" >
          </div>

        </div>


        <div ng-show="comp_f.show">
          <hr class="style3"></hr>

          <div class="equal width fields">
            <div class="three wide field" ng-class="{'error':!comp_f.prix_achat}">
              <label>Prix achat</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_f.prix_achat">
                <div class="ui basic label">
                  DA
                </div>
              </div>
            </div>

            <div class="three wide field" ng-class="{'error':!comp_f.transport}">
              <label>Transport</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_f.transport">
                <div class="ui basic label">
                  DA
                </div>
              </div>
            </div>

            <div class="three wide field" ng-class="{'error':!comp_f.dechargement}">
              <label>Dechargement</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_f.dechargement">
                <div class="ui basic label">
                  DA
                </div>
              </div>
            </div>

            <div class="three wide field" ng-class="{'error':!comp_f.perte}">
              <label>Perte</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_f.perte">
                <div class="ui basic label">
                  %
                </div>
              </div>
            </div>

          </div>
          <button class="ui button" ng-click="calc_Fourniture_detail()">
            <i class="calculator icon"></i>
            Calculer
          </button>

        </div>
      </div>

      <div class="actions">
        <div class="ui black deny button">
          <i class='remove icon'></i>
          Annuler
        </div>
        <div class="ui black button"  ng-class="delete" ng-show="comp_f.$index != null" ng-click="del_Fourniture()">
          <i class='trash icon'></i>
          Supprimer
        </div>
        <div class="ui green button"
              ng-click="set_Fourniture()"
              ng-class="{
                'disabled':
                     !comp_f.code_fourniture
                  || !comp_f.libelle
                  || !comp_f.cout_revient
                  || !comp_f.qte
              }"
            >
          <i class='checkmark icon'></i>
          {{titel}}
        </div>
      </div>

    </div>
  </form>


</div>

<script language="javascript" src="">      </script>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $filter, $http) {
    
  $http.get('api/?bd<?=isset($_GET['lot'])?"&lot=$_GET[lot]":"";?><?=isset($_GET['slot'])?"&slot=$_GET[slot]":"";?><?=isset($_GET['tache'])?"&tache=$_GET[tache]":"&tache=-1";?>')
    .then(function(res){


      var lot            = res.data[0];
          lot.slot       = lot.slot[0];
          lot.slot.tache = lot.slot.tache[0];

      if (lot.slot.tache===undefined)
      {
        lot.slot.tache  = {
          num_tache    : null,
          nom_tache    : null,
          um           : null,
          tmp_u        : 0.00,
          qte          : 1.00,
          prx_unitaire : 0.00,
          observation  : null,
          descriptif   : null,
          img          : null,
          main_doeuvre : [],
          materiel     : [],
          fourniture   : [],
          //deb_sec      : 0.00, // <= recalculate
          agree        : false,
          saved        : false
        }
      }
      else
      {
        if (lot.slot.tache.main_doeuvre == null) lot.slot.tache.main_doeuvre = [];
        lot.slot.tache.main_doeuvre.some(function(e1){
          e1.nombre = parseFloat(e1.nombre);
          e1.amplitude = parseFloat(e1.amplitude);
          e1.cout_horaire = parseFloat(e1.cout_horaire);
        });

        if (lot.slot.tache.materiel == null) lot.slot.tache.materiel = [];
        lot.slot.tache.materiel.some(function(e1){
          e1.nombre = parseFloat(e1.nombre);
          e1.amplitude = parseFloat(e1.amplitude);
          e1.cout_horaire = parseFloat(e1.cout_horaire);
        });

        if (lot.slot.tache.fourniture == null) lot.slot.tache.fourniture = [];
        lot.slot.tache.fourniture.some(function(e1){
          e1.qte = parseFloat(e1.qte);
          e1.cout_revient = parseFloat(e1.cout_revient);
        });

        lot.slot.tache.tmp_u   = parseFloat(lot.slot.tache.tmp_u);
        lot.slot.tache.qte     = parseFloat(lot.slot.tache.qte);
        lot.slot.tache.prx_unitaire = parseFloat(lot.slot.tache.prx_unitaire);

        lot.slot.tache.saved   = true;
      }

      $scope.lot = lot;
      $scope.recalc();
      console.log('lot', $scope.lot);
    })
  ;
    
  $http.get('api/?list=main_doeuvre')
    .then(function(res){
      $scope.main_doeuvre = res.data;
      console.log('main_doeuvre',$scope.main_doeuvre);
    })
  ;
    
  $http.get('api/?list=materiel')
    .then(function(res){
      $scope.materiel = res.data;
      console.log('materiel',$scope.materiel);
    })
  ;
    
  $http.get('api/?list=fourniture')
    .then(function(res){
      $scope.fourniture = res.data;
      console.log('fourniture',$scope.fourniture);
    })
  ;
    
  $http.get('api/?list=um')
    .then(function(res){
      $scope.um = res.data;
      console.log('um',$scope.um);
    })
  ;


  // LOAD TACHE ///////
  //$http.get('api/?tache_flist')
  $http.get('api/?tache_view')
    .then(function(res){
      res.data.forEach(function(e){
        e.deb_sec = parseFloat(e.deb_sec);
      })
      $scope.tache_list = res.data;
//      console.log('tache',$scope.tache_list);
    })
  ;
  /////////////////////


      
// MAIN OEUVRE /////////////////////////////////////////////////////////////////////////////////////
      
    $scope.add_upd_Main_oeuvre = function(v,i){
      if (i !== undefined && i !== null) v.$index = i;
      console.log('v',v);

      if (v)
      {
        $scope.titel = 'Modifier';
        $scope.comp_mo = angular.copy(v);
      }
      else
      {
        $scope.titel = 'Ajouter';
        $scope.comp_mo = {$index:null, code_poste:'', libelle:'', nombre:0.00 ,amplitude:0.00 ,cout_horaire:0.00};
      }
      console.log('comp_mo',$scope.comp_mo);

      $('.ui.modal.Main_oeuvre').modal('show');
    }

    $scope.set_Main_oeuvre = function(){
      if ($scope.comp_mo.code_poste && $scope.comp_mo.libelle)
      {
        
        if ($scope.comp_mo.$index !== null)
        {
          $scope.lot.slot.tache.main_doeuvre[$scope.comp_mo.$index] = $scope.comp_mo;
        }
        else
        {
          $scope.lot.slot.tache.main_doeuvre.push($scope.comp_mo);
        }
        
        $scope.recalc();
        $('.ui.modal.Main_oeuvre').modal('hide');
      }
      else
      {
        console.log('recherche FIELD');
        $scope.loading = '';
      }
    }

    $scope.del_Main_oeuvre = function(){
      if (confirm("Etre vous sure de vouloir supprimer "+$scope.comp_mo.libelle))
      {
        $scope.lot.slot.tache.main_doeuvre.splice($scope.comp_mo.$index, 1);
        $scope.recalc();
        $('.ui.modal.Main_oeuvre').modal('hide');
      }
    }

// MATÉRIELS ////////////////////////////////////////////////////////////////////////////////////////

    $scope.add_upd_Materiel = function(v,i){
      if (i !== undefined && i !== null) v.$index = i;
      //console.log('v',v);

      if (v)
      {
        $scope.titel = 'Modifier';
        $scope.comp_mat = angular.copy(v);
      }
      else
      {
        $scope.titel = 'Ajouter';
        $scope.comp_mat = {$index:null, code_mat:null,libelle:null,cout_horaire:0,valeur:0,frais_g:0,longevite:0,interet:0,frais_e:0,duree_t:0};
      }

      $('.ui.modal.Materiel').modal('show');
    }

    $scope.set_Materiel = function(){
      if ($scope.comp_mat.code_mat && $scope.comp_mat.libelle)
      {
        
        if ($scope.comp_mat.$index !== null)
        {
          $scope.lot.slot.tache.materiel[$scope.comp_mat.$index] = $scope.comp_mat;
        }
        else
        {
          $scope.lot.slot.tache.materiel.push($scope.comp_mat);
        }
        
        $('.ui.modal.Materiel').modal('hide');
        console.log('lot',$scope.lot);
      }
      else
      {
        console.log('recherche FIELD');
        $scope.loading = '';
      }
    }

    $scope.del_Materiel = function(){
      if (confirm("Etre vous sure de vouloir supprimer "+$scope.comp_mat.libelle))
      {
        $scope.lot.slot.tache.materiel.splice($scope.comp_mat.$index, 1);
        $scope.recalc();
        $('.ui.modal.Materiel').modal('hide');
      }
    }

    $scope.calc_Materiel_detail = function(v){
      console.log('comp_mat',$scope.comp_mat);
      var v = $scope.comp_mat;

      var one   = v.valeur;
      var two   = v.frais_g / 100;
      var three = v.longevite;
      var four  = v.interet;
      var five  = v.frais_e;
      var six   = v.duree_t;
      var result = parseFloat(((one * ( 1 + two ) ) / 100 ) *  ( ( ( 100 / three ) + four + five ) / six ) ).toFixed(2);
      
      console.log(one);
      console.log(two);
      console.log(three);
      console.log(four);
      console.log(five);
      console.log(six);
      console.log( result );

      $scope.comp_mat.cout_horaire = result *1; // *1 <- pour converter string to integer
    }

// FOURNITURE ///////////////////////////////////////////////////////////////////////////////////////

    $scope.add_upd_Fourniture = function(v,i){
      if (i !== undefined && i !== null) v.$index = i;
      //console.log('v',v);

      if (v)
      {
        $scope.titel = 'Modifier';
        $scope.comp_f = angular.copy(v);
      }
      else
      {
        $scope.titel = 'Ajouter';
        $scope.comp_f = {$index:null, code_fourniture:null,libelle:null,cout_revient:0,origine:null,um:null,prix_achat:0,transport:0,dechargement:0,perte:0};
      }

      $('.ui.modal.Fourniture').modal('show');
    }

    $scope.set_Fourniture = function(){
      if ($scope.comp_f.code_fourniture && $scope.comp_f.libelle)
      {
        
        if ($scope.comp_f.$index !== null)
        {
          $scope.lot.slot.tache.fourniture[$scope.comp_f.$index] = $scope.comp_f;
        }
        else
        {
          $scope.lot.slot.tache.fourniture.push($scope.comp_f);
        }
        
        $('.ui.modal.Fourniture').modal('hide');
        console.log('lot',$scope.lot);
      }
      else
      {
        console.log('recherche FIELD');
        $scope.loading = '';
      }
    }

    $scope.del_Fourniture = function(){
      if (confirm("Etre vous sure de vouloir supprimer "+$scope.comp_f.libelle))
      {
        $scope.lot.slot.tache.fourniture.splice($scope.comp_f.$index, 1);
        $scope.recalc();
        $('.ui.modal.Fourniture').modal('hide');
      }
    }

    $scope.calc_Fourniture_detail = function(v){
      console.log('comp_f',$scope.comp_f);
      var v = $scope.comp_f;

      var one   = v.valeur;
      var two   = v.frais_g / 100;
      var three = v.longevite;
      var four  = v.interet;
      var five  = v.frais_e;
      var six   = v.duree_t;
      var result = parseFloat(((one * ( 1 + two ) ) / 100 ) *  ( ( ( 100 / three ) + four + five ) / six ) ).toFixed(2);
      
      console.log(one);
      console.log(two);
      console.log(three);
      console.log(four);
      console.log(five);
      console.log(six);
      console.log( result );

      $scope.comp_f.cout_horaire = result *1; // *1 <- pour converter string to integer
    }

// CALCULE /////////////////////////////////////////////////////////////////////////////////////////

    $scope.recalc = function(){
      var th = $scope.lot.slot.tache;
      if (!th.qte) th.qte = 1;
      if (!th.prx_unitaire){
        th.prx_unitaire = 0;
      }

      th.vol_horaire = th.tmp_u * th.qte; // vol_horaire = tmp_u
      if ($scope.lot.slot.tache.compo===null){
        // Main doeuvre //////////////////////////////////////////////////////////
        th.t_montant_mo      = 0;
        th.vol_h_mo          = 0;
        th.prx_h_mo          = 0;
        th.cout_mo           = 0;
        th.main_doeuvre.some(function(e1){
          e1.montant         = e1.nombre * e1.amplitude * e1.cout_horaire;
          th.t_montant_mo    = th.t_montant_mo + e1.montant;
          th.vol_h_mo        = th.vol_h_mo + (e1.nombre * e1.amplitude);
          th.prx_h_mo        = th.t_montant_mo / th.vol_h_mo;
          th.cout_mo         = th.prx_h_mo * th.vol_horaire;
        });

        // Materiel //////////////////////////////////////////////////////////////
        th.t_montant_mat     = 0;
        //th.t_vol_horaire     = 0;
        th.prx_h_mat         = 0;
        th.cout_mat          = 0;
        th.materiel.some(function(e1){
          e1.montant         = e1.nombre * e1.amplitude * e1.cout_horaire;
          th.t_montant_mat   = th.t_montant_mat + e1.montant;
          th.prx_h_mat       = th.t_montant_mat / th.vol_h_mo;
          th.cout_mat        = th.prx_h_mat * th.vol_horaire;
        });     

        // Fourniture ////////////////////////////////////////////////////////////
        th.prx_u_fourn       = 0; // <= total du montant fourniture
        th.prx_fourn         = 0;
        th.fourniture.some(function(e1){
          e1.montant         = e1.qte * e1.cout_revient; 
          th.prx_u_fourn     = th.prx_u_fourn + e1.montant;
          th.prx_fourn       = th.prx_u_fourn * th.qte;
        });

        // DEBOURSE SEC //////////////////////////////////////////////////////////
        //th.deb_sec = (th.prx_unitaire * th.qte)  + th.prx_fourn + th.cout_mat + th.cout_mo;
        th.deb_sec = (th.prx_unitaire * th.qte) || ((th.prx_fourn + th.cout_mat + th.cout_mo)*th.qte);
      }else{
        th.deb_sec = 0;
        th.compo.forEach( el => {
          th.deb_sec += el.montant;
        });
      }
      console.log('recalc deb sec:', th.deb_sec);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    $scope.search = function(search_by){
      var srh;
      switch(search_by) {
          case 'code_poste':
              srh = $filter('filter')($scope.main_doeuvre, {code_poste: $scope.comp_mo.code_poste}, true);
              srh = srh[0] ? srh[0] : {libelle:null,cout_horaire:0}; 
              $scope.comp_mo.libelle        =           (srh.libelle);
              $scope.comp_mo.cout_horaire   = parseFloat(srh.cout_horaire);
              break;
              
          case 'libelle_mo':
              srh = $filter('filter')($scope.main_doeuvre, {libelle: $scope.comp_mo.libelle}, true);
              srh = srh[0] ? srh[0] : {code_poste:null,cout_horaire:0} ; 
              $scope.comp_mo.code_poste     =           (srh.code_poste);
              $scope.comp_mo.cout_horaire   = parseFloat(srh.cout_horaire);
              break;

          case 'code_mat':
              srh = $filter('filter')($scope.materiel, {code_mat: $scope.comp_mat.code_mat}, true);
              srh = srh[0] ? srh[0] : {libelle:null,cout_horaire:0,valeur:0,frais_g:0,longevite:0,interet:0,frais_e:0,duree_t:0} ; 
              $scope.comp_mat.libelle       =           (srh.libelle);
              $scope.comp_mat.cout_horaire  = parseFloat(srh.cout_horaire);                      
              $scope.comp_mat.valeur        = parseFloat(srh.valeur);
              $scope.comp_mat.frais_g       = parseFloat(srh.frais_g);
              $scope.comp_mat.longevite     = parseFloat(srh.longevite);
              $scope.comp_mat.interet       = parseFloat(srh.interet);
              $scope.comp_mat.frais_e       = parseFloat(srh.frais_e);
              $scope.comp_mat.duree_t       = parseFloat(srh.duree_t);
              break;

          case 'libelle_mat':
              srh = $filter('filter')($scope.materiel, {libelle: $scope.comp_mat.libelle}, true);
              srh = srh[0] ? srh[0] : {code_mat:null,cout_horaire:0,valeur:0,frais_g:0,longevite:0,interet:0,frais_e:0,duree_t:0} ; 
              $scope.comp_mat.code_mat      =           (srh.code_mat);
              $scope.comp_mat.cout_horaire  = parseFloat(srh.cout_horaire);                      
              $scope.comp_mat.valeur        = parseFloat(srh.valeur);
              $scope.comp_mat.frais_g       = parseFloat(srh.frais_g);
              $scope.comp_mat.longevite     = parseFloat(srh.longevite);
              $scope.comp_mat.interet       = parseFloat(srh.interet);
              $scope.comp_mat.frais_e       = parseFloat(srh.frais_e);
              $scope.comp_mat.duree_t       = parseFloat(srh.duree_t);
              break;

          case 'code_fourniture':
              srh = $filter('filter')($scope.fourniture, {code_fourniture: $scope.comp_f.code_fourniture}, true);
              srh = srh[0] ? srh[0] : {libelle:null,cout_revient:0,origine:null,um:null,prix_achat:0,transport:0,dechargement:0,perte:0}; 
              $scope.comp_f.libelle         =           (srh.libelle      );
              $scope.comp_f.cout_revient    = parseFloat(srh.cout_revient );
              $scope.comp_f.origine         =           (srh.origine      );
              $scope.comp_f.um              =           (srh.um           );
              $scope.comp_f.prix_achat      = parseFloat(srh.prix_achat   );
              $scope.comp_f.transport       = parseFloat(srh.transport    );
              $scope.comp_f.dechargement    = parseFloat(srh.dechargement );
              $scope.comp_f.perte           = parseFloat(srh.perte        );
              break;

          case 'libelle_fourniture':
              srh = $filter('filter')($scope.fourniture, {libelle: $scope.comp_f.libelle}, true);
              srh = srh[0] ? srh[0] : {code_fourniture:null,cout_revient:0,origine:null,um:null,prix_achat:0,transport:0,dechargement:0,perte:0}; 
              $scope.comp_f.code_fourniture =           (srh.code_fourniture);
              $scope.comp_f.cout_revient    = parseFloat(srh.cout_revient );
              $scope.comp_f.origine         =           (srh.origine      );
              $scope.comp_f.um              =           (srh.um           );
              $scope.comp_f.prix_achat      = parseFloat(srh.prix_achat   );
              $scope.comp_f.transport       = parseFloat(srh.transport    );
              $scope.comp_f.dechargement    = parseFloat(srh.dechargement );
              $scope.comp_f.perte           = parseFloat(srh.perte        );
              break;
      }
      console.log('search', srh);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    $scope.save = function(){
      var lot = $scope.lot;
      $scope.loading = 'loading';

      //console.log('result',lot);

      $http.post('api/?tache_save',lot)
        .then(function(r){
          //console.log('result',r);
          if (r.data.errorCode == null && r.data.sqlState == "00000")
            top.history.back();
            //location.reload();
        });
    }

    $scope.delete_this = function(){
      var tache = this.lot.slot.tache;
      console.log('delete', tache.nom_tache);

      if (confirm("Etre vous sure de vouloir supprimer la tache "+tache.nom_tache))
      {
        $http.get('api/?delete=tache_mo&w=num_tache&e='+tache.num_tache)
          .then(function(r){
            console.log('tache_mo',tache.num_tache);
            if (r.data.errorCode == null)
              $http.get('api/?delete=tache_mat&w=num_tache&e='+tache.num_tache)
                .then(function(r){
                  console.log('tache_mat',tache.num_tache);
                  if (r.data.errorCode == null)
                    $http.get('api/?delete=tache_four&w=num_tache&e='+tache.num_tache)
                      .then(function(r){
                        console.log('tache_four',tache.num_tache);
                        if (r.data.errorCode == null)
                          $http.get('api/?delete=tache&w=num_tache&e='+tache.num_tache)
                            .then(function(r){
                              console.log('tache',tache.num_tache);
                              if (r.data.errorCode == null)
                                top.history.back();
                              // location.reload();
                            });
                      });
                });
          });
      }
    }

    // upload file whene input change 
    $scope.upload = function(el){
      var fileData = new FormData();
      fileData.append('file', el.files[0]);
      
      //console.log(fileData);
      $http.post('api/?upload=img',fileData, {
        headers: {
          'Content-Type': undefined
        },
        transformResponse: angular.identity
      })
      .then(function(r){
        $scope.lot.slot.tache.uploadFile = JSON.parse(r.data).fullpath;
        console.log($scope.lot.slot.tache.uploadFile );
      });
    }

    $scope.copy_tache = function () {
      localStorage.setItem("copy_tache", JSON.stringify($scope.lot.slot.tache));
      toastr.options.extendedTimeOut = 3000;
      toastr.options.timeOut = 3000;
      toastr.options.onclick = null;
      toastr.options.positionClass = "toast-top-right";
      toastr["info"](
        "La tache à ete copyer. "+$scope.lot.slot.tache.nom_tache+" "+$scope.lot.slot.tache.num_tache
      );
    }
    $scope.paste_tache = function () {
      $scope.lot.slot.tache = JSON.parse(localStorage.getItem("copy_tache"));
      toastr.options.extendedTimeOut = 3000;
      toastr.options.timeOut = 3000;
      toastr.options.onclick = null;
      toastr.options.positionClass = "toast-top-right";
      toastr["info"](
        "La tache à ete Coller. "+$scope.lot.slot.tache.nom_tache+" "+$scope.lot.slot.tache.num_tache
      );
    }

    $scope.add_th = function(){
      if ($scope.lot.slot.tache.compo=='' || $scope.lot.slot.tache.compo===true || $scope.lot.slot.tache.compo===null)
        $scope.lot.slot.tache.compo=[];
      $scope.lot.slot.tache.compo.push({code:null,libelle:null,deb_sec:0,qte:0,montant:0});
    }

    $scope.del_th = function(){
      $scope.lot.slot.tache.compo.splice(this.$index, 1);
      $scope.recalc();
    }

    $scope.srh_comp = function(){
      //console.log(this.th);
      var srh = $filter('filter')($scope.tache_list, {nom_tache: this.th.libelle }, true);
      if (srh.length == 1)
      {
        var t = angular.copy(srh[0]);
        console.log(t);
        this.th.code    =t.num_tache;
        this.th.libelle =t.nom_tache;
        this.th.deb_sec =t.deb_sec;
        this.th.qte     =1;
        this.th.montant =this.th.deb_sec * this.th.qte;
        
        $scope.recalc();
      }else{
        this.th.code    =null;
        this.th.qte     =0;
        this.th.deb_sec =0;
        this.th.qte     =0;
        this.th.montant  =0;
      }
    }

    $scope.calc_mnt = function () {
      this.th.montant  = this.th.deb_sec * this.th.qte;
      $scope.recalc();
    }

    $scope.tog_comp = function () {
      if (confirm("Etre vous sure de vouloir changer le type de tache ?")){
        if (!$scope.lot.slot.tache.compo || $scope.lot.slot.tache.compo==="" || $scope.lot.slot.tache.compo===null)
          $scope.lot.slot.tache.compo=[];
        else
          $scope.lot.slot.tache.compo=null;

        $scope.lot.slot.tache.fourniture   = [];
        $scope.lot.slot.tache.main_doeuvre = [];
        $scope.lot.slot.tache.materiel     = [];

        $scope.recalc();
      }
    }
    

});

//$('.ui.checkbox').checkbox();

</script>