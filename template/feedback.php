<div ng-controller="TodoCtrl" class="ui text form container">
  
  <div class="ui top attached secondary segment card">
    <div class="image">
      <img src="img/596c9a151c554.image.jpg">
    </div>  
  </div>
  <div class="ui attached segment">

    <div class="field">
      <textarea placeholder="Message ..." ng-model="mail.content" tabindex="1"></textarea>
    </div>

    <div class='ui basic right aligned segment print_ignore'>
      <div class="ui blue labeled submit icon button" ng-click="sendMail()" ng-class="{disabled:!mail.content}">
        <i class="icon paper plane"></i> Envoyer
      </div>
      
    </div>

  </div>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $filter, $http) {
  $scope.sendMail = function(){
    $scope.mail.to = 'programmer';
    $scope.mail.subject = 'FEEDBACK';
    $http.post('api/?topics',$scope.mail)
      .then(function(r){
        console.log(r);
        if (r.data.sqlState == "00000" && r.data.id)
          location.assign("?");
      })
    ;
  }

  $scope.takeIt = function(){

  }
});</script>
