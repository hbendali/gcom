<?php
  if(!isallow("admin") && !isallow("cet") && !isallow("tc") ) {
    echo "<script type='text/javascript'>location='?p=err&err=accès non autoriser&msg=Vous n\'avez pas l\'autorisation d\'accéder a cette rubrique, contactez l\'administrateur';</script>";
    exit;
  }
?>


<div ng-controller="TodoCtrl" >
  <h2>MATERIELS</h2>

  <div class="ui action input">
    <input type="text" placeholder="Recherche ..." ng-model="srh_mat">
    <button class="ui icon button">
      <i class="search icon"></i>
    </button>
  </div>

  <table class="ui compact selectable red table">
    <thead>
      <tr>
        <th width="5%">Code</th>
        <th width="30%">Designation</th>
        <th class="right aligned">Valeur (DA)</th>
        <th class="right aligned">Frais Gestion (%)</th>
        <th class="right aligned">Longevité (U)</th>
        <th class="right aligned">Interet (%)</th>
        <th class="right aligned">Frais Entretien (%)</th>
        <th class="right aligned">Durée Travail (H)</th>
        <th class="right aligned">Cout horaire</th>
        <th>
        </th>
        <th>
          <div class='ui mini red icon button'
            data-tooltip="Ajouter un MATERIEL"
            data-inverted=""
            data-position="left center"
            ng-click="detail();"
            >
            <i class='add icon'></i>
          </div>          
        </th>
      </tr>
    </thead>
    <tbody>
      <tr
        ng-repeat="l_mat in materiel | filter:srh_mat"
        ng-cloak
        >
        <td>{{l_mat.code_mat}}</td>
        <td>{{l_mat.libelle}}</td>
        <td class="right aligned">{{l_mat.valeur|number:2}}</td>
        <td class="right aligned">{{l_mat.frais_g|number:2}}</td>
        <td class="right aligned">{{l_mat.longevite|number:2}}</td>
        <td class="right aligned">{{l_mat.interet|number:2}}</td>
        <td class="right aligned">{{l_mat.frais_e|number:2}}</td>
        <td class="right aligned">{{l_mat.duree_t|number:2}}</td>
        <td class="right aligned">{{l_mat.cout_horaire|number:2}}</td>
        <td class="right aligned">
          <div
            class="ui mini basic icon button"
            data-tooltip="Supprimer {{l_mat.code_mat}}"
            data-inverted=""
            data-position="left center"
            ng-click="delete(this.l_mat);"
            >
            <i class='trash icon'></i>
          </div>
        </td>
        <td class="right aligned">
          <div
            class="ui mini basic icon button"
            data-tooltip="Modifier {{l_mat.code_mat}}"
            data-inverted=""
            data-position="left center"
            ng-click="detail(this.l_mat);"
            >
            <i class='write icon'></i>
          </div>
        </td>
      </tr>
    </tbody>
  </table>

  <!-- Ajouter / Modifier Materiel -->
  <form method="post" action="" autocomplete="off">
    <div class="ui longer modal Materiel">
      <div class="header">
        {{comp_mat.titel}} MATERIEL
      </div>

      <div class="ui form scrolling content">
        <div class="fields">
          <div class="three wide field" ng-class="{'error':!comp_mat.code_mat}">
            <label>Code</label>
            <input type="text" ng-model="comp_mat.code_mat" class="uppercase">
          </div>
          <div class="ten wide field" ng-class="{'error':!comp_mat.libelle}">
            <label>Designation</label>
            <input type="text" ng-model="comp_mat.libelle">
          </div>

          <div class="three wide field" ng-class="{'error':!comp_mat.cout_horaire}">
            <label>Cout horaire (DA)</label>
            <div class="ui action input">
              <input
                type="number" min="0" step="0.01" toNumber
                ng-model="comp_mat.cout_horaire">
              <button class="ui icon button" ng-click="comp_mat.show = !comp_mat.show;" data-tooltip="Détail" data-inverted="">
                <i class="options icon"></i>
              </button>
            </div>
          </div>
        </div>

        <div ng-show="comp_mat.show">
          <hr class="style3"></hr>
          
          <div class="equal width fields">
            <div class="three wide field">
              <label>Valeur</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.valeur">
                <div class="ui basic label">
                  DA
                </div>
              </div>
            </div>

            <div class="three wide field">
              <label>Frais Gestion</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.frais_g">
                <div class="ui basic label">
                  %
                </div>
              </div>
            </div>

            <div class="three wide field">
              <label>Longevité</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.longevite">
                <div class="ui basic label">
                  U
                </div>
              </div>
            </div>

            <div class="three wide field">
              <label>Interet</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.interet">
                <div class="ui basic label">
                  %
                </div>
              </div>
            </div>
            <div class="three wide field">
              <label>Frais Entretien</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.frais_e">
                <div class="ui basic label">
                  %
                </div>
              </div>
            </div>
            <div class="three wide field">
              <label>Durée Travail</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_mat.duree_t"
                  >
                <div class="ui basic label">
                  H
                </div>
              </div>
            </div>
          </div>
          <button class="ui button" ng-click="calculer()">
            <i class="calculator icon"></i>
            Calculer
          </button>

        </div>
      </div>
      

      <div class="actions">
        <div
          class="ui deny icon button"
          ng-show="comp_mat.titel=='Modifier'"
          ng-click="delete(comp_mat)"
          ><i class="trash icon"></i>
        </div>
        <div class="ui deny button">
          Annuler
        </div>
        <div
          class="ui green right button"
          ng-click="update(comp_mat)"
          ng-disabled=""
          ng-class="{ 'disabled' : !comp_mat.code_mat || !comp_mat.libelle || !comp_mat.cout_horaire }"
          >{{comp_mat.titel}}
        </div>
      </div>
    </div>
  </form>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $http) {
    
  $http.get('api/?list=materiel')
    .then(function(res){
      angular.forEach(res.data, function(ele) {
        
        ele.cout_horaire = parseFloat(ele.cout_horaire);
        ele.valeur       = parseFloat(ele.valeur);
        ele.frais_g      = parseFloat(ele.frais_g);
        ele.longevite    = parseFloat(ele.longevite);
        ele.interet      = parseFloat(ele.interet);
        ele.frais_e      = parseFloat(ele.frais_e);
        ele.duree_t      = parseFloat(ele.duree_t);

      });
      $scope.materiel = res.data;
      console.log('materiel',$scope.materiel);
    });
  
  $scope.detail = function(mat){
    console.log('detail',mat);
    if (mat)
    {
      $scope.comp_mat = angular.copy(mat) ;
      $scope.comp_mat.titel = "Modifier";
    }
    else
      $scope.comp_mat = {titel:"Ajouter",code_mat:null,libelle:null,cout_horaire:0.00,valeur:0.00,frais_g:0.00,longevite:0.00,interet:0.00,frais_e:0.00,duree_t:0.00};
    $('.ui.modal.Materiel').modal('show');
  }

  $scope.update = function(mat){
    console.log('update',mat);
    $http.post('api/?update=materiel',mat)
    .then(function(r){
      console.log('result',r);
      if (r.data.res = "done!" && r.data.pdo.sqlState == "00000")
        location.reload();
    });
  }

  $scope.delete = function(mat){
    console.log('delete',mat);
    if (confirm("Etre vous sure de vouloir le supprimer"))
    {
      $http.post('api/?delete=materiel&w=code_mat&e='+mat.code_mat)
      .then(function(r){
        console.log('result',r);
        if (r.data.res = "done!" && r.data.pdo.sqlState == "00000")
          location.reload();
      });
    }
  }

  $scope.calculer = function(){
    console.log($scope.comp_mat);
    var v = $scope.comp_mat;

    var one   = v.valeur;
    var two   = v.frais_g / 100;
    var three = v.longevite;
    var four  = v.interet;
    var five  = v.frais_e;
    var six   = v.duree_t;
    var result = parseFloat(((one * ( 1 + two ) ) / 100 ) *  ( ( ( 100 / three ) + four + five ) / six ) ).toFixed(2);
    
    console.log(one);
    console.log(two);
    console.log(three);
    console.log(four);
    console.log(five);
    console.log(six);
    console.log( result );

    $scope.comp_mat.cout_horaire = result *1; // *1 <- pour converter string to integer
  }

}); </script>