<div class="ui raised very padded text container piled red segment">
  <h2 class="ui header">Ajouter une UNITÉ DE MESURE</h2> 

  <form method="post" action="ctrl/um_add.php" class="ui form">

    <div class="required field">
      <label>Libellé</label>
      <div class="ui right input">
        <input type="text" ng-model="um" name="um" required>
      </div>
    </div>

    <button class="ui teal button">
      Ajouter
    </button>

    <hr class="style3"></hr>

    <div
      class="ui divided horizontal list"
      ng-init="list=[<?php
    $result = $fw->fetchAll("SELECT * FROM um");
    foreach ($result as $ele) {
      echo "'".sql_inj($ele->um)."',";
    }
    ?>]">
      <div class="item" ng-repeat="ele in list | filter:um" ng-cloak>{{ele}}</div>
    </div>
  </form>
</div>