<div ng-controller="TodoCtrl">

  <div class="ui raised very padded text container piled segment">

    <form method="post" ng-submit="submit()" autocomplete="off" class="ui form content" ng-cloak>
            
            <h2 class="ui header">{{client.titel}} un CLIENT</h2> 
            
            <div class="equal width fields">
              <div class="three wide field">
                <label>CLIENT</label>
                <input type="text" placeholder="CODE" ng-model="client.num_client" maxlength="20" ng-readonly="client.titel=='Modifier'" class="uppercase" required>
              </div>
              <div class="fluid field">
                <label>&nbsp;</label>
                <input type="text" placeholder="NOM CLIENT" ng-model="client.nom_client" maxlength="100" required>
              </div>
            </div>

            <div class="field">
              <label>Responsable</label>
              <input type="text" placeholder="RESPONSABLE" ng-model="client.responsable" maxlength="50" required>
            </div>

            <div class="field">
              <label>Tel</label>
              <input type="tel" placeholder="TEL" ng-model="client.tel" maxlength="50">
            </div>

            <div class="field">
              <label>Email</label>
              <input type="email" placeholder="EMAIL" ng-model="client.email" maxlength="50">
            </div>

            <div class="field">
              <label>Adresse</label>
              <textarea rows="3" placeholder="ADRESSE" ng-model="client.adresse" required maxlength="16000"></textarea>
            </div>

            <div class="field">
              <button class="ui black deny icon button" ng-click="delete()" ng-show="client.titel=='Modifier'"><i class='trash icon'></i></button>
              <a      class="ui black deny button"      href="?p=client/list"> Annuler </a>
              <button class="ui positive right button"  type="submit" > {{client.titel}} </button>
            </div>
    </form>

  </div>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $filter, $http) {

  $http.get('api/?list=client&w=num_client&e=<?=(isset($_GET['num_client']) && $_GET['num_client'] != "")?$_GET['num_client']:'-1';?>')
    .then(function(res){
      console.log('client',res.data);
      if (res.data.length>0){
        $scope.client = res.data[0];
        $scope.client.titel = 'Modifier';
      }else{
        $scope.client = {titel:'Ajouter', num_client:null, nom_client:null, responsable:null, tel:null, email:null, adresse:null};
      }
    });

  $scope.submit = function(){
    console.log('update client',$scope.client);
    $http.post('api/?update=client',$scope.client)
      .then(function(r){
        console.log('result',r);
        if (r.data.errorCode == null)
          top.history.back();
      });
  }

  $scope.delete = function(){
    if (confirm("Etre vous sure de vouloir supprimer ce client "+$scope.client.nom_client))
    {
      $http.post('api/?delete=client&w=num_client&e='+$scope.client.num_client,{})
        .then(function(r){
          console.log('client',$scope.client.num_client);
          if (r.data.errorCode == null)
            location.assign("?p=client/list");
        });
    }
  }

}); </script>