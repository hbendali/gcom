<div class="ui container" ng-controller="TodoCtrl" ng-cloak>
  <form class="ui form"
        name="form"
        ng-model="profile"
        ng-submit="form.$valid && submit()"
        autocomplete="off"
        novalidate
        >

    <div class="ui top attached secondary segment">
      <input type="hidden" name="id" ng-model="profile.id_user">
      
      <!--img src="{{profile.gravatar}}?s=180&d=mm"-->
      <div class="ui center aligned basic segment">
        <img src="img/image.png" data-ng-src="{{profile.avatar}}" height="160" class="ui centered circular image">
        <h2>{{profile.nom}} {{profile.pnom}}</h2>
        @{{profile.username}} - <a href="mailto:{{profile.email}}"><i class="mail icon" ng-show="profile.email"></i> {{profile.email}}</a>
        <br>
        {{profile.adr}}
      </div>


      <p></p>
    
    </div>
    <div class="ui attached segment">


      <p>
        <div class="ui blue ribbon label">
          <i class="unlock alternate icon"></i> Identification
        </div>
      </p>

      <div class="equal width fields">
        
        <div class="field">
          <label> Nom d'utilisateur </label>
          <div class="ui left corner labeled input rmv_corner">
            <input  type="text" 
                    name="username"
                    value=""
                    placeholder="Nom d'utilisateur"
                    ng-model="profile.username"
                    min="3"
                    max="20"
                    autocomplete="off"
                    <?=isallow("programmer") || isallow("admin") ? 'required' : 'readonly' ?>
                    >
            <div class="ui left corner label">
              <i class="inverted asterisk icon"></i>
            </div>
          </div>
          <div class="ui pointing red inverted label" ng-show="form.username.$invalid && form.username.$touched">
            Choisir un nom d'utilisateur entre 3 et 20 character !
          </div>
        </div>

        <div class="field">
          <label>Mot de passe</label>
          <div class="ui left corner labeled input rmv_corner">
            <input  type="password"
                    name="passwor"
                    value=""
                    placeholder="Mot de passe"
                    ng-model="profile.password"
                    maxlength="20"
                    minlength="1"
                    pattern=".{1,}"
                    autocomplete="off"
                    >
            <div class="ui left corner label">
              <i class="inverted asterisk icon"></i>
            </div>
          </div>
          <div class="ui pointing red inverted label" ng-show="form.password.$invalid && form.password.$touched">
            Choisir un mot de passe entre 4 et 20 character !
          </div>
        </div>
      </div>

      <p>
        <div class="ui blue ribbon label"><i class="user icon"></i> profile</div>
      </p>
      
      <div class="equal width fields">
        <div class="field">
          <label>Nom</label>
          <div class="ui left corner labeled input">
            <input type="text" ng-model="profile.nom" name="nom" placeholder="Nom" required>
            <div class="ui left corner label">
              <i class="inverted asterisk icon"></i>
            </div>
          </div>
          <div class="ui pointing red inverted label" ng-show="form.nom.$invalid && form.nom.$touched">
            Choisir un nom d'utilisateur !
          </div>
        </div>
        <div class="field">
          <label>Prénom</label>
          <div class="ui left corner labeled input">
            <input type="text" ng-model="profile.pnom" name="pnom" placeholder="Prénom" required>
            <div class="ui left corner label">
              <i class="inverted asterisk icon"></i>
            </div>
          </div>
        </div>
        <div class="field">
          <label>EMail</label>
          <div class="ui left corner labeled input">
            <input type="email" placeholder="EMail" ng-model="profile.email" name="email" required>
            <div class="ui left corner label">
              <i class="inverted asterisk icon"></i>
            </div>
          </div>
          <div class="ui pointing red inverted label" ng-show="form.email.$invalid && form.email.$touched">
            email non valide!
          </div>
        </div>
      </div>


      <div class="field">

        <div class="two fields">
          <div class="field">
            <label>Chantier / Département</label>
            <input type="text" ng-model="profile.ch" placeholder="Chantier / Département ">
          </div>

          <div class="field">
            <label>Directeur</label>
            <input type="text" ng-model="profile.dir" placeholder="Directeur">
          </div>
        </div>

      </div>

      <div class="equal width fields">

          <div class="field">
            <label>Mobile</label>
            <input type="text" ng-model="profile.mob" placeholder="Mobile">
          </div>

          <div class="field">
            <label>Tel</label>
            <input type="text" ng-model="profile.tel" placeholder="Tel">
          </div>

          <div class="field">
            <label>Fax</label>
            <input type="text" ng-model="profile.fax" placeholder="Fax">
          </div>

      </div>

      <div class="field">
        <label>Addresse</label>
        <textarea ng-model="profile.adr"></textarea>
      </div>

      <div class="field">
        <label>Signature</label>
        <textarea ng-model="profile.sig"></textarea>
      </div>
      <div class="field">
        <label> URL de l'image </label>
        <div class="ui input">
          <input  type="text" 
                  placeholder="url image"
                  id="avatar"
                  ng-model="profile.avatar"
                  >
        </div>
      </div>

      <?php
    if(isallow("programmer") || isallow("admin"))
      echo <<< ACL
      <div class="ui red ribbon label">
        <i class="shield icon"></i> Les droit d'acc&egrave;s
      </div>

      <div class="ui divided selection list">
        
        <div class="item red_bottom_separate">
          <div class="ui checkbox">
            <input type="checkbox" name="unlock" ng-model="profile.acl.enable">
            <label>Compte Activer</label>
          </div>
        </div>

        <div class="item">
          <div class="ui checkbox">
            <input type="checkbox" name="coder" ng-model="profile.acl.programmer">
            <label>Developer, Debuger</label>
          </div>
        </div>

        <div class="item">
          <div class="ui checkbox">
            <input type="checkbox" name="admin" ng-model="profile.acl.admin">
            <label>Administrateur</label>
          </div>
        </div>

        <div class="item">
          <div class="ui checkbox">
            <input type="checkbox" name="user_plus" ng-model="profile.acl.user_plus">
            <label>Utilisateur Avancer ( Chef de gtoup ou de projet )</label>
          </div>
        </div>

        <div class="item">
          <div class="ui checkbox">
            <input type="checkbox" name="user" ng-model="profile.acl.user">
            <label>Utilisateur Simple</label>
          </div>
        </div>

        <div class="item red_bottom_separate">
          <div class="ui checkbox">
            <input type="checkbox" name="guest" ng-model="profile.acl.guest">
            <label>Consultant</label>
          </div>
        </div>

        <div class="item">
          <div class="ui checkbox">
            <input type="checkbox" name="cet" ng-model="profile.acl.cet">
            <label>C.E.T - CORPS D'ETAT TECHNIQUES</label>
          </div>
        </div>

        <div class="item">
          <div class="ui checkbox">
            <input type="checkbox" name="cet" ng-model="profile.acl.tc">
            <label>T.C - TECHNIQUO COMMERCIALE </label>
          </div>
        </div>

        <div class="item">
          <div class="ui checkbox">
            <input type="checkbox" name="bat" ng-model="profile.acl.bat">
            <label>BAT - BATIMENT</label>
          </div>
        </div>

        <div class="item">
          <div class="ui checkbox">
            <input type="checkbox" name="gc" ng-model="profile.acl.gc">
            <label>GC - GENIE CIVIL</label>
          </div>
        </div>

        <div class="item red_bottom_separate">
          <div class="ui checkbox">
            <input type="checkbox" name="dpi" ng-model="profile.acl.dpi">
            <label>DPI - DÉPARTEMENT DE PRODUCTION ET INFRASTRUCTURE</label>
          </div>
        </div>

        <div class="item">
          <div class="ui checkbox">
            <input type="checkbox" name="method" ng-model="profile.acl.method">
            <label>Methodiste</label>
          </div>
        </div>

      </div>
ACL;



      ?>
      
    </div>


    <div class="ui bottom attached right aligned secondary segment">
      
      <div class="ui info message" ng-show="msg">
        <i class="close icon"></i>
        <ul class="list">
          {{msg}}
        </ul>
      </div>

      <?=isallow("admin")?"<div class='ui cancel button' ng-click='delete()' ng-class='{loading:spiner_del}'> <i class='trash alternate outline icon'></i> Supprimer l'utilisateur </div>":"";?>

      <div class="ui cancel button" onClick="window.history.back();" ng-class="{'loading':spiner_del}"> <i class="remove icon"></i> Annuler </div>
      
      <button class="ui green ok button" ng-click="submit()" type="submit"
        ng-disabled="form.username.$dirty && form.username.$invalid || form.email.$dirty && form.email.$invalid"
      >
        <i class="checkmark icon"></i> Enregistrer
      </button>

    </div>
    
    <!--div class="ui attached error message"></div-->

    <p></p>
  </form>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $interval, $filter, $http) {

  // LOAD PROFILE //////////
  $http.get('api/?user=<?=sql_inj($_GET['id'],'');?>')
    .then(function(res){
      if (res.data.length > 0){
        $scope.profile = res.data[0];
        $scope.profile.password = null;
      }else{
        $scope.profile = {id_user:'new',username:null,password:null}
      }

      console.log("Load Profile <?=sql_inj($_GET['id'],'');?>", $scope.profile);
    });

  $scope.submit = function(){
    $http.post('api/?user',$scope.profile)
      .then(function(r){
        console.log('result',r);
        if (r.data.sqlState == '00000')
          location.assign("?<?=isallow("admin")?'p=utilisateur/liste_utilisateur':'';?>");
        else
          $scope.msg_error = r.data.pdo.message;
      });
  }

});</script>