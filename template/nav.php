<div style="margin:0;padding:0px 10px 0px 0px; background-color:#fff; border-right: 15px solid #F00;" class="print_ignore">
  <img src="img/logo.svg" style="float: right; height: 80px;" />
  <img src="img/banner.png" style="max-height: 100px" />
</div>

<div class="ui attached inverted structured menu print_ignore">
  <div class="ui item"></div>
<?php
  //if (isset($_GET[0]))
    //echo "<a class='red item' onclick='window.history.back();'><i class='angle left icon'></i></a>";


if( isallow("programmer") || isallow("admin") || isallow("bat") || isallow("dpi") || isallow("gc") || isallow("tc") || isallow("cet") ){
 
  echo "<div class='ui dropdown red item'> TABLES <div class='menu'>";

  if( isallow("programmer") || isallow("admin") || isallow("cet") || isallow("tc") )
  {
    echo '
    <a class="item" href="?p=main_doeuvre">MAIN D\'OEUVRE</a>
      <a class="item" href="?p=materiel">MATERIEL</a>
      <a class="item" href="?p=fourniture">FOURNITURE</a>
      <a class="item" href="?p=um">UNITE DE MESURE</a>
      <a class="item" href="?p=fam">LIST DES FAMILLE</a>
    ';
  }
    
  if ( isallow("programmer") || isallow("admin") || isallow("bat") || isallow("gc") || isallow("dpi") )
  {
    echo '
    <a class="item" href="?p=client/list" >CLIENT</a>
    <a class="item" href="?p=tva" >TVA</a>
    ';
  }
  
  if ( isallow("programmer") || isallow("admin")  || isallow("user_plus") )
  {
    echo '
    <a class="item" href="?p=devis/devis_user_rule">GESTION PROJET, UTILISATEUR</a>
    ';
  }
  
  if ( isallow("programmer") || isallow("admin"))
  {
    echo '
    <a class="item" href="?p=utilisateur/liste_utilisateur">LIST DES UTILISATEUR <span style="color:red;">ADMINISTRATEUR</span></a>
    ';
  }
  
  echo "</div></div>";
}
  
  if ( isallow("programmer") || isallow("admin") || isallow("cet") || isallow("tc") )
  {
    $banque_donnee = "";
    
    $lot = $fw->fetchAll("SELECT * FROM lot");
    foreach ($lot as $line) {
      $banque_donnee .=  "<a class='item' href='?p=banque_donnee/list&lot=$line->num_lot'>$line->num_lot / $line->nom_lot</a>";
    }
    $banque_donnee .=  "<a class='item active' href='?p=banque_donnee/add'><i class='add icon'></i> Ajouter un LOT</a>";
    
    echo <<< BANQUE_DE_DONNEES
  <div class="ui dropdown red item">
    BANQUE DE DONNEES
    <div class="menu">
      $banque_donnee
    </div>
  </div>
BANQUE_DE_DONNEES;
  }

  if ( isallow("programmer") || isallow("admin") || isallow("bat") || isallow("dpi") || isallow("gc") ){


    $id_user = $_SESSION['user']->id_user;

    $projet = "";
    $pr = $fw->fetchAll("SELECT * FROM `projet` WHERE etat < 5 AND `num_devis` in (
        SELECT SUBSTRING_INDEX(`devis`.`num_devis`, '-', 1) AS ndevis FROM `devis` WHERE `group_utilisateur` LIKE CONCAT('%\"$id_user\"%') GROUP BY ndevis
      )");

    if( isallow("programmer") )
      $pr = $fw->fetchAll("SELECT * FROM `projet` WHERE etat < 5 AND `num_devis` in (
        SELECT SUBSTRING_INDEX(`devis`.`num_devis`, '-', 1) AS ndevis FROM `devis` WHERE 1 GROUP BY ndevis
      )");

    foreach ($pr as $pr_line) {

      $devis = "";
      $dv = $fw->fetchAll("SELECT * FROM devis WHERE etat < 99 AND num_devis LIKE '$pr_line->num_devis-%' AND `group_utilisateur` LIKE CONCAT('%\"$id_user\"%')");

      if( isallow("programmer") )
        $dv = $fw->fetchAll("SELECT * FROM devis WHERE etat < 99 AND num_devis LIKE '$pr_line->num_devis-%'");

      foreach ($dv as $dv_line) {

        $dv_line->nom_devis = wordwrap($dv_line->nom_devis,30,"<br/>");

        $devis .= "
            <a class='item' href='?p=devis/add$dv_line->etat&projet=$dv_line->num_devis'>
              <i class='file text outline icon'></i> $dv_line->nom_devis
            </a>";
      }

      $pr_line->nom_devis = wordwrap($pr_line->nom_devis,30,"<br/>");


      $projet .= "
        <div class='item' onClick=\"go('?p=devis/add0&projet=$pr_line->num_devis')\"><i class='sitemap icon'></i> $pr_line->nom_devis<i class='dropdown icon'></i>
          <div class='menu'>
            <a class='item active' href='?p=devis/add1&projet=$pr_line->num_devis-new'> NOUVEAU SOUS PROJET </a>
            <a class='item active' href='?p=underc'> DOCUMENT D'ACCOMPAGNEMENT </a>

            $devis
          </div>
        </div>
      ";
    }

    echo '<a class="ui item" href="?p=devis/devis_list"> PROJETS </a>';
/*
    echo <<< PROJETS
  <div class="ui dropdown red item">
    <div>PROJETS</div>
    <div class="structured menu">
      <a class="item active" href="?p=devis/add0"> NOUVEAU PROJET </a>
      $projet
    </div>
  </div>
PROJETS;
*/

  }

  
  // CONSULTAYION ////////////////
  echo "<div class='ui dropdown red item'><a href='?p=devis' >CONSULTATION</a><div class='menu'>";
  
  if ( isallow("programmer") || isallow("admin") || isallow("bat") || isallow("dpi") || isallow("gc") ){
    echo "<a class='item' href='?p=devis/list'>Liste Projets</a>";
  }
  
  if ( isallow("programmer") || isallow("admin") || isallow("bat") || isallow("dpi") || isallow("gc")  || isallow("tc") || isallow("guest") ){    
    echo "<a class='item' href='?p=banque_donnee/list_global'>BANQUE DE DONNEES</a>";    
  }
  
  echo "</div></div>";

  // //- MAIL -----------------------

  // $user_id = $_SESSION["user"]->id_user;

  // //- NOTIFICATION ---------------
  // $fw->fetchAll("
  //   UPDATE mail SET sts='read' WHERE
  //   (
  //     ( SELECT acl FROM utilisateur WHERE id_user=$user_id ) LIKE CONCAT( '%\"',`receiver`,'\":true%' )
  //     OR
  //     receiver=$user_id
  //   )
  //   AND sts is NULL;");


  // //- NBR of notification --------
  // $mail = $fw->fetchAll("
  //   SELECT * FROM mail WHERE
  //     ( ( SELECT acl FROM utilisateur WHERE id_user=$user_id ) LIKE CONCAT( '%\"',`receiver`,'\":true%' ) OR `receiver`=$user_id )
  //   AND
  //     ( `sr` IS NULL OR `sr`!='d' );
  //   ");

  // $nbr_notification = count($mail)>0 ? "<div class='floating ui red label'>".count($mail)."</div>" : "" ;

  // //- MAIL RECEIVED --------------
  // $mail_received = "";
  // $mail = $fw->fetchAll("SELECT * FROM mail WHERE
  //   ( SELECT acl FROM utilisateur WHERE `id_user`=$user_id ) LIKE CONCAT( '%\"',`receiver`,'\":true%' )
  //   OR `receiver`=$user_id AND ( `sr` IS NULL OR `sr`!='d' )");
  // $notif = 0;
  // foreach ($mail as $element){
  //   $element->obj = wordwrap($element->obj,40,"<br/>");
  //   $mail_received .= "<a class='item' href='?p=mail&sr&id=$element->id_mail'>".(($element->sr=='c')?'<del>':'')."$element->obj".(($element->sr=='c')?'</del>':'')."</a>";
  // }

  // //- MAIL SENT ------------------
  // $mail_sent = "";
  // $mail = $fw->fetchAll("SELECT * FROM mail WHERE `sender`=$user_id AND `ss`!='d' ORDER BY date_insert DESC");
  // foreach ($mail as $element){
  //   $element->obj = wordwrap($element->obj,40,"<br/>");
  //   $mail_sent .= "<a class='item' href='?p=mail&ss&id=$element->id_mail'>".(($element->ss=='c')?'<del>':'')."$element->obj".(($element->ss=='c')?'</del>':'')."</a>";
  // }

  // //- MAIL MENU ITEM -------------
  // // echo "
  // // <div class='ui dropdown item'>NOTIFICATIONS $nbr_notification
  // //   <div class='menu'>
  // //     <a class='item' href='?p=notification'><i class='edit icon'></i> NOTIFICATIONS</a>
  // //     <a class='item' href='?p=mail'><i class='edit icon'></i> NOUVELLE</a>
  // //     <div class='item dropdown' href='?p=mail'>
  // //       <i class='envelope open outline icon'></i> REÇUE
  // //       <div class='menu'>
  // //         $mail_received
  // //       </div>
  // //     </div>
  // //     <div class='item dropdown' href='?p=mail'>
  // //       <i class='envelope outline icon'></i> ENVOYÉS
  // //       <div class='menu'>
  // //         $mail_sent
  // //       </div>
  // //     </div>
  // //   </div>
  // // </div>";

  // echo "<a class='ui item' href='?p=notification'>NOTIFICATIONS $nbr_notification</a>";

?>
  <a class='ui item' href='?p=notification&id={{last_posts_id}}' ng-click="notification_read()">
    NOTIFICATIONS
    <div class='floating ui red label' ng-show='new_posts && new_posts > 0 ' ng-cloak>{{new_posts}}</div>
  </a>

  <div class="right menu">

<?php if( isallow("programmer") ) echo <<< PROGRAMMER
    <div class="ui dropdown olive item">
      <a href="?p=devis" >DEVELOPER</a>
      <div class="menu">
        <a class="item" href="/adminer.php" target="_blank">ADMINER MySQL</a>
        <a class="item" href="javascript:if (confirm('Êtes-vous sûr ?')){location = '?p=recalc_all_tache';}">RECALC ALL TACHE</a>
        <a class="item" href="http://192.168.5.81:81/COSIDER.CONSTRUCTION/GCom/wiki" target="Gitea"> DOCUMENTATION Wiki</a>
        <a class="item" href="?p=issue"><i class="icon exclamation"></i> issues</a>
        <a class="item" href="?p=backup"><i class="icon database"></i> Backup</a>
      </div>
    </div>
PROGRAMMER;
?>
    <div class="ui dropdown uppercase item" style="padding:5px 20px;">
      <?=$_SESSION['user']->nom;?> <?=$_SESSION['user']->pnom;?>
      <img src="./img/transparent.png" data-src="<?=$_SESSION["user"]->avatar;?>" class="ui circular image" style="margin:0 0 0 20px;height:36px;width:36px;border:0">

      <div class="menu">
        <a class="item" href="?p=utilisateur/profile&id=<?=$_SESSION['user']->id_user;?>">PROFIL</a>
        <a class="item" href="?p=feedback">SIGNALER UNE ANOMALIE</a>
        <a class="item" href="?p=upload" >DÉPOT DE FICHIER</a>
        <a class="item" target="_blank" href="https://imail/">E-Mail LOCAL</a>
        <?=isallow("programmer")||isallow("admin")?'<a class="item" href="http://192.168.5.81:81/COSIDER.CONSTRUCTION/GCom/wiki" target="Gitea"><i class="icon book"></i> Wiki</a>':''?>
        <?=isallow("programmer")||isallow("admin")?'<a class="item" href="?p=issue"><i class="icon exclamation"></i> issues</a>':''?>

        



        <a class="item" href="javascript:closeWindow();">DECONNEXION</a>
      </div>
    </div>
  </div>




</div>

<script>

  //document.oncontextmenu = RightMouseDown;
  //document.onmousedown   = mouseDown;
  function mouseDown(e) {
    //righClick
    if (e.which==3){
    }
  }
  function RightMouseDown() { return false;}

  function closeWindow() {
    if (confirm('Êtes-vous sûr ?')){
      $.get( "bin/logout.php", function(data){
        window.open('','_self','');
        window.close();
        self.close();
        location.reload();
      });
    }
  }

  function Gogs(){
    $('.ui.Gogs')
      .modal('show')
    ;
  }

</script>
