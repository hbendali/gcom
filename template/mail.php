<?php

  // Lib for MARKDOWN 
  //require_once('bin/parsedown.php');
  //$parsedown = new Parsedown();
  //$parsedown->setSafeMode(true);

//  if (isset($_GET['del']) && $_GET['del'] != ""){
//    $id_mail = sql_inj($_GET['del']);
//    $fw->fetchAll("UPDATE mail SET sts = 'del' WHERE id_mail = $id_mail;",true,true);
    // location.reload();
//    echo "<script type='text/javascript'>location='?p=mail';</script>"; exit;
//  }


  $mail = sql_inj($_GET['id'],0);
  // if ($mail){
  //   $mail = $fw->fetchAll("
  //     SELECT
  //       mail.*, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname, utilisateur.avatar
  //     FROM
  //       mail, utilisateur
  //     WHERE
  //       id_mail=$mail
  //     AND
  //       utilisateur.id_user = mail.sender
  //   ")[0];

  // }

  // if (!$mail){
  //   $mail = new \stdClass;
  //   $mail->id_mail  = null;
  //   $mail->sender   = null;
  //   $mail->receiver = null;
  //   //$mail->replayTo = null;
  //   $mail->fullname = null;
  //   $mail->obj      = null;
  //   $mail->core     = null;
  // }

?>

<div ng-controller="TodoCtrl" class="ui text form container">

  <div class="ui top attached secondary segment card">
    <div class="image">
      <img src="img/mail.jpg">
    </div>  
  </div>
  <div class="ui attached segment">

    <div ng-show="gotMail" ng-cloak>
      <div class="ui horizontal list">
        <div class="item">
          <img class="ui mini circular image" ng-src="{{gotMail.avatar}}">
          <div class="content">
            <div class="ui sub header">{{gotMail.fullname}}</div>
            {{gotMail.date_insert}}
          </div>
        </div>
      </div>

      <div class="ui info message">
        <div class="header">
          {{gotMail.obj}}
        </div>
        {{gotMail.core}}
      </div>
    </div>

    <div ng-hide="gotMail" ng-cloak>
      <div class="field">
        <select class="ui dropdown fluid" ng-model="mail.to">
          <option value='BAT'>BAT - BATIMENT</option>
          <option value='GC'>GC - GENIE CIVIL</option>
          <option value='DPI'>DPI - DÉPARTEMENT DE PRODUCTION ET INFRASTRUCTURE</option>
          <option value='CET'>C.E.T - CORPS D'ETAT TECHNIQUES</option>
          <option value='TC'>T.C - TECHNIQUO COMMERCIALE</option>
          <?php
          $list_sender = $fw->fetchAll("SELECT id_user, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname FROM utilisateur");
          foreach ($list_sender as $user){
            echo "<option value='$user->id_user'>$user->fullname</option>";
          }
          ?>
        </select>
      </div>

      <div class="field">
        <div class="ui fluid input">
          <input type="text" placeholder="Objet..." ng-model="mail.obj" maxlength="250">
        </div>
      </div>
    </div>
    <p></p>
    


    <div class="field">
      <textarea placeholder="Message ..." ng-model="mail.core"></textarea>
    </div>
    
    <div class='ui basic right aligned segment print_ignore'>
      
      <div class="ui button" ng-click="closeMail()" ng-show="gotMail.sender">
        <i class="icon remove"></i> Clôturer
      </div>
      
      <div class="ui button" ng-click="chMailSts('d')">
        <i class="icon remove"></i> Supprimer
      </div>
      
      <div class="ui button" ng-click="sendMail()">
        <i class="icon edit"></i> Envoyer
      </div>

    </div>
  </div>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $filter, $http) {
  
  $http.get('api/?mail=<?=$mail?>')
    .then(function(res){
      if (res.data.pdo && res.data.pdo.result){
        $scope.gotMail = res.data.pdo.result[0];
        console.log("Load Mail", $scope.gotMail);
        $scope.mail={
          id_mail : $scope.gotMail.id_mail,
          to      : $scope.gotMail.sender,
          obj     : '> '+$scope.gotMail.obj,
          core    : null
        }
      }

    });

  $scope.sendMail = function(){
    if ($scope.mail.to && $scope.mail.obj && $scope.mail.core){
      $http.post('api/?mail',$scope.mail)
        .then(function(r){
          console.log(r);
          if (r.data.pdo.sqlState == "00000" && r.data.pdo.id)
            location.assign("?");
        })
      ;
    }
  }

  $scope.closeMail = function(){
    if ($scope.mail.id_mail){
      $http.post('api/?mail',$scope.mail)
        .then(function(r){
          $scope.chMailSts('c');
        })
      ;
    }
  }

  $scope.chMailSts = function(z){

    if ($scope.mail.id_mail){
      $http.get('api/?mail='+$scope.mail.id_mail+'&<?=isset($_GET['ss'])?'ss':(isset($_GET['sr'])?'sr':'');?>='+z)
        .then(function(r){
          location.assign("?");
        })
      ;
    }else{
      location.assign("?");
    }
  }

});</script>