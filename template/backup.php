<div ng-controller="TodoCtrl"  class="ui container">
  
  <button class="ui labeled green icon button" ng-click="backup()">
    <i class="paperclip icon" ></i> Backup File
  </button>

<div class="ui five column grid">
  <a class="ui column label" ng-repeat="(key, value) in list" href="backup/{{value.file}}" target="_blanc" style="margin: 5px 10px;">
    <img class="ui right spaced tiny image" src="backup/{{value.file}}">
    {{value.basename}} {{value.date}} 
  </a>
</div>




  
</div>

<script language="javascript">
  app.controller('TodoCtrl', function($scope, $filter, $http) {

    // upload file whene input change 
    $scope.backup = function(){
      $http.get('api/?backup=zip');
    }

    $scope.loadlist = function(){
      $http.get('api/?backup=list')
        .then(function(res){
          $scope.list=res.data;
        })
      ;
    }

    $scope.loadlist();
  });
</script>