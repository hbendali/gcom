<div class="ui raised very padded text container piled red segment" ng-init="color='#ffffffff'">
  <h2 class="ui header">Ajouter une FAMILLE</h2> 

  <form method="post" action="ctrl/fam_add.php" class="ui form">

    <div class="inline fields">
      <div class="four wide field">
        <input type="text" placeholder="CODE" maxlength="10" ng-model="fam" name="fam" required>
      </div>
      <div class="twelve wide required field">
        <input type="text" placeholder="LIBELLE"  maxlength="255" ng-model="libelle" name="libelle" required>
      </div>

      <div class="field">
        <input type="color" class="ui circular label" ng-model="color" name="color" style="background-color:{{color}} !important" >
      </div>

      <div class="two wide field">
        <button class="ui teal button"> Ajouter </button>
      </div>
    </div>




    <hr class="style3"></hr>
    <table class="ui very basic table"

      ng-init="list=[
        <?php
          $result = $fw->fetchAll("SELECT * FROM fam");
          foreach ($result as $ele) {
            echo "{fam:'".$ele->fam."',libelle:'".$ele->libelle."',color:'".$ele->color."'},";
          }
        ?>
      ]"
    >
      <tr ng-repeat="ele in list | filter:fam">
        <td width='1'><a class="ui empty circular label" style="background-color:{{ele.color}} !important"></a></td>
        <td> {{ele.fam}} </td>
        <td> {{ele.libelle}} </td>
      </tr>
    </table>

  </form>
</div>

<script>
$('.dropdown').dropdown();
</script>