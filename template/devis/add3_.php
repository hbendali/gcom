<div ng-controller="TodoCtrl" ng-cloak>

  <div class="ui attached small steps print_ignore">
    <a class="step" href="?p=devis/add1&projet=<?=$num_sub_devis;?>">
      <i class="id card icon"></i>
      <div class="content">
        <div class="title">Projet</div>
        <div class="description">Informations du Projet</div>
      </div>
    </a>
    <a class="step red_border" href="?p=devis/add2&projet=<?=$num_sub_devis;?>">
      <i class="file excel icon"></i>
      <div class="content">
        <div class="title">Devis Quantitatif EXCEL</div>
        <div class="description">Analyse du Fichier Client</div>
      </div>
    </a>
    <a class="active step" href="?p=devis/add3&projet=<?=$num_sub_devis;?>">
      <i class="info icon"></i>
      <div class="content">
        <div class="title">Définition des frais</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step"  href="?p=devis/add5&projet=<?=$num_sub_devis?>"
       ng-class="{disabled:!sub_devis.etudes}">
      <i class="codepen icon"></i>
      <div class="content">
        <div class="title">ETUDES</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step" href="?p=devis/add4&projet=<?=$num_sub_devis;?>">
      <i class="calculator icon"></i>
      <div class="content">
        <div class="title">Estimation du DEVIS</div>
        <div class="description"></div>
      </div>
    </a>
  </div>

  <div class="ui raised container center aligned piled segment print_ignore" ng-cloak>
    <h1>En attente, Définition des frais !</h1>
  </div>

</div>

<script language="javascript">app.controller('TodoCtrl', function($scope, $filter, $http, $location){

  
});</script>