<div ng-controller="TodoCtrl">

  <div class="ui inverted menu">
    <a class="active red item" href="?p=devis/add0">
      <i class="add icon"></i> NOUVEAU PROJET
    <a class="right item">
      <div class="ui inverted transparent icon input">
          <input type="text" placeholder="Recherche ..." ng-model="search">
          <i class="search icon"></i>
      </div>
    </a>
    </a>
  </div>

  <div class="ui segments" ng-repeat="prj in projet_list | filter:search" ng-cloak>
    
      <div class="ui segment inverted">
        
        <div class="ui header">
          {{prj.nom_devis}}
        </div>
        <!--div class="ui header">{{prj.nom_devis}}</d-->
        
        
        {{prj.nom_client}} | {{prj.adresse}}
        <!--div class="ui label">{{prj.nature_prj}} / {{prj.department}}</di-->
      </div>
      
      <?php
        if ( isallow("programmer") || isallow("admin")  || isallow("user_plus") )
          echo '<a class="ui right red corner label" ng-click="delete_prj()" ><i class="remove icon"></i></a>'; ?>

      <div class="ui buttons">        
        <a class="ui basic button" href="?p=devis/add1&projet={{prj.num_devis}}-new"><i class="add icon"></i> Ajouter</a>
        <a class="ui basic button" href="?p=underc"><i class="file word icon"></i> Document d'accompagnement</a>
      </div>

      <div class="ui segment">
        <div class="ui cards" >
          <div class="ui card" ng-repeat="subdv in prj.sub_devis" >
            <?php
              if ( isallow("programmer") || isallow("admin")  || isallow("user_plus") )
                echo '<a class="ui right corner label" ng-click="delete_prj()"><i class="remove icon"></i></a>'; ?>

            <a class='content' href='?p=devis/add{{subdv.etat}}&projet={{subdv.num_devis}}'>
              <div class="ui small header">
                {{subdv.obj_devis}}
              </div>
            </a>
            
            <div class="content">
              <div class="ui mini aligned divided list">
                <div class="item" ng-repeat="user in subdv.g_user"
                  <?php
                    if ( isallow("programmer") || isallow("admin")  || isallow("user_plus") )
                      echo 'ng-dblclick="chuser(subdv,this.user,\'remove\')"'; ?>
                >
                  <img class="ui avatar image" src="{{user.avatar}}">
                  <div class="content">
                    <a class="header">{{user.nom}} {{user.pnom}}</a>
                    {{user.ch}}
                  </div>
                </div>
              </div>
            </div>

            <a class="ui mini button" ng-click="show_model(this);"><i class="add icon"></i> Ajouter un contributeur </a>
            <!--a class='ui mini left button' href='?p=underc'><i class="paperclip icon"></i> Document d'accompagnement </a-->
          </div>
        </div>
      </div>
  </div>
  
  <div class="ui mini modal user_details">
    <div class="header">Utilisateur</div>
    <div class="content">
      <p>

        <div class="ui fluid search selection dropdown">
          <input type="hidden" id="user">
          <i class="dropdown icon"></i>
          <div class="default text">Selectioner un utilisateur</div>
          <div class="menu">
            <div class="item" data-value="{{value.id_user}}" ng-repeat="(key, value) in user_list | filter:{acl:{<?=$fw->getGroup($_SESSION['user']->id_user)?>:true}}">
              <!--img class="ui avatar image" src="{{value.avatar}}"-->
              <b>{{value.nom}} {{value.pnom}}</b> <br> {{value.ch}}
            </div>
          </div>
        </div>

      </p>
    </div>
    <div class="actions">
      <div class="ui approve button" ng-click="chuser(selected_sdv,this,'add')">Ajouter</div>
      <div class="ui cancel button">Cancel</div>
    </div>
  </div>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $http) {

  $scope.load_prj_list = function () {
    $http.get('api/?get=987535')
    .then(function(res){
      $scope.projet_list = res.data;
      //console.log('project list',$scope.projet_list);

      $scope.projet_list.forEach( function(prj, index) {
        //console.log('prj 1', prj);
        prj.sub_devis.forEach( function(devis, index) {
          //console.log('devis 2', devis);
          devis.g_user = [];
          if (devis.group_utilisateur && $.isArray(devis.group_utilisateur) )
          devis.group_utilisateur.forEach( function(user, index) {
            devis.g_user.push($scope.user_list.find( f => f.id_user === user) );
          });
        });
      });

    });
  }



  $http.get('api/?list=utilisateur&active')
  .then(function(res){
    res.data.forEach(function(e){
      e.acl = JSON.parse(e.acl);
    });
    $scope.user_list = res.data;
    console.log('user list',$scope.user_list)
    $scope.load_prj_list();
  });

  $scope.show_model = function(id){
    $scope.selected_sdv = id.subdv;
    $('.modal.user_details').modal('show');
  }

  $scope.chuser = function(dv,user,ch){
    
    let ndx = this.$index;
    if ( ( ch=='add' ) || ( ch=='remove' && confirm('voulez vous supprimer les droits d\'accès à cette utilisateur ('+user.nom+' '+user.pnom+')?') ) ){
      
      if ( ch=='add' ){
        user = $scope.user_list.find( f => f.id_user === $('#user').val() );
      }

      $http.post('api/?get=982175&'+ch, {id_devis:dv.num_devis, id_user:user.id_user})
        .then(function (e) {
          if ( ch=='add' && e.data==1 )
            $scope.selected_sdv.g_user.push( angular.copy(user) );
          else if ( ch=='remove' && e.data==1 ){
            dv.g_user.splice(ndx, 1);
          }
          console.log('e.data', e.data);
        });

      //console.log('ok', user);
    }
  }

  $scope.delete_prj = function(){
    let num_devis = this.subdv === undefined ? this.prj.num_devis : this.subdv.num_devis ;
    if ( confirm('Êtes-vous sûr de vouloir supprimer ce projet ' + ( this.subdv === undefined ? 'est c\'est sous projet' : '' ) + ' ?') ){
      $http.post('api/?get=524892&', {prj_dv: this.subdv === undefined ? 'projet' : 'devis' , num_devis:num_devis})
        .then(function (e) {
          if (e.data==1){
            $scope.load_prj_list();
            //location.reload();
          }

        });
    }
  }


}); </script>
