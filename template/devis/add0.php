<?php

$num_sub_devis = sql_inj($_GET['projet'],'');
list($num_devis,) = explode('-', $num_sub_devis);

$dv = null;
if ($num_devis)
  $dv = $fw->fetchAll("SELECT * FROM projet WHERE num_devis='$num_devis'");
else
  $num_devis = $fw->next_id('projet');

$dv = !empty($dv) ? $dv[0] : (object)[
  "num_devis"=>$num_devis,
  "nom_devis"=>null,
  "obj_devis"=>null,
  "num_client"=>null,
  "adresse"=>null,
  "mode_pass"=>null,
  "nsub_devis"=>1,
  "utilisateur"=>$_SESSION['user']->id_user,
  "group_utilisateur"=>null,
  "nature_prj"=>$fw->getGroup($_SESSION['user']->id_user),
  "query"=>"insert"
];

// echo "<pre>";
// print_r($dv);
// echo "</pre>";
?>
<div ng-controller="TodoCtrl">

  <div class="ui attached small steps">
    <a class="active red_border step" href="?p=devis/add1&projet=<?=$num_sub_devis;?>">
      <i class="id card icon"></i>
      <div class="content">
        <div class="title">Projet</div>
        <div class="description">Informations du Projet</div>
      </div>
    </a>
    <a class="disabled step" href="?p=devis/add2&projet=<?=$num_sub_devis;?>">
      <i class="file excel icon"></i>
      <div class="content">
        <div class="title">Devis Quantitatif EXCEL</div>
        <div class="description">Analyse du Fichier Client</div>
      </div>
    </a>
    <a class="disabled step" href="?p=devis/add3&projet=<?=$num_sub_devis;?>">
      <i class="info icon"></i>
      <div class="content">
        <div class="title">Définition des frais</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="disabled step" href="?p=devis/add4&projet=<?=$num_sub_devis;?>">
      <i class="calculator icon"></i>
      <div class="content">
        <div class="title">Estimation du DEVIS</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="disabled step"  href="?p=devis/add5&projet=<?=$num_sub_devis?>"
       ng-class="{disabled:!sub_devis.etudes}">
      <i class="codepen icon"></i>
      <div class="content">
        <div class="title">ETUDES</div>
        <div class="description"></div>
      </div>
    </a>
  </div>





  <div class="ui raised very padded text container piled segment" ng-cloak>

    <div class="ui right dividing rail">
      <div class="ui mini celled list">
        
        <?php

          $user = $fw->getUser($dv->utilisateur);
          echo "
        <div class='item' style='margin: 7px; padding: 7px;' ng-cloak>
          <img class='ui avatar image' src='$user->avatar'>
          <div class='content'>
            <div class='header'>$user->nom $user->pnom</div>
            $user->ch
          </div>
        </div>";

          $contributeur = json_decode( str_replace( '\"', '"', sql_inj($dv->group_utilisateur ) ) );
          if ($contributeur){
            foreach ($contributeur as &$value) {
              $user = $fw->getUser($value);
              echo "
        <div class='item' style='margin: 7px; padding: 7px;' ng-cloak>
          <img class='ui avatar image' src='$user->avatar'>
          <div class='content'>
            <div class='header'>$user->nom $user->pnom</div>
            $user->ch
          </div>
        </div>";        
            }
          }

          if ( isallow("programmer") || isallow("admin") || isallow("user_plus") ){
            echo "</div><div align='center'><a class='circular ui icon button mini basic' href='?p=devis/devis_user_rule'><i class='icon circular user link '></i> Ajouter un collaborateur </a>";
          }

        ?>
      </div>
    </div>












    <form method="post" class="ui form" name="form" id="form" ng-submit="form.$valid && submit()">
      <div class="equal width fields">
        <div class="four wide field" ng-class="{error:form.num_devis.$invalid}">
          <label>NUMERO</label>
          <input 
          type="text" 
          name="num_devis" 
          ng-model="devis.num_devis" 
          pattern="[A-Z,a-z,0-9]*"
          maxlength="20"  
          class="uppercase" 
          readonly>
        </div>

        <div class="field" ng-class="{error:form.nom_devis.$invalid}">
          <label>INTITULÉ</label>
          <input type="text" ng-model="devis.nom_devis" name="nom_devis" maxlength="255" required>
        </div>
      </div>

      <div class="equal width fields">
        <div class="field" ng-class="{error:form.obj_devis.$invalid}">
          <label>Objet du marché</label>
          <input type="text" ng-model="devis.obj_devis" name="obj_devis" maxlength="255" required>
        </div>
      </div>

      <div class="equal width fields">
        <div class="field"  ng-class="{error:form.num_client.$invalid}">
          <label>CLIENT</label>
          <select class="ui dropdown" ng-model="devis.num_client" ng-options="obj.num_client as obj.num_client + ' / ' + obj.nom_client for obj in client" required><option value="" selected>--choisir un client--</option></select>
        </div>
      </div>

      <div class="field" ng-class="{error:form.adresse.$invalid}">
        <label>ADRESSE DU PROJET</label>
        <textarea ng-model="devis.adresse" name="adresse" maxlength="65535" rows="4" required></textarea>
      </div>

      <div class="field" ng-class="{error:form.mode_pass.$invalid}">
        <label>Mode de passation</label>
        <select class="ui dropdown" ng-model="devis.mode_pass" ng-options="item for item in mode_pass" required><option value="" selected>--choisir un mode de passation--</option></select>
<!--         <select class="ui dropdown" ng-model="devis.mode_pass" ng-options="obj.value as obj.label for obj in mode_pass" required><option value="" selected>--choisir un mode de passation--</option></select>
 -->      </div>

      <div class="ui negative message" ng-show="msg_error">
        <i class="close icon"></i>
        <div class="header">
          Erreur 
        </div>
          <p>{{msg_error}}</p>
      </div>

      <div class='ui basic right aligned segment'>
        <button class="ui teal button"
                type="submit" 
                ng-class="{
                  disabled: form.num_devis.$invalid  || 
                            form.nom_devis.$invalid  ||
                            form.obj_devis.$invalid  ||
                            form.num_client.$invalid ||
                            form.adresse.$invalid    ||
                            form.mode_pass.$invalid
                }">
          <i class="arrow right icon"></i>
          Enregistrer 
        </button>
      </div>


    </form>
  </div>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $filter, $http) {
  // INIT || LOAD /////
  $scope.client = <?=json_encode($fw->fetchAll("SELECT num_client,nom_client FROM client"));?>;
  //$scope.mode_pass=[{value:'1',label:'Gré à Gré'},{value:'2',label:'Appel d\'offres'},{value:'3',label:'Consultation'}];
  $scope.mode_pass=['Gré à Gré','Appel d\'offres','Consultation'];
  $scope.devis = <?=json_encode($dv);?>;
  // $('#num_client').val($scope.devis.num_client);
  // $('#mode_pass').val($scope.devis.mode_pass);

  // SAVE /////////////
  $scope.save = function(){
    return $http.post('api/?draft='+$scope.devis.num_devis+'&save',$scope.devis)
      .then(function(res){
        console.log('Save');
      });
  }

  // NEXT /////////////
  $scope.submit = function(){
    $scope.msg_error = null;
    $scope.devis.num_devis  = $scope.devis.num_devis.toUpperCase();

    if( $('.ui.form').form('is valid') ){
      $scope.save()
        .then(function(){
          $http.post('api/?update=step0',$scope.devis)
            .then(function(r){
              console.log('result',r);
              if (r.data.res == 'done!')
                location.assign("?p=devis/add1&projet="+$scope.devis.num_devis+'-new');
              else
                $scope.msg_error = r.data.pdo.message;
              
            });
        });
    }
  }
  /////////////////////

}); 

$('.ui.checkbox').checkbox();
$('.ui.radio.checkbox').checkbox();

</script>