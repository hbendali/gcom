<?php
$etat      = sql_inj($_GET['e'],null);
$maj       = sql_inj($_GET['maj'],null);
$num_devis = sql_inj($_GET['num_devis'],null);


/*

  List status project
  ====================================
  0:  Entitulé du projet
  1:  Entitulé de sous devis
  2:  Cahier de charge client, (excel)
  3:  Définition des frais
  4:  Estimation et Analyse
  5:  etude
  6:  gagnée
  7:  perdue
  99: Project supprimé

*/


if (( $maj=="6" || $maj=="7" ) && $num_devis)
{
  $fw->fetchAll("UPDATE projet SET etat=$maj WHERE num_devis = '$num_devis'",true,true);
}

?>
<div ng-controller="TodoCtrl" >
  
  <table class="ui red selectable link compact table" ng-cloak>
    <thead>
      <tr>
        <th colspan="8">
          <div class="ui left aligned">
            <h2 class='ui left floated'>LISTE DES PROJETS</h2>
          </div>
          <div class="ui right aligned">

            <a class='ui mini icon button'
              data-tooltip="Filtré sur les Offre Gagnée"
              data-inverted=""
              data-position="bottom center"
              ng-click="offre='6'"
              >
              <i class='thumbs up icon'></i>
              Offres Gagnées
            </a>
            <a class='ui mini icon button'
              data-tooltip="Filtré sur les Offre Perdue"
              data-inverted=""
              data-position="bottom center"
              ng-click="offre='7'"
              >
              <i class='thumbs down icon'></i>
              Offres Perdues
            </a>
            <a class='ui mini icon button'
              data-tooltip="Filtré sur tous les Offre"
              data-inverted=""
              data-position="bottom center"
              ng-click="offre=''"
              >
              <i class='help icon'></i>
              Toutes les Offres
            </a>
            <div class="ui mini icon input">
              <input type="text" placeholder="Recherche ..." ng-model="p_tache" >
              <i class="search icon"></i>
            </div>
          </div>
        </th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="ele in devis | filter:p_tache | filter:{etat:offre} <?=$etat?"| filter:{etat:'$etat'}":''; ?>" ng-init="$last ? rebuild_dropdown() : null">
        <td>
          {{ele.num_devis}}
          <div class="ui dropdown" ng-show="ele.etat != '6' && ele.etat != '7' ">
            <i class="dropdown icon"></i>
            <div class="menu">
              <a href="?p=devis/list&maj=6&num_devis={{ele.num_devis}}" class="item"><i class="thumbs outline up icon"></i> Offre Gagnée</a>
              <a href="?p=devis/list&maj=7&num_devis={{ele.num_devis}}" class="item"><i class="thumbs outline down icon"></i> Offre Perdue</a>
            </div>
          </div>
        </td>
        <td>{{ele.nom_devis}}</td>
        <td>{{ele.nom_client}}</td>
        <td>{{ele.adresse}}</td>
        <!--td><a href="uploads/{{ele.fichier_excel}}.xlsx">{{ele.fichier_excel}}<span ng-if="ele.fichier_excel">.xlsx</span></a></td-->
        <td>{{ele.date_devis | date:'dd/mm/yyyy'}}</td>
        <td>
          <div ng-if="ele.etat == 1"><a href="?p=devis/add1&projet={{ele.num_devis}}"></a></div>
          <div ng-if="ele.etat == 2"><a href="?p=devis/add2&projet={{ele.num_devis}}">Cahier de charge client, (excel)</a></div>
          <div ng-if="ele.etat == 3"><a href="?p=devis/add3&projet={{ele.num_devis}}">Définition des frais</a></div>
          <div ng-if="ele.etat == 4"><a href="?p=devis/add4&projet={{ele.num_devis}}">Estimation et Analyse</a></div>
          <div ng-if="ele.etat == 5"><a href="?p=devis/add5&projet={{ele.num_devis}}">Etudes</a></div>
          <div ng-if="ele.etat == 6"><a href="?p=devis/view6&projet={{ele.num_devis}}">Offre gagnée</a></div>
          <div ng-if="ele.etat == 7"><a href="?p=devis/view7&projet={{ele.num_devis}}">Offre perdue</a></div>
        </td>
        <td>

        </td>
        <!-- <td class="right aligned">
          <a
            class="ui mini basic icon button"
            data-tooltip="Modifier {{ele.nom_devis}}"
            data-inverted=""
            data-position="left center"
            href="?p=projet/add<?=$etat?$etat:''; ?>&num_devis={{ele.num_devis}}"
            >
            <i class='write icon'></i>
          </a>

        </td> -->
      </tr>
    </tbody>
  </table>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $http) {

  $http.get('api/?list=projet&w=etat&lt=99')
    .then(function(res){
      $scope.devis = res.data;
      console.log("devis", $scope.devis);
    });

  $scope.rebuild_dropdown = function(){
    $('.dropdown')
      .dropdown();
  }

}); </script>