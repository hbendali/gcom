<?php

$num_sub_devis = sql_inj($_GET['projet'],'');
list($num_devis,$diff) = explode('-', $num_sub_devis);

if (!$num_sub_devis){
  $_GET['err']='Erreur 404';
  $_GET['msg']='Projet non trouver';
  include("template/err.php");
  die();
}

$target_file = null;
if(isset($_POST["submit"])){
  $target_file = uniqid();
  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "./uploads/xls/".$target_file.".xlsx")){
    // update prj -> fichier_excel
    $fw->fetchAll("UPDATE devis SET fichier_excel = '$target_file' WHERE num_devis = '$num_sub_devis'");
  }else{
    $target_file = null;
  }
}
?>
<div ng-controller="TodoCtrl" ng-cloak>
  <?php if (isallow("debug")) echo DEBUG_BUTTON;?>
  <div class="ui fixed bottom sticky">
    <div class="ui image label" >
      <i class="hotjar icon"></i>
      {{sub_devis.num_devis}}
      <div class="detail">{{sub_devis.obj_devis}}</div>
    </div>
  </div>
  <div class="ui attached small steps print_ignore">
    <a class="step" href="?p=devis/add1&projet=<?=$num_sub_devis;?>">
      <i class="id card icon"></i>
      <div class="content">
        <div class="title">Projet</div>
        <div class="description">Informations du Projet</div>
      </div>
    </a>
    <a class="active red_border step" href="?p=devis/add2&projet=<?=$num_sub_devis;?>">
      <i class="file excel icon"></i>
      <div class="content">
        <div class="title">Devis Quantitatif EXCEL</div>
        <div class="description">Analyse du Fichier Client</div>
      </div>
    </a>
    <a class="step" href="?p=devis/add3&projet=<?=$num_sub_devis;?>">
      <i class="info icon"></i>
      <div class="content">
        <div class="title">Définition des frais</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step"  href="?p=devis/add5&projet=<?=$num_sub_devis?>"
       ng-class="{disabled:!sub_devis.dv_etudes}">
      <i class="codepen icon"></i>
      <div class="content">
        <div class="title">ETUDES</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step" href="?p=devis/add4&projet=<?=$num_sub_devis;?>">
      <i class="calculator icon"></i>
      <div class="content">
        <div class="title">Estimation du DEVIS</div>
        <div class="description"></div>
      </div>
    </a>
  </div>

<!-- <div class="ui red icon button print_ignore" ng-click="dbg()"><i class="code icon"></i></div>
 -->

  <datalist id="tache">
    <option value="IGNORER"></option>
    <option value="LOT"></option>
    <option value="TOTAL LOT"></option>
    <option value="SOUS LOT"></option>
    <option value="TOTAL SOUS LOT"></option>
    <option value="COMPOSITION TACHE"></option>
    <option value="INSTALLATION CHANTIER"></option>
    <option value="NOTIFICATION TACHE"></option>
    <option value="NOTIFICATION LOT"></option>
    <option value="ETUDES"></option>
    <option value="ETUDES ESQUISSE"></option>
    <option value="ETUDES AVANT PROJET"></option>
    <option value="ETUDES PROJET EXECUTION"></option>
    <option value="ETUDES PLAN DE RECOLLEMENT"></option>
    <option value="ETUDES PERMIS DE CONSTRUIRE"></option>
    <option value="TOTAL ETUDES HT"></option>
    <option value="TOTAL ETUDES TVA"></option>
    <option value="TOTAL ETUDES TTC"></option>
    <option value="TOTAL REALISATION HT"></option>
    <option value="TOTAL REALISATION TVA"></option>
    <option value="TOTAL REALISATION TTC"></option>
    <option value="TOTAL ETUDES ET REALISATION HT"></option>
    <option value="TOTAL ETUDES ET REALISATION TVA"></option>
    <option value="TOTAL ETUDES ET REALISATION TTC"></option>
    <option ng-repeat="ele in tache" value="{{ele.nom_tache}}">{{ele.num_tache}}</option>
  </datalist>
<!--div ng-click='debug_()' class='ui youtube button' ng-class='save.loading'><i class="bug icon"></i> View $scope.devis</div-->

  <div class="ui raised container piled segment print_ignore" ng-cloak>
    <form method="post" action="" class="ui form" enctype="multipart/form-data">
      <div class="field">

        <label>Fichier attaché</label>
        <div class="ui action input">
          <input  type="file"
                  name="fileToUpload"
                  id="fileToUpload"
                  accept=".xlsx"
                  onchange="angular.element(this).scope().fileNameChanged(this);"
                  >
          <button class="ui icon button" type="submit" name="submit" data-tooltip="Importation Fichier Excel" data-inverted="" ng-class="{red:fileIsReady}"><i class="attach icon"></i>{{fileIsReady}}</button>
        </div>
      </div>
      <div class="ui negative message" ng-show="msg_error">
        <i class="close icon"></i>
        <div class="header">
          Erreur 
        </div>
          <p>{{msg_error}}</p>
      </div>

    </form>

    <p>&nbsp;</p>
    <!-- <div ng-show="sub_devis.fichier_excel && sub_devis.xls"> -->
    <div ng-show="sub_devis.xls">
      <a class="ui red ribbon label" href='./uploads/xls/{{sub_devis.xls_file}}.xlsx'>
        <i class="file excel outline icon"></i>
        {{sub_devis.xls_file}}.xlsx
      </a>

      <!--div class="item">
        <div class="ui checkbox">
          <input type="checkbox" id="bpu"ng-model="sub_devis.bpu">
          <label for="bpu">Devis avec BPU attaché</label>
        </div>
      </div-->

      <div class='ui basic right aligned segment'>
        <div ng-click="composeMail()" class='ui button' >
          <i class="envelope outline icon"></i> Envoi Notifications</div>
        <div onclick="$('.show_db').modal('show');" class='ui button' >
          <i class="book icon"></i> Consultation BDD</div>
        <div ng-click='auto()' class='ui button' ng-class='auto_loading' >
          <i class="tasks icon"></i> Correspondance Automatique</div>
        <div ng-click='init()' class='ui button' >
          <i class="remove icon"></i> Effacer</div>
        <div ng-click='save()' class='ui button' ng-class='save.loading' >
          <i class="save  icon"></i> Enregistrer brouillon</div>
        <div ng-click='recap()' class='ui button teal' ng-class='next_loading' >
          <i class="arrow right icon"></i> Suivant</div>
      </div>
    </div>

  </div>

  <table class='ui selectable compact celled table' ng-show="sub_devis.xls.length" ng-cloak>
    <thead><tr>
      <th style="background-color: #ccc" title="{{chr(sub_devis.header.ref)}}">N</th>
      <th style="background-color: #ccc" title="{{chr(sub_devis.header.des)}}" width='50%'>Designation</th>
      <th style="background-color: #ccc" title="{{chr(sub_devis.header.um)}}">Um</th>
      <th style="background-color: #ccc" title="{{chr(sub_devis.header.qte)}}" class="right aligned">Qte</th>
      <th style="background-color: #ccc">Code tache</th>
      <th style="background-color: #ccc">Um</th>
      <th style="background-color: #ccc" width='50%'>Correspondance</th>
      <th style="background-color: #ccc">Coût U Sec</th>
      <th style="background-color: #ccc" title="{{chr(sub_devis.header.mnt)}}">Montant</th>
    </tr></thead>
    <tbody>
      <tr ng-repeat="ele in sub_devis.xls" 
          ng-class="{
            negative:   (!ele.nArticle && !ele.matching),

            warning:      ele.matching == 'LOT' ||
                          ele.matching == 'SOUS LOT' ||
                          ele.matching == 'IGNORER',

            positive:     ele.matching == 'TOTAL LOT' ||
                          ele.matching == 'TOTAL SOUS LOT' ||
                          ele.matching == 'TOTAL REALISATION HT',

            yellow_bg:    ele.matching == 'NOTIFICATION LOT' ||
                          ele.matching == 'NOTIFICATION TACHE',

            print_ignore: ele.matching == 'IGNORER'
          }"
          ng-cloak>
        <td>{{ele.nArticle}}</td>
        <td>{{ele.designation}}</td>
        <td>{{ele.um}}</td>
        <td style="border-right: 3px solid #f33" class="right aligned"><div ng-if="ele.nArticle || ele.qte">{{ele.qte | number:2}}</div></td>
        <td style="border-left: 3px solid #f33">{{ele.num_tache}}</div></td>
        <td>{{ele.um_cos}}</div></td>
        <td style="padding:0px 5px;">
          <div class="ui transparent fluid input">
            <input  type='text'
                    list='tache'
                    ng-model='ele.matching'
                    ng-change='search_complete()'
                    ng-dblclick='clr_this()'
              >
          </div>
        </td>
        <td class="right aligned">
            <element ng-if="ele.um_cos == 'M3'">{{ele.deb_sec | number:3}}</element>
            <element ng-if="ele.um_cos != 'M3'">{{ele.deb_sec | number:2}}</element>
        </td>
        <td class="right aligned">{{ele.mnt     | number:2}}</td>
      </tr>
    <tbody>
  </table>

<!--
ooooooooooooo            .o8       oooo                 .oooooo.       .            oooo
8'   888   `8           "888       `888                d8P'  `Y8b    .o8            `888
     888       .oooo.    888oooo.   888   .ooooo.     888          .o888oo oooo d8b  888
     888      `P  )88b   d88' `88b  888  d88' `88b    888            888   `888""8P  888
     888       .oP"888   888   888  888  888ooo888    888            888    888      888
     888      d8(  888   888   888  888  888    .o    `88b    ooo    888 .  888      888
    o888o     `Y888""8o  `Y8bod8P' o888o `Y8bod8P'     `Y8bood8P'    "888" d888b    o888o
-->

  <div class="ui modal recap">
    <div class="header">Tableau de Contrôle </div>
    <div class="scrolling content">
      <ul class="ui list">
        <li><b>[ {{sub_devis.nbr_lot}} ]</b> LOT
          <ul>
            <li ng-repeat="ele in sub_devis.lot">{{ele.ref}}, {{ele.des}}</li>
          </ul>
        </li>
        <li><b>[ {{sub_devis.nbr_slot}} ]</b> SOUS LOT
          <ul>
            <li ng-repeat="ele in sub_devis.slot">{{ele.des}}</li>
          </ul>
        </li>
        <!--li><b>{{sub_devis.total_deb_sec | number:2}}</b> DEBOURSÉ SEC</li-->

        <div ng-show="msg_error_recap.length">
          <hr class="style3"></hr>
          <ul style="color: #F00;">
            <li ng-repeat="ele in msg_error_recap">{{ele}}</li>
          </ul>
        </div>
        <div ng-show="sub_devis.nack_task">
          <ul style="color: #F00;">
            <li><b>[ {{sub_devis.nack_task}} ] Element n'a pas été défini !!!</b></li>
          </ul>
        </div>



      </ul>
    </div>
    <div class="actions">
      <div class="negative ui button">Annuler</div>
      <div class="positive ui button" ng-click='next()'>Suivant</div>
    </div>
  </div>

<!--
  .oooooo.                                                              o8o      .    o8o
 d8P'  `Y8b                                                             `"'    .o8    `"'
888           .ooooo.  ooo. .oo.  .oo.   oo.ooooo.   .ooooo.   .oooo.o oooo  .o888oo oooo   .ooooo.  ooo. .oo.
888          d88' `88b `888P"Y88bP"Y88b   888' `88b d88' `88b d88(  "8 `888    888   `888  d88' `88b `888P"Y88b
888          888   888  888   888   888   888   888 888   888 `"Y88b.   888    888    888  888   888  888   888
`88b    ooo  888   888  888   888   888   888   888 888   888 o.  )88b  888    888 .  888  888   888  888   888
 `Y8bood8P'  `Y8bod8P' o888o o888o o888o  888bod8P' `Y8bod8P' 8""888P' o888o   "888" o888o `Y8bod8P' o888o o888o
                                          888
                                         o888o

-->

  <div class="ui modal composition" id="composition">
    <div class="ui header">
      <div class="print_ignore">Créer une Composition</div>
      <div class="print_only">Composition d'une Tache </div>
      <div class="sub header">
        {{sub_devis.xls[compose].nArticle}}, {{sub_devis.xls[compose].designation}}
      </div>
    </div>
    <div class="ui scrolling content form">

      <table class="ui very compact fixed single line celled table">
        <thead>
          <tr>
            <th width="50%">Tache</th>
            <th>Prix U</th>
            <th>Qte</th>
            <th>Montant SEC</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="ele in sub_devis.comp[compose]" ng-dblclick="del_comp()">
            <td >
              <div class="ui transparent fluid input print_ignore">
                <input type="text" class="" ng-model="ele.tch" ng-change="srh_comp()" list="tache" >
              </div>
              <div class="print_only">{{ele.tch}}</div>
            </td>
            <td class="right aligned">
              {{ele.pru | number:2}}
            </td>
            <td class="center aligned">
              <div class="ui transparent fluid input print_ignore">
                <input type="number" class="" ng-model="ele.qte" ng-change="ele.mnt = ele.pru * ele.qte; calc_comp(compose)">
              </div>
              <div class="print_only">{{ele.qte}}</div>
            </td>
            <td class="right aligned">
              {{ele.mnt | number:2}}
            </td>
          </tr>
        </tbody>
        <tfoot>
            <th colspan="3"><b>Montant de cette composition</b></th>
            <th class="right aligned">
              <div class="ui tiny statistic"><div class="value">{{sub_devis.comp_mnt[compose] | number:2}}</div></div>
            </th>
          </tr>
        </tfoot>
      </table>

      <div class="ui right floated mini icon button print_ignore" ng-click="ele_comp()">
        <i class='add icon'></i>
      </div>
      
    </div>
    <div class="actions print_ignore">
      <div class="         ui button" ng-click="exportToPrint('#composition')">Imprimer</div>
      <div class="positive ui button" ng-click="set_comp()">Terminer</div>
    </div>
  </div>

<!--

Dislay TAC
-->

  <div class="ui modal show_db">
    <div class="header">BANQUE DE DONNEES</div>
    <div class="ui content form">
      <div class="field">
        <div class="ui fluid input">
          <input type="text" placeholder="Recherche..." ng-model="fdb">
        </div>
      </div>
      <table class="ui very basic compact table">
        <thead>
          <tr>
            <th>REF</th>
            <th>DESIGNATION</th>
            <th>COUT SEC</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="ele in tache | filter:fdb">
            <td>{{ele.num_tache}}</td>
            <td>{{ele.nom_tache}}</td>
            <td align="right">{{ele.deb_sec}}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

<!--
ooo        ooooo  .oooooo..o   .oooooo.
`88.       .888' d8P'    `Y8  d8P'  `Y8b
 888b     d'888  Y88bo.      888
 8 Y88. .P  888   `"Y8888o.  888
 8  `888'   888       `"Y88b 888     ooooo
 8    Y     888  oo     .d8P `88.    .88'
o8o        o888o 8""88888P'   `Y8bood8P'

-->

  <div class="ui modal mail">
    <div class="header">Notifier une Tâche</div>
    <div class="ui scrolling content form">

      <div class="field">
        <select class="ui dropdown fluid"  ng-model="mail.to">
          <option value="">Envoyer un message à</option>
          <option value='BAT'>BATIMENT</option>
          <option value='CET'>C.E.T</option>
          <option value='DPI'>D.P.I</option>
          <option value='GC'>G.C</option>
          <?php
          $list_sender = $fw->fetchAll("SELECT id_user, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname FROM utilisateur");
          foreach ($list_sender as $user) {
            echo "<option value='$user->id_user'>$user->fullname</option>";
          }
          ?>
        </select>
      </div>

      <div class="field">
        <div class="ui fluid input">
          <input type="text" placeholder="Objet..." ng-model="mail.obj">
        </div>
      </div>

      <div class="field">
        <textarea ng-model="mail.msg" placeholder="Message ..."></textarea>
      </div>
      
    </div>
    <div class="actions">
      <div class="ui blue labeled submit icon button" ng-click="sendMail()">
        <i class="icon edit"></i> Envoyer
      </div>
    </div>
  </div>

<!--
   oooo  .oooooo..o
   `888 d8P'    `Y8
    888 Y88bo.
    888  `"Y8888o.
    888      `"Y88b
    888 oo     .d8P
.o. 88P 8""88888P'
`Y888P

-->

</div>
<script src="./js/natural.js"></script>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $interval, $filter, $http) {
  var th_cmp      ={
    tch:null,
    des:null,
    pru:0,
    qte:0,
    mnt:0
  }
  
  // LOAD PRJ //////////
  $http.get('api/?draft=<?=$num_devis?>&load')
    .then(function(res){
      $scope.devis = res.data;
      console.log("Load Devis <?=$num_devis?>", $scope.devis);
    });

  // LOAD SUB PRJ //////////
  $http.get("api/?draft=<?=$num_sub_devis?>&load")
    .then(function(res){
      if (res.data.num_devis){
        $scope.sub_devis = res.data;
        $scope.sub_devis.query = 'update';
        <?php
          if ($target_file){
echo <<<load_xls_file
        \$http.get("api/?xlsx=$target_file")
          .then(function(res){
            \$scope.sub_devis.xls_file   = '$target_file';
            \$scope.sub_devis.xls        = res.data.xls;
            \$scope.sub_devis.header     = res.data.header;
            \$scope.sub_devis.bpu        = res.data.bpu;
            \$scope.sub_devis.SheetNames = res.data.listSheetNames;
            console.log('Load xls',\$scope.sub_devis);
            \$scope.recap(true);
            \$scope.sub_devis.comp       =[];
            \$scope.sub_devis.comp_mnt   =[];
          });
load_xls_file;
          }
        ?>
        console.log('Load Sub Devis: ',$scope.sub_devis);
      }
    });


/*
  ooo        ooooo       .o.       ooooo ooooo
  `88.       .888'      .888.      `888' `888'
   888b     d'888      .8"888.      888   888
   8 Y88. .P  888     .8' `888.     888   888
   8  `888'   888    .88ooo8888.    888   888
   8    Y     888   .8'     `888.   888   888       o
  o8o        o888o o88o     o8888o o888o o888ooooood8

*/

  $scope.composeMail = function(){

    var obj='Tâche inexistante dans la Banque de Données.';
    var mail='';
    var l='';
    var tmpmail='';

    $scope.sub_devis.xls.forEach(function (z) {
      l = (z.nArticle?z.nArticle:'')+' '+(z.designation?z.designation:'');
      if (z.matching == 'LOT'){
        tmpmail = l+' :\r';
      }else if (z.matching == 'NOTIFICATION LOT'){
        mail += tmpmail+'---------------------------------------------------------------\r';
      }else if (z.matching == 'NOTIFICATION TACHE'){
        mail += l+'\r---------------------------------------------------------------\r';
      }else{
        tmpmail += l+'\r';
      }
    });

    $scope.mail = {to:'CET', obj:obj ,msg:mail };
    $('.ui.modal.mail').modal('show');
  }
  $scope.sendMail = function(){
    $http.post('api/?mail',$scope.mail)
      .then(function(){
        console.log('mail sent', $scope.mail)
        $('.ui.modal.mail').modal('hide');
        $scope.mail = null;
      })
    ;
  }


  // SAVE /////////////
  $scope.save = function(){
    return $http.post('api/?draft=<?=$num_sub_devis?>&save',$scope.sub_devis)
      .then(function(res){
        console.log('Save '+$scope.sub_devis.num_devis);
      });
  }

  // NEXT /////////////
  $scope.next = function(){
    $scope.save()
      .then(function(){
        $http.post('api/?update=step2',{devis:$scope.devis, sdevis:$scope.sub_devis})
          .then(function(r){
            console.log('result',r);
            if (r.data.res == 'done!')
              location.assign("?p=devis/add3&projet="+$scope.sub_devis.num_devis);
            else
              $scope.msg_error = r.data.pdo.message;
          });
      });
  }
  /////////////////////

  // LOAD TACHE ///////
  $http.get('api/?list=tache&s=num_tache,nom_tache,um,deb_sec')
    .then(function(res){
      $scope.tache = res.data;
      console.log('tache',$scope.tache);
    })
  ;
  /////////////////////




/*

ooooooooo.   oooooooooooo   .oooooo.         .o.       ooooooooo.
`888   `Y88. `888'     `8  d8P'  `Y8b       .888.      `888   `Y88.
 888   .d88'  888         888              .8"888.      888   .d88'
 888ooo88P'   888oooo8    888             .8' `888.     888ooo88P'
 888`88b.     888    "    888            .88ooo8888.    888
 888  `88b.   888       o `88b    ooo   .8'     `888.   888
o888o  o888o o888ooooood8  `Y8bood8P'  o88o     o8888o o888o

*/


  $scope.recap = function(fresh){

    var sdv = $scope.sub_devis;
        sdv.lot           = [];
        sdv.nlot          = 0;
        sdv.tlot          = 0;
        sdv.slot          = [];
        sdv.nslot         = 0;
        sdv.tslot         = 0;
        sdv.nack_task     = 0;
        sdv.total_mnt_sec = 0;  // Z mnt * Qte
        sdv.total_mnt     = 0;  // Z mnt * Qte * KV
        sdv.tva           = parseFloat( sdv.tva || 0 );

        // nLine             : 0,
        // nArticle          : null,
        // designation       : null,
        // um_client         : null,
        // matching          : null,
        // num_tache         : null,
        // um_cosider        : null,
        // deb_sec           : 0,
        // qte               : 0,
        // mnt_sec           : 0,
        // kv                : 0,
        // mnt               : 0,
        // lot               : null

    var tmp = {
        nlot:0,
        lot:null,
        tlot:0,
        nslot:0,
        slot:null,
        tslot:0
    };

    $scope.msg_error_recap = [];

    var last_index_lot;
    var close_lot = 0;
    var last_index_slot;
    var close_slot = 0;

    sdv.xls.forEach(function (l) {


      switch(l.matching) {

        case 'IGNORER':
          break;

        case 'LOT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = null;
          l.lot           = l.matching;
          
          tmp.nlot       += 1;
          tmp.lot         = (l.nArticle?l.nArticle:'')+' '+(l.designation?l.designation:'');
          tmp.tlot        = 0;

          if (close_slot != 0){
            $scope.msg_error_recap.push('TOTAL SOUS LOT [ '+sdv.slot[last_index_slot].des+' ] n\'ai pas définie');
            close_slot = 0;
          }
          if (close_lot != 0){
            $scope.msg_error_recap.push('TOTAL LOT [ '+sdv.lot[last_index_lot-1].des+' ] n\'ai pas définie');
            close_lot = 0;
          }
          close_lot +=1;

          break;
          
        case 'SOUS LOT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = null;
          l.lot           = l.matching;
          
          tmp.nslot      += 1;
          tmp.slot        = (l.nArticle?l.nArticle:'')+' '+(l.designation?l.designation:'');
          tmp.tslot       = 0;

          if (close_lot == 0){
            $scope.msg_error_recap.push('À quel LOT appartient '+l.designation+'?');
          }
          if (close_slot != 0){
            $scope.msg_error_recap.push('TOTAL SOUS LOT [ '+tmp.slot+' ] nai pas définie');
            close_slot = 0;
          }
          close_slot +=1;
          break;

        case 'TOTAL LOT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(tmp.tlot);
          l.lot           = l.matching;
          sdv.tlot       += tmp.tlot;
          
          if (close_lot != 1){
            $scope.msg_error_recap.push('Nom LOT non défini pour le TOTAL LOT, [ '+l.designation+' ]');
            close_lot = 1;
          }else{
            sdv.lot.push({des:tmp.lot, mnt:tmp.tlot});
          }
          close_lot -=1;
          break;

        case 'TOTAL SOUS LOT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(tmp.tslot);
          l.lot           = l.matching;
          sdv.tslot      += tmp.tslot;
          
          if (close_slot != 1){
            $scope.msg_error_recap.push('Nom SOUS LOT, [ '+l.designation+' ] non défini');
            close_slot = 1;
          }else{
            sdv.slot.push({des:tmp.slot, mnt:tmp.tslot});
          }
          close_slot -=1;
          break;

        case 'ETUDES':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt:0);
          l.lot           = l.matching;
          break;

        case 'ETUDES ESQUISSE':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_esq:0);
          l.lot           = l.matching;
          break;

        case 'ETUDES AVANT PROJET':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_avprj:0);
          l.lot           = l.matching;
          break;

        case 'ETUDES PROJET EXECUTION':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_prjex:0);
          l.lot           = l.matching;
          break;

        case 'ETUDES PLAN DE RECOLLEMENT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_plrec:0);
          l.lot           = l.matching;
          break;

        case 'ETUDES PERMIS DE CONSTRUIRE':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_permis:0);
          l.lot           = l.matching;
          break;

        case 'INSTALLATION CHANTIER':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = parseFloat(sdv.installation_CH || 0);
          l.lot           = l.matching;
          sdv.total_mnt  += l.mnt;
          break;

        case 'NOTIFICATION LOT':
          l.lot           = l.matching;
          break;

        case 'NOTIFICATION TACHE':
          l.lot           = l.matching;
          break;

        case 'TOTAL ETUDES HT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0);
          l.lot           = l.matching;
          break;

        case 'TOTAL ETUDES TVA':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) * .19;
          l.lot           = l.matching;
          break;

        case 'TOTAL ETUDES TTC':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) + ((sdv.etudes?sdv.etudes.mnt:0) * .19);
          l.lot           = l.matching;
          break;

        case 'TOTAL ETUDES ET REALISATION HT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) + sdv.total_mnt;
          l.lot           = l.matching;
          break;

        case 'TOTAL ETUDES ET REALISATION TVA':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = ((sdv.etudes?sdv.etudes.mnt:0) * .19) + ((sdv.etudes?sdv.total_mnt:0) * sdv.tva);
          l.lot           = l.matching;
          break;

        case 'TOTAL ETUDES ET REALISATION TTC':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) + ((sdv.etudes?sdv.etudes.mnt:0) * .19) + sdv.total_mnt + (sdv.total_mnt * sdv.tva);
          l.lot           = l.matching;
          break;

        case 'TOTAL REALISATION HT':
          l.deb_sec       = null;
          l.kv            = null;

          l.mnt           = sdv.total_mnt;
          l.lot           = l.matching;
          break;

        default:
          l.deb_sec       = parseFloat( l.deb_sec || 0 );
          l.qte           = parseFloat( l.qte || 0 );
          l.mnt_sec       = l.deb_sec * l.qte
          l.kv            = 1;
          l.mnt           = l.mnt_sec * l.kv;
          l.lot           = tmp.lot;

          tmp.tlot       += l.mnt;
          tmp.tslot      += l.mnt;
          sdv.total_mnt_sec += l.mnt_sec;
          sdv.total_mnt  += l.mnt;

          if ( !l.matching || l.matching == "" ){
            sdv.nack_task++;
          }

          if ( close_lot == 0 && close_slot == 0 && l.matching && l.matching.indexOf("TOTAL") ){
            $scope.msg_error_recap.push('Tâche [ '+l.designation+' ] n\'appartenant pas à un LOT ou un SOUS LOT');
          }

      }
    });

    //$scope.sub_devis.format = angular.copy(svd);
    console.log($scope.sub_devis);

    if (!fresh)
      $('.ui.modal.recap').modal('show');
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////


  $scope.recap0 = function(fresh){

    $scope.sub_devis.lot           = [];
    $scope.sub_devis.nbr_lot       = 0;
    $scope.sub_devis.total_lot     = 0;
    $scope.sub_devis.slot          = [];
    $scope.sub_devis.nbr_slot      = 0;
    $scope.sub_devis.total_slot    = 0;
    $scope.sub_devis.nack_task     = 0;
    $scope.sub_devis.total_deb_sec = 0;
    $scope.sub_devis.total_mnt     = 0;

    $scope.msg_error_recap = [];

    var last_index_lot;
    var close_lot = 0;
    var last_index_slot;
    var close_slot = 0;
    var ref_lot    = null;
    var des_lot    = null;

    if ($scope.sub_devis.xls)
    $scope.sub_devis.xls.forEach(function (z) {
      //console.log('z ',z);

      if (z.matching == 'LOT'){
        console.log('lot '+ z.designation);
        last_index_lot = $scope.sub_devis.lot.push({ref:z.nArticle, des:z.designation, total:0}) -1;
        $scope.sub_devis.nbr_lot     += 1;
        $scope.sub_devis.total_lot    = 0;
        z.deb_sec                     = null;
        z.mnt                         = "===============";
        //z.ref_lot                     = z.nArticle;
        //z.des_lot                     = z.designation;
        ref_lot                       = z.nArticle;
        des_lot                       = z.designation;

        if (close_slot != 0){
          $scope.msg_error_recap.push('TOTAL SOUS LOT [ '+$scope.sub_devis.slot[last_index_slot].des+' ] n\'ai pas définie');
          close_slot = 0;
        }
        if (close_lot != 0){
          $scope.msg_error_recap.push('TOTAL LOT [ '+$scope.sub_devis.lot[last_index_lot-1].des+' ] n\'ai pas définie');
          close_lot = 0;
        }
        close_lot +=1;

      }else if (z.matching == 'SOUS LOT'){
        console.log('sous lot '+ z.designation);
        last_index_slot = $scope.sub_devis.slot.push({ref:z.nArticle, des:z.designation, total:0}) -1;
        $scope.sub_devis.nbr_slot    += 1;
        $scope.sub_devis.total_slot   = 0;
        z.deb_sec                       = null;
        z.mnt                           = "===============";

        if (close_lot == 0){
          $scope.msg_error_recap.push('À quel LOT appartient '+z.designation+'?');
        }
        if (close_slot != 0){
          $scope.msg_error_recap.push('TOTAL SOUS LOT [ '+$scope.sub_devis.slot[last_index_slot -1].des+' ] nai pas définie');
          close_slot = 0;
        }
        close_slot +=1;

      }else if (z.matching == 'TOTAL LOT'){
        console.log('total lot '+ z.designation);
        
        z.deb_sec = null;
        z.mnt     = $scope.sub_devis.total_lot;
        ref_lot   = null;
        des_lot   = null;

        if (close_lot != 1){
          $scope.msg_error_recap.push('Nom LOT non défini pour le TOTAL LOT, [ '+z.designation+' ]');
          close_lot = 1;
        }else{
          $scope.sub_devis.lot[last_index_lot].total = angular.copy($scope.sub_devis.total_lot);
        }
        close_lot -=1;

      }else if (z.matching == 'TOTAL SOUS LOT'){
        console.log('total sous lot '+ z.designation);
        
        z.deb_sec = null;
        z.mnt     = $scope.sub_devis.total_slot;

        if (close_slot != 1){
          $scope.msg_error_recap.push('Nom SOUS LOT, [ '+z.designation+' ] non défini');
          close_slot = 1;
        }else{
          $scope.sub_devis.slot[last_index_slot].total = angular.copy($scope.sub_devis.total_slot);
        }
        close_slot -=1;

      }else if (
        z.matching == 'ETUDES' ||
        z.matching == 'ETUDES ESQUISSE' ||
        z.matching == 'ETUDES AVANT PROJET' ||
        z.matching == 'ETUDES PROJET EXECUTION' ||
        z.matching == 'ETUDES PLAN DE RECOLLEMENT' ||
        z.matching == 'ETUDES PERMIS DE CONSTRUIRE'
      ){
      


      }else if (
        z.matching != "IGNORER" &&
        z.matching != "NOTIFICATION TACHE" &&
        z.matching != "NOTIFICATION LOT" &&
        z.matching != "TOTAL ETUDES HT" &&
        z.matching != "TOTAL ETUDES TVA" &&
        z.matching != "TOTAL ETUDES TTC" &&
        z.matching != "TOTAL ETUDES ET REALISATION HT" &&
        z.matching != "TOTAL ETUDES ET REALISATION TVA" &&
        z.matching != "TOTAL ETUDES ET REALISATION TTC"

// IGNORER
// LOT
// TOTAL LOT
// SOUS LOT
// TOTAL SOUS LOT
// COMPOSITION TACHE
// INSTALLATION CHANTIER
// NOTIFICATION TACHE
// NOTIFICATION LOT
// ETUDES
// ETUDES ESQUISSE
// ETUDES AVANT PROJET
// ETUDES PROJET EXECUTION
// ETUDES PLAN DE RECOLLEMENT
// ETUDES PERMIS DE CONSTRUIRE
// TOTAL ETUDES HT
// TOTAL ETUDES TVA
// TOTAL ETUDES TTC
// TOTAL REALISATION HT
// TOTAL REALISATION TVA
// TOTAL REALISATION TTC
// TOTAL ETUDES ET REALISATION HT
// TOTAL ETUDES ET REALISATION TVA
// TOTAL ETUDES ET REALISATION TTC

      ){
        
        console.log('+mnt');

        z.qte                           = parseFloat( z.qte || 0 );
        z.deb_sec                       = parseFloat( z.deb_sec || 0 );
        z.mnt                           = z.qte * z.deb_sec;
        z.ref_lot                       = ref_lot;
        z.des_lot                       = des_lot;
        $scope.sub_devis.total_lot     += z.mnt;
        $scope.sub_devis.total_slot    += z.mnt;
        $scope.sub_devis.total_deb_sec += z.deb_sec;
        $scope.sub_devis.total_mnt     += z.mnt;


        if ( !z.matching || z.matching == "" ){
          $scope.sub_devis.nack_task++;
        }

        if ( close_lot == 0 && close_slot == 0 && z.matching.indexOf("TOTAL") ){
          $scope.msg_error_recap.push('Tâche [ '+z.designation+' ] n\'appartenant pas à un LOT ou un SOUS LOT');
        }
      }else{
        z.deb_sec                         = null;
        z.mnt                             = null;
      }

    });

    
    if (!fresh)
      $('.ui.modal.recap').modal('show');
  }



/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

  $scope.auto = function(){
    $scope.sub_devis.xls
      .forEach(function(z){
        $scope.auto_loading = 'loading';
        var ai0=null ,ai1 = null;
        var v = {};
        $scope.tache.forEach(function(e){
          if (z.nArticle !== null && z.designation !== null)
          {
            ai0 = natural.JaroWinklerDistance(e.nom_tache,z.designation);
            if (ai0>ai1){
              ai1=ai0;
              v.num_tache = e.num_tache;
              v.nom_tache = e.nom_tache;
              v.deb_sec   = e.deb_sec;
            }
          }
        });
        if (ai1 > 0.73)
        {
          z.num_tache = v.num_tache;
          z.matching  = v.nom_tache;
          z.deb_sec   = v.deb_sec;
          z.mnt       = z.qte * z.deb_sec;
          console.log(ai1,':',z.designation, " = ", z.num_tache,'/',z.matching);
        }
        $scope.auto_loading = '';
      });
  }
  
  $scope.init = function(){
    $scope.sub_devis.xls.forEach(function(e){
      e.num_tache = null;
      e.matching  = null;
      e.deb_sec   = null;
      e.mnt       = null;
    });
  }

  $scope.clr_this = function(){
    if (this.ele.matching != 'COMPOSITION TACHE')
    {
      this.ele.num_tache = null;
      this.ele.matching  = null;
      this.ele.deb_sec   = null;
      this.ele.mnt       = null;
    }else{
      $scope.compose = this.$index;
      console.log('composition ',$('.ui.modal.composition').modal('show'));
    }
  }




/*

 .oooooo..o                                        oooo
d8P'    `Y8                                        `888
Y88bo.       .ooooo.   .oooo.   oooo d8b  .ooooo.   888 .oo.
 `"Y8888o.  d88' `88b `P  )88b  `888""8P d88' `"Y8  888P"Y88b
     `"Y88b 888ooo888  .oP"888   888     888        888   888
oo     .d8P 888    .o d8(  888   888     888   .o8  888   888
8""88888P'  `Y8bod8P' `Y888""8o d888b    `Y8bod8P' o888o o888o


*/


  $scope.search_complete = function(){
    console.log(this);
    if (this.ele.matching.toUpperCase() == 'COMPOSITION TACHE')
    {
      $scope.compose = this.$index;
      
      if (!$scope.sub_devis.comp_mnt)
        $scope.sub_devis.comp_mnt = [];
      
      if (!$scope.sub_devis.comp_mnt[$scope.compose])
        $scope.sub_devis.comp_mnt[$scope.compose] = 0;
      
      if (!$scope.sub_devis.comp)
        $scope.sub_devis.comp=[];
      
      if (!$scope.sub_devis.comp[$scope.compose])
        $scope.sub_devis.comp[$scope.compose] = [];
      
      if ($scope.sub_devis.comp[$scope.compose].length == 0)
        $scope.ele_comp();

      console.log('composition ',$('.ui.modal.composition').modal('show'));

    }
    else if (this.ele.matching.toUpperCase() == 'NOTIFICATION TACHE')
    {
      // compose mail
      //$scope.mail = {to:'CET', obj:'Tâche non Trouvée "'+this.ele.nArticle+' '+this.ele.designation+'"' ,msg:null };
      //$('.ui.modal.mail').modal('show');
    }
    else
    {
      var srh = $filter('filter')($scope.tache, {nom_tache: this.ele.matching }, true);
      //console.log(this.ele.des, srh);
      if (srh.length == 1)
      {
        console.log(srh);
        this.ele.num_tache = srh[0].num_tache;
        this.ele.um_cos    = srh[0].um;
        this.ele.deb_sec   = parseFloat(srh[0].deb_sec); //  * parseFloat(this.ele.qte);

      }else{
        this.ele.num_tache = null;
        this.ele.um_cos    = null;
        this.ele.deb_sec   = 0;

      }
    }
    $scope.recap(true);
  }






  $scope.fileNameChanged = function (ele) {
    if (ele.files[0].name)
      $scope.fileIsReady = 'Charger le fichier';
    else
      $scope.fileIsReady = false;

    $scope.$apply(); //   <- this to refraich attach buttom color to red 
    console.log($scope.fileIsReady);
  }

  $scope.ele_comp=function(){
    $scope.sub_devis.comp[$scope.compose].push(angular.copy(th_cmp));
  }

  $scope.calc_comp=function(){
    $scope.sub_devis.comp_mnt[$scope.compose] = 0;
    $scope.sub_devis.comp[$scope.compose].forEach( function(element, index) {
      $scope.sub_devis.comp_mnt[$scope.compose] += element.mnt; 
    });
  }

  $scope.srh_comp = function(){
    console.log(this);
    var srh = $filter('filter')($scope.tache, {nom_tache: this.ele.tch }, true);
    if (srh.length == 1)
    {
      console.log(srh);
      this.ele.pru = srh[0].deb_sec;
      this.ele.qte = 0;
      this.ele.mnt = 0;
    }else{
      this.ele.pru = 0;
      this.ele.qte = 0;
      this.ele.mnt = 0;
    }
  }

  $scope.del_comp = function(){
    console.log($scope.sub_devis.comp[$scope.compose]);
    $scope.sub_devis.comp[$scope.compose].splice(this.$index, 1);
  }

  $scope.set_comp = function(){
    $scope.sub_devis.xls[$scope.compose].deb_sec = $scope.sub_devis.comp_mnt[$scope.compose];
    $scope.recap(true);
  }



  $scope.exportToPrint = function(obj){
    console.log('print ',obj);
    var html = '';
    html += '<!DOCTYPE html><html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><link rel="stylesheet" href="css/semantic-ui/semantic.min.css"><link rel="stylesheet" href="css/print.css"></head><body>';
    html += '<div style="border-bottom:solid 2px #000;width:100%;padding:.2em 0;height:80px;"><img src="img/logo.svg" style="float:left;height:70px;"><img src="img/iso.svg" style="float:right;height:70px;"></div><p></p>';
    html += $(obj).html();
    html += '<script>print();close();<\/script></body></html>';

    w=window.open();
    w.document.write(html);
  }

  $scope.chr = function(v){
    return String.fromCharCode(65 + v);
  }

  $scope.dbg=function(){console.log($scope);$http.post('api/?draft=tmp&save',$scope.devis)}

}); </script>