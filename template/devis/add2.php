<?php // DEVIS CONTITATIF

$num_sub_devis = sql_inj($_GET['projet'],'');
list($num_devis,$diff) = explode('-', $num_sub_devis);

if (!$num_sub_devis){
  $_GET['err']='Erreur 404';
  $_GET['msg']='Projet non trouver';
  include("template/err.php");
  die();
}

/*$target_file = null;
if(isset($_POST["submit"])){
  $target_file = uniqid();
  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "./uploads/xls/".$target_file.".xlsx")){
    // update prj -> fichier_excel
    $fw->fetchAll("UPDATE devis SET fichier_excel = '$target_file' WHERE num_devis = '$num_sub_devis'");
  }else{
    $target_file = null;
  }
}*/
?>

<?=HEADER_PAGE?>


<div ng-controller="TodoCtrl" ng-cloak>
  
  <?php if (isallow("programmer")) echo DEBUG_BUTTON;?>

  <div class="ui fixed bottom sticky">
    <div class="ui image label" >
      <i class="hotjar icon"></i>
      {{sub_devis.num_devis}}
      <div class="detail">{{sub_devis.obj_devis}}</div>
    </div>
  </div>
  <div class="ui attached small steps print_ignore">
    <a class="step" href="?p=devis/add1&projet=<?=$num_sub_devis;?>">
      <i class="id card icon"></i>
      <div class="content">
        <div class="title">Projet</div>
        <div class="description">Informations du Projet</div>
      </div>
    </a>
    <a class="active red_border step" href="?p=devis/add2&projet=<?=$num_sub_devis;?>">
      <i class="file excel icon"></i>
      <div class="content">
        <div class="title">Devis Quantitatif EXCEL</div>
        <div class="description">Analyse du Fichier Client</div>
      </div>
    </a>
    <a class="step" href="?p=devis/add3&projet=<?=$num_sub_devis;?>" ng-class="{disabled:!sub_devis.xls}">
      <i class="info icon"></i>
      <div class="content">
        <div class="title">Définition des frais</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step"  href="?p=devis/add5&projet=<?=$num_sub_devis?>" ng-class="{disabled:!sub_devis.dv_etudes}">
      <i class="codepen icon"></i>
      <div class="content">
        <div class="title">ETUDES</div>
        <div class="description"></div>
      </div>
    </a>
    <a class="step" href="?p=devis/add4&projet=<?=$num_sub_devis;?>" ng-class="{disabled:!sub_devis.xls}">
      <i class="calculator icon"></i>
      <div class="content">
        <div class="title">Estimation du DEVIS</div>
        <div class="description"></div>
      </div>
    </a>
  </div>

<!-- <div class="ui red icon button print_ignore" ng-click="dbg()"><i class="code icon"></i></div>
 -->

  <datalist id="tache_list">
    <option value="IGNORER"></option>
    <option value="LOT"></option>
    <option value="TOTAL LOT"></option>
    <option value="SOUS LOT"></option>
    <option value="TOTAL SOUS LOT"></option>
    <option value="COMPOSITION TACHE"></option>
    <option value="ESTIMATION DES REGARDS"></option>
    <option value="INSTALLATION CHANTIER"></option>
    <option value="NOTIFICATION TACHE"></option>
    <option value="NOTIFICATION LOT"></option>
    <option value="ETUDES"></option>
    <option value="ETUDES ESQUISSE"></option>
    <option value="ETUDES AVANT PROJET"></option>
    <option value="ETUDES PROJET EXECUTION"></option>
    <option value="ETUDES PLAN DE RECOLLEMENT"></option>
    <option value="ETUDES PERMIS DE CONSTRUIRE"></option>
    <option value="TOTAL ETUDES HT"></option>
    <option value="TOTAL ETUDES TVA"></option>
    <option value="TOTAL ETUDES TTC"></option>
    <option value="TOTAL REALISATION HT"></option>
    <option value="TOTAL REALISATION TVA"></option>
    <option value="TOTAL REALISATION TTC"></option>
    <option value="TOTAL ETUDES ET REALISATION HT"></option>
    <option value="TOTAL ETUDES ET REALISATION TVA"></option>
    <option value="TOTAL ETUDES ET REALISATION TTC"></option>
    <option ng-repeat="ele in tache_list" value="{{ele.nom_tache}}">{{ele.num_tache}}</option>
  </datalist>
<!--div ng-click='debug_()' class='ui youtube button' ng-class='save.loading'><i class="bug icon"></i> View $scope.devis</div-->


  <div class="ui mini test modal maj">
    <div class="ui header">
      <i class="file excel outline icon"></i>
      Devis Quantitatif EXCEL
    </div>
    <div class="content center ">
      <p>Voulez vous récuperer l'ancien matching sur le nouveau devis ?</p>
    </div>
    <div class="actions">
      <div class="ui red cancel button">
        <i class="remove icon"></i>
        Non
      </div>
      <div class="ui green ok button">
        <i class="checkmark icon"></i>
        Oui
      </div>
    </div>
  </div>


  <div class="ui olive raised segment print_ignore" ng-cloak>
    
      
      <div class="ui active inverted dimmer" ng-show="loading">
        <div class="ui text loader">Chargement de fichier</div>
      </div>

      <div style="border: 1px dashed #ebb; padding: 10px">
        <label class="ui red button" tabindex="0" for="file">
            <i class="paperclip icon"></i>
            Fichier attaché
        </label>
        <div class="ui tag label" ng-show="sub_devis.xls_file"><a href="uploads/xls/{{sub_devis.xls_file}}.xlsx">{{sub_devis.xls_file}}.xlsx <i class="icon file excel"></i></a></div>
      </div>
      
      <input  type="file"
              id="file"
              name="file"
              ng-model="file"
              onchange="angular.element(this).scope().uploadXlsx(this)"
              accept=".xlsx"
              style="visibility: hidden;" 
              >

      <div class='ui basic right aligned segment' ng-show="sub_devis.xls">
        <div ng-click="composeMail()" class='ui button' >
          <i class="envelope outline icon"></i> Envoi Notifications</div>
        <div onclick="$('.show_db').modal('show');" class='ui button' >
          <i class="book icon"></i> Consultation BDD</div>
        <div ng-click='auto()' class='ui button' ng-class='auto_loading' >
          <i class="tasks icon"></i> Correspondance Automatique</div>
        <div ng-click='init()' class='ui button' >
          <i class="remove icon"></i> Effacer</div>
        <!--div ng-click='save()' class='ui button' ng-class='save.loading' >
          <i class="save  icon"></i> Enregistrer brouillon</div-->
        <div ng-click='recap()' class='ui button teal' ng-class='{disabled:!sub_devis.xls}' >
          <i class="arrow right icon"></i> Suivant</div>
      </div>

  </div>

  <table class='ui selectable compact celled table print_ignore' ng-show="sub_devis.xls.length" ng-cloak>
    <thead><tr>
      <th style="background-color: #ccc" title="{{chr(sub_devis.header.ref)}}">N</th>
      <th style="background-color: #ccc" title="{{chr(sub_devis.header.des)}}" width='50%'>Designation</th>
      <th style="background-color: #ccc" title="{{chr(sub_devis.header.um)}}">Um</th>
      <th style="background-color: #ccc" title="{{chr(sub_devis.header.qte)}}" class="right aligned">Qte</th>
      <th style="background-color: #ccc">Code tache</th>
      <th style="background-color: #ccc">Um</th>
      <th style="background-color: #ccc" width='50%'>Correspondance</th>
      <th style="background-color: #ccc">Coût U Sec</th>
      <th style="background-color: #ccc" title="{{chr(sub_devis.header.mnt)}}">Montant</th>
    </tr></thead>
    <tbody>
      <tr ng-repeat="ele in sub_devis.xls" ng-init="$last && initReadyToSave();"
          ng-class="{
            negative:   (!ele.nArticle && !ele.matching),

            bold:         ele.matching == 'LOT' ||
                          ele.matching == 'SOUS LOT' ||
                          ele.matching == 'IGNORER' ||
                          ele.matching == 'TOTAL LOT' ||
                          ele.matching == 'TOTAL SOUS LOT' ||
                          ele.matching == 'TOTAL REALISATION HT',


            warning:      ele.matching == 'LOT' ||
                          ele.matching == 'SOUS LOT' ||
                          ele.matching == 'IGNORER',

            positive:     ele.matching == 'TOTAL LOT' ||
                          ele.matching == 'TOTAL SOUS LOT' ||
                          ele.matching == 'TOTAL REALISATION HT',

            yellow_bg:    ele.matching == 'NOTIFICATION LOT' ||
                          ele.matching == 'NOTIFICATION TACHE',

            print_ignore: ele.matching == 'IGNORER'


          }"
          ng-cloak>
        <td>{{ele.nArticle}}</td>
        <td>{{ele.designation}}</td>
        <td>{{ele.um}}</td>
        <td style="border-right: 3px solid #f33" class="right aligned"><div ng-if="ele.nArticle || ele.qte">{{ele.qte | number:2}}</div></td>
        <td style="border-left: 3px solid #f33" ng-dblclick="tache_view(ele.num_tache)">{{ele.num_tache}}</div></td>
        <td>{{ele.um_cos}}</div></td>
        <td style="padding:0; margin:0; position:relative; min-height: 80px;">
            <input  type='text'
                    list='tache_list'
                    ng-model='ele.matching'
                    ng-change='search_complete()'
                    ng-dblclick='dbl_this()'
                    style="
                      position:absolute;
                      top:0px;
                      height:100%;
                      width: 100%;
                      padding: 10px;
                      border:0;
                      background-color: transparent;
                    "
                    class="focus_border"
                    >
          <!--div class="ui transparent fluid input">
          </div-->
        </td>
        <td class="right aligned">
            <element ng-if="ele.matching != 'NOTIFICATION TACHE' && ele.um_cos == 'M3'">{{ele.deb_sec | number:3}}</element>
            <element ng-if="ele.matching != 'NOTIFICATION TACHE' && ele.um_cos != 'M3'">{{ele.deb_sec | number:2}}</element>
            <div     ng-if="ele.matching == 'NOTIFICATION TACHE'" class="ui input">
              <input type='number' ng-model="ele.deb_sec" ng-blur="recap(true)" step="any" toNumber>
            </div>
        </td>
        <td class="right aligned">{{ele.mnt | number:2}}</td>
      </tr>
    <tbody>
  </table>




  <div class="ui raised very padded container piled segment print_ignore"  ng-show="sub_devis.xls.length" id="recap_G" ng-cloak>

    <button class="ui print_ignore right floated mini icon button" style="z-index: 1;" 
            data-inverted=""
            data-position="bottom right"
            data-tooltip="Imprimer ..."
            ng-click="printElement('#recap_G',1)"><i class="print icon"></i>  IMPRIMER
    </button>


    <div class="ui basic segment">
      PROJET : <b>{{sub_devis.num_devis}} / {{sub_devis.nom_devis}}</b> <br>
      Objet : <b>{{sub_devis.obj_devis}}</b> <br>
    </div>
    <div class="ui center aligned basic segment">
      <h1>RECAP</h1>
    </div>

    <table class="ui striped celled table" ng-cloak>
      <thead>
        <tr>
          <th class="left aligned">DESIGNATION</th>
          <th class="right aligned">MONTANT</th>
        </tr>
      </thead>

        <tr ng-repeat="ele in sub_devis.lot">
          <td class="left aligned"><h4>{{ele.des}}</h4></td>
          <td class="right aligned"><h4>{{ ele.mnt | number:2 }}</h4></td>
        </tr>

        <tr>
          <td class="left aligned"><h4>MONTANT RÉALISATION EN HT</h4></td>
          <td class="right aligned"><h4>{{ sub_devis.total_mnt_sec | number:2}}</h4></td>
        </tr>

    </table>

  </div>






<!--
ooooooooooooo            .o8       oooo                 .oooooo.       .            oooo
8'   888   `8           "888       `888                d8P'  `Y8b    .o8            `888
     888       .oooo.    888oooo.   888   .ooooo.     888          .o888oo oooo d8b  888
     888      `P  )88b   d88' `88b  888  d88' `88b    888            888   `888""8P  888
     888       .oP"888   888   888  888  888ooo888    888            888    888      888
     888      d8(  888   888   888  888  888    .o    `88b    ooo    888 .  888      888
    o888o     `Y888""8o  `Y8bod8P' o888o `Y8bod8P'     `Y8bood8P'    "888" d888b    o888o
-->

  <div class="ui modal recap">
    <div class="header">Tableau de Contrôle </div>
    <div class="scrolling content">
      <ul class="ui list">
        <li><b>[ {{sub_devis.lot.length}} ]</b> LOT
          <ul>
            <li ng-repeat="ele in sub_devis.lot">{{ele.ref}}, {{ele.des}}</li>
          </ul>
        </li>
        <li><b>[ {{sub_devis.slot.length}} ]</b> SOUS LOT
          <ul>
            <li ng-repeat="ele in sub_devis.slot">{{ele.des}}</li>
          </ul>
        </li>
        <!--li><b>{{sub_devis.total_deb_sec | number:2}}</b> DEBOURSÉ SEC</li-->

        <div ng-show="msg_error_recap.length">
          <hr class="style3"></hr>
          <ul style="color: #F00;">
            <li ng-repeat="ele in msg_error_recap">{{ele}}</li>
          </ul>
        </div>
        <div ng-show="sub_devis.nack_task">
          <ul style="color: #F00;">
            <li><b>[ {{sub_devis.nack_task}} ] Element n'a pas été défini !!!</b></li>
          </ul>
        </div>



      </ul>
    </div>
    <div class="actions">
      <div class="negative ui button">Annuler</div>
      <div class="positive ui button" ng-click='next()'>Suivant</div>
    </div>
  </div>

<!--
  .oooooo.                                                              o8o      .    o8o
 d8P'  `Y8b                                                             `"'    .o8    `"'
888           .ooooo.  ooo. .oo.  .oo.   oo.ooooo.   .ooooo.   .oooo.o oooo  .o888oo oooo   .ooooo.  ooo. .oo.
888          d88' `88b `888P"Y88bP"Y88b   888' `88b d88' `88b d88(  "8 `888    888   `888  d88' `88b `888P"Y88b
888          888   888  888   888   888   888   888 888   888 `"Y88b.   888    888    888  888   888  888   888
`88b    ooo  888   888  888   888   888   888   888 888   888 o.  )88b  888    888 .  888  888   888  888   888
 `Y8bood8P'  `Y8bod8P' o888o o888o o888o  888bod8P' `Y8bod8P' 8""888P' o888o   "888" o888o `Y8bod8P' o888o o888o
                                          888
                                         o888o

-->

  <div class="ui modal composition" id="composition">
    <div class="ui header">
      <div class="print_ignore">Créer une Composition</div>
      <div class="print_only">Composition d'une Tache </div>
      <div class="sub header">
        {{sub_devis.xls[compose].nArticle}}, {{sub_devis.xls[compose].designation}}
      </div>
    </div>
    <div class="ui scrolling content form">

      <table class="ui very compact fixed single line celled table">
        <thead>
          <tr>
            <th width="50%">Tache</th>
            <th>Prix U</th>
            <th>Qte</th>
            <th width="25%">Montant SEC</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="ele in sub_devis.comp[compose]">
            <td >
              <div class="ui transparent fluid input print_ignore">
                <i class="link icon red remove print_ignore" ng-click="del_comp()"></i>
                <input type="text" class="" ng-model="ele.tch" ng-change="srh_comp()" list="tache_list" >
              </div>
              <div class="print_only">{{ele.tch}}</div>
            </td>
            <td class="right aligned">
              {{ele.pru | number:2}}
            </td>
            <td class="center aligned">
              <div class="ui transparent fluid input print_ignore">
                <input type="number" class="" step="any" ng-model="ele.qte" size="4" ng-change="ele.mnt = ele.pru * ele.qte; calc_comp(compose)">
              </div>
              <div class="print_only">{{ele.qte}}</div>
            </td>
            <td class="right aligned">
              {{ele.mnt | number:2}}
            </td>
          </tr>
        </tbody>
        <tfoot>
            <th colspan="3"><b>Montant de cette composition</b></th>
            <th class="right aligned">
              <div class="ui tiny statistic"><div class="value">{{sub_devis.comp_mnt[compose] | number:2}}</div></div>
            </th>
          </tr>
        </tfoot>
      </table>

      <div class="ui right floated mini icon button print_ignore" ng-click="ele_comp()">
        <i class='add icon'></i>
      </div>
      
    </div>
    <div class="actions print_ignore">

      <div class="ui icon button " ng-click="copy('COMPOSITION TACHE')" data-tooltip="Copy"><i class='copy icon'></i></div>
      <div class="ui icon button " ng-click="paste('COMPOSITION TACHE')" data-tooltip="Coller"><i class='paste icon'></i></div>
      <div class="ui icon button" ng-click="exportToPrint('#composition')" data-tooltip="Imprimer"><i class='print icon'></i></div>



      <div class="positive ui button" ng-click="set_comp()"><i class='check icon'></i> Appliquer</div>
    </div>
  </div>

<!--

Dislay TACHE

-->

  <div class="ui modal show_db">
    <div class="header">BANQUE DE DONNEES</div>
    <div class="ui content form">
      <div class="field">
        <div class="ui fluid input">
          <input type="text" placeholder="Recherche..." ng-model="fdb">
        </div>
      </div>
      <table class="ui very basic compact table">
        <thead>
          <tr>
            <th>REF</th>
            <th>DESIGNATION</th>
            <th>COUT SEC</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="ele in tache_list | filter:fdb">
            <td>{{ele.num_tache}}</td>
            <td>{{ele.nom_tache}}</td>
            <td align="right">{{ele.deb_sec}}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="ui modal ext_view">
    <div class="content" id="result"></div>
  </div>


<!--
ooo        ooooo  .oooooo..o   .oooooo.
`88.       .888' d8P'    `Y8  d8P'  `Y8b
 888b     d'888  Y88bo.      888
 8 Y88. .P  888   `"Y8888o.  888
 8  `888'   888       `"Y88b 888     ooooo
 8    Y     888  oo     .d8P `88.    .88'
o8o        o888o 8""88888P'   `Y8bood8P'

-->

  <div class="ui modal mail">
    <div class="header">Notifier une Tâche</div>
    <div class="ui scrolling content form">

      <div class="field">
        <select class="ui dropdown fluid" ng-model="mail.to">
          <option value='BAT'>BAT - BATIMENT</option>
          <option value='GC'>GC - GENIE CIVIL</option>
          <option value='DPI'>DPI - DÉPARTEMENT DE PRODUCTION ET INFRASTRUCTURE</option>
          <option value='CET'>C.E.T - CORPS D'ETAT TECHNIQUES</option>
          <option value='TC'>T.C - TECHNIQUO COMMERCIALE</option>
          <?php foreach ($fw->fetchAll("SELECT id_user, CONCAT(utilisateur.nom,' ',utilisateur.pnom) as fullname FROM utilisateur WHERE acl LIKE '%\"enable\":true%'") as $user){
            echo "<option value='$user->id_user'>$user->fullname</option>";
          } ?>
        </select>


      </div>

      <div class="field">
        <div class="ui fluid input">
          <input type="text" placeholder="Objet..." ng-model="mail.subject">
        </div>
      </div>

      <div class="field">
        <textarea ng-model="mail.content" placeholder="Message ..."></textarea>
      </div>
      
    </div>
    <div class="actions">
      <div class="ui blue labeled submit icon button" ng-click="sendMail()" ng-class="{disabled:!mail.to||!mail.subject||!mail.content}">
        <i class="icon edit"></i> Envoyer
      </div>
    </div>
  </div>


  <div class="ui basic modal alert">
    <div class="ui icon header">
      <i class="exclamation triangle icon"></i>
      Fichier non conforme
    </div>
    <div class="content">
      <p style="font-size: 20px;">
        Veuillez verifier le format de fichier XLSX et l'en tête des colone <u>designation</u>, <u>qte</u>, <u>montant</u>, <u>prix unitaire</u> et <u>um</u> unité de mesure doit etre avant Qte !
      </p>
    </div>
    <div class="actions">
      <div class="ui green ok inverted button">
        <i class="checkmark icon"></i>
        Ok
      </div>
    </div>
  </div>


<?=FOOTER_PAGE?>

</div>

<!--
   oooo  .oooooo..o
   `888 d8P'    `Y8
    888 Y88bo.
    888  `"Y8888o.
    888      `"Y88b
    888 oo     .d8P
.o. 88P 8""88888P'
`Y888P

-->
<script language="javascript"> app.controller('TodoCtrl', function($scope, $interval, $filter, $http, $templateRequest, $sce, $compile) {
  $scope.Math = window.Math;
  
  $scope.tache_view = function(num_tache){
    $http.get('./api/?tache_view='+num_tache).then(function(res){
      $scope.tache = res.data;
      var templateUrl = $sce.getTrustedResourceUrl('template/tache_view.html');
      $templateRequest(templateUrl).then(function(template) {
        $compile($("#result").html(template).contents())($scope);
        $('.ui.modal.ext_view').modal('show');  
      });
    });
  }

  $scope.calc_cas_niveau = function(){
    var templateUrl = $sce.getTrustedResourceUrl('template/cas_niveau.html');
    $templateRequest(templateUrl).then(function(template) {
      $scope.est_reg = {};
      $compile($("#result").html(template).contents())($scope);
      $('.ui.modal.ext_view')
        .modal({
          onVisible : function(){
            $scope.est_reg = angular.copy($scope.sub_devis.regard[$scope.regard]);
          }
        })
        .modal('show')
      ;

    });
  }

  $scope.set_cas_niveau = function(){
    $scope.sub_devis.regard[$scope.regard] = angular.copy($scope.est_reg);
    $scope.sub_devis.xls[$scope.regard].est_reg = angular.copy($scope.est_reg);
    $scope.sub_devis.xls[$scope.regard].deb_sec = $scope.sub_devis.regard[$scope.regard].deb_sec;
    $('.ui.modal.ext_view').modal('hide');
    $scope.recap(true);
  }


  $scope.loading = 3;
  var th_cmp = {
    tch:null,
    des:null,
    pru:0,
    qte:0,
    mnt:0
  }

  // upload file whene input change 
  $scope.loadXlsx = function(flXlsx){
    $http.get("api/?xlsx="+flXlsx)
      .then(function(res){
        console.log("get xlsx",res);
        if (res.data.length == 0 || res.data.xls.length == 0){
          $scope.loading = 0;
          //alert("Fichier Excel non valide, ou non reconue!");
          $('.ui.basic.modal.alert').modal('show');
          return false;
        }

        if (!$scope.sub_devis){
          $scope.sub_devis = {};  
        }

        if ($scope.sub_devis.xls){
          $scope.loading += 1;
          let old_matching = angular.copy($scope.sub_devis.xls);
          $scope.sub_devis.xls      = res.data.xls;
          $('.modal.maj')
            .modal({
              closable  : false,
              onDeny    : function(){
                $scope.loading -= 1;
              },
              onApprove : function() {
                console.log('old_matching',old_matching);
                let el;
                
                $scope.sub_devis.xls.forEach(function(element, index){
                  if (element.designation){
                    el = old_matching.find( f => f.designation === element.designation);
                    if (el){
                      console.log('find ',element.designation, el);
                      element.matching  = el.matching;
                      element.num_tache = el.num_tache;
                      element.um_cos    = el.um_cos;
                      element.deb_sec   = el.deb_sec;
                    }
                  }

                    

                });

                $scope.save();
                $scope.loading -= 1;
              }
            })
            .modal('show');
        }else{
          $scope.sub_devis.xls      = res.data.xls;
        }

        $scope.sub_devis.query      = 'update';
        $scope.sub_devis.xls_file   = flXlsx;
        $scope.sub_devis.header     = res.data.header;
        $scope.sub_devis.bpu        = res.data.bpu;
        $scope.sub_devis.SheetNames = res.data.listSheetNames;
        console.log('Load xls',$scope.sub_devis);
        $scope.recap(true);
        $scope.sub_devis.comp       =[];
        $scope.sub_devis.comp_mnt   =[];
        $scope.loading -= 1;
        return true;
      });
  }

  $scope.uploadXlsx = function(el){
    $scope.loading =1 ;
    var fileData = new FormData();
    fileData.append('file', el.files[0]);

    if (el.files[0] === undefined)
      return false;

    //console.log(fileData);
    $http.post('api/?upload=xls',fileData, {
      headers: {
        'Content-Type': undefined
      },
      transformResponse: angular.identity
    })
    .then(function(r){
      upFile = JSON.parse(r.data);
      console.log(upFile);

      if (upFile.filename && upFile.filename != '' && $scope.loadXlsx(upFile.filename))
        $http.post('api/?update=step2&xls='+upFile.filename,{sdevis:{num_devis:'<?=$num_sub_devis?>'}});
    });
  }

  
  // LOAD PRJ //////////
  $http.get('api/?draft=<?=$num_devis?>&load')
    .then(function(res){
      $scope.loading -= 1;
      $scope.devis = res.data;
      console.log("Load Devis <?=$num_devis?>", $scope.devis);
      //$scope.recap(true);

    });

  // LOAD SUB PRJ //////////
  $http.get("api/?draft=<?=$num_sub_devis?>&load")
    .then(function(res){
      $scope.loading -= 1;
      $scope.sub_devis = res.data;
      console.log('>> ',res);
    });


/*
  ooo        ooooo       .o.       ooooo ooooo
  `88.       .888'      .888.      `888' `888'
   888b     d'888      .8"888.      888   888
   8 Y88. .P  888     .8' `888.     888   888
   8  `888'   888    .88ooo8888.    888   888
   8    Y     888   .8'     `888.   888   888       o
  o8o        o888o o88o     o8888o o888o o888ooooood8

*/

  $scope.composeMail = function(){

    var subject='Tâche inexistante dans la Banque de Données.';
    var content='';
    var l='';
    var tmpmail='';

    $scope.sub_devis.xls.forEach(function (z) {
      l = (z.nArticle?z.nArticle:'')+' '+(z.designation?z.designation:'');
      if (z.matching == 'LOT'){
        tmpmail = '### '+l+' :  \r';
      }else if (z.matching == 'NOTIFICATION LOT'){
        content += tmpmail+'___  \r';
      }else if (z.matching == 'NOTIFICATION TACHE'){
        content += '* '+l+'\r___  \r';
      }else{
        tmpmail += '* '+l+'  \r';
      }
    });

    $scope.mail = {to:'TC', subject:subject ,content:content };
    $('.ui.modal.mail').modal('show');
  }

  $scope.sendMail = function(){
    $http.post('api/?topics',$scope.mail)
      .then(function(){
        console.log('mail sent', $scope.mail)
        $('.ui.modal.mail').modal('hide');
        $scope.mail = null;
      })
    ;
  }


  // SAVE /////////////
  $scope.save = function(){
    return $http.post('api/?draft=<?=$num_sub_devis?>&save',$scope.sub_devis)
      .then(function(res){
        console.log('Save '+$scope.sub_devis.num_devis);
      });
  }

  // NEXT /////////////
  $scope.next = function(){
    $scope.save()
      .then(function(){
        $http.post('api/?update=step2',{devis:$scope.devis, sdevis:$scope.sub_devis})
          .then(function(r){
            console.log('result',r);
            if (r.data.res == 'done!')
              location.assign("?p=devis/add3&projet="+$scope.sub_devis.num_devis);
            else
              $scope.msg_error = r.data.pdo.message;
          });
      });
  }
  /////////////////////

  // LOAD TACHE ///////
  //$http.get('api/?list=tache&s=num_tache,nom_tache,um,deb_sec')
  $http.get('api/?tache_flist')
    .then(function(res){
      res.data.forEach(function(e){
        e.deb_sec = parseFloat(e.deb_sec);
      })
      $scope.tache_list = res.data;
      console.log('tache',$scope.tache_list);

      $scope.loading -= 1;
    })
  ;
  /////////////////////




/*

ooooooooo.   oooooooooooo   .oooooo.         .o.       ooooooooo.
`888   `Y88. `888'     `8  d8P'  `Y8b       .888.      `888   `Y88.
 888   .d88'  888         888              .8"888.      888   .d88'
 888ooo88P'   888oooo8    888             .8' `888.     888ooo88P'
 888`88b.     888    "    888            .88ooo8888.    888
 888  `88b.   888       o `88b    ooo   .8'     `888.   888
o888o  o888o o888ooooood8  `Y8bood8P'  o88o     o8888o o888o

*/


  $scope.recap = function(fresh){
    if (!$scope.sub_devis)$scope.sub_devis={};
    var sdv = $scope.sub_devis;
        sdv.lot           = [];
        sdv.nlot          = 0;
        sdv.tlot          = 0;
        sdv.slot          = [];
        sdv.nslot         = 0;
        sdv.tslot         = 0;
        sdv.nack_task     = 0;
        sdv.total_mnt_sec = 0;  // Z mnt * Qte
        sdv.total_mnt     = 0;  // Z mnt * Qte * KV
        sdv.tva           = parseFloat( sdv.tva || 0 );

        // nLine             : 0,
        // nArticle          : null,
        // designation       : null,
        // um_client         : null,
        // matching          : null,
        // num_tache         : null,
        // um_cosider        : null,
        // deb_sec           : 0,
        // qte               : 0,
        // mnt_sec           : 0,
        // kv                : 0,
        // mnt               : 0,
        // lot               : null

    var tmp = {
        nlot:0,
        lot:null,
        tlot:0,
        nslot:0,
        slot:null,
        tslot:0
    };

    $scope.msg_error_recap = [];

    var last_index_lot;
    var close_lot = 0;
    var last_index_slot;
    var close_slot = 0;



    sdv.xls.forEach(function (l) {


      switch(l.matching) {

        case 'IGNORER':
          break;

        case 'LOT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = null;
          l.lot           = l.matching;
          
          tmp.nlot       += 1;
          tmp.lot         = (l.nArticle?l.nArticle:'')+' '+(l.designation?l.designation:'');
          tmp.tlot        = 0;

          if (close_slot > 0){
            $scope.msg_error_recap.push('TOTAL SOUS LOT [ '+sdv.slot[last_index_slot].des+' ] n\'ai pas définie');
            close_slot = 0;
          }




          if (close_lot > 0){
          console.log('close_lot',close_lot);
          console.log('lot',sdv.lot);
          console.log('last_index_lot',last_index_lot);
            
            //let last_lot = '';
            //if (sdv.lot && (last_index_lot-1)>=0 && sdv.lot[last_index_lot-1] && sdv.lot[last_index_lot-1].des)
            //  last_lot = '['+sdv.lot[last_index_lot-1].des+'] ';
            
            $scope.msg_error_recap.push('TOTAL LOT ['+sdv.lot[last_index_lot-1].des+'] n\'ai pas définie');
            close_lot = 0;
          }
          close_lot +=1;

          break;
          
        case 'SOUS LOT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = null;
          l.lot           = l.matching;
          
          tmp.nslot      += 1;
          tmp.slot        = (l.nArticle?l.nArticle:'')+' '+(l.designation?l.designation:'');
          tmp.tslot       = 0;

          if (close_lot == 0){
            $scope.msg_error_recap.push('À quel LOT appartient '+l.designation+'?');
          }
          if (close_slot != 0){
            $scope.msg_error_recap.push('TOTAL SOUS LOT [ '+tmp.slot+' ] nai pas définie');
            close_slot = 0;
          }
          close_slot +=1;
          break;

        case 'TOTAL LOT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(tmp.tlot);
          l.lot           = l.matching;
          sdv.tlot       += tmp.tlot;
          
          if (close_lot != 1){
            $scope.msg_error_recap.push('Nom LOT non défini pour le TOTAL LOT, [ '+l.designation+' ]');
            close_lot = 1;
          }else{
            last_index_lot = sdv.lot.push({des:tmp.lot, mnt:tmp.tlot});
          }
          close_lot -=1;
          break;

        case 'TOTAL SOUS LOT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(tmp.tslot);
          l.lot           = l.matching;
          sdv.tslot      += tmp.tslot;
          
          if (close_slot != 1){
            $scope.msg_error_recap.push('Nom SOUS LOT, [ '+l.designation+' ] non défini');
            close_slot = 1;
          }else{
            last_index_slot = sdv.slot.push({des:tmp.slot, mnt:tmp.tslot});
          }
          close_slot -=1;
          break;

        case 'ETUDES':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt:0);
          l.lot           = l.matching;
          break;

        case 'ETUDES ESQUISSE':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_esq:0);
          l.lot           = l.matching;
          break;

        case 'ETUDES AVANT PROJET':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_avprj:0);
          l.lot           = l.matching;
          break;

        case 'ETUDES PROJET EXECUTION':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_prjex:0);
          l.lot           = l.matching;
          break;

        case 'ETUDES PLAN DE RECOLLEMENT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_plrec:0);
          l.lot           = l.matching;
          break;

        case 'ETUDES PERMIS DE CONSTRUIRE':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = angular.copy(sdv.etudes?sdv.etudes.mnt_permis:0);
          l.lot           = l.matching;
          break;

        case 'INSTALLATION CHANTIER':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = parseFloat(sdv.installation_CH || 0);
          l.lot           = l.matching;
          sdv.total_mnt  += l.mnt;
          break;

        case 'NOTIFICATION LOT':
          l.lot           = l.matching;
          break;

        //case 'NOTIFICATION TACHE':
        //  l.lot           = l.matching;
        //  break;

        case 'TOTAL ETUDES HT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0);
          l.lot           = l.matching;
          break;

        case 'TOTAL ETUDES TVA':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) * .19;
          l.lot           = l.matching;
          break;

        case 'TOTAL ETUDES TTC':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) + ((sdv.etudes?sdv.etudes.mnt:0) * .19);
          l.lot           = l.matching;
          break;

        case 'TOTAL ETUDES ET REALISATION HT':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) + sdv.total_mnt;
          l.lot           = l.matching;
          break;

        case 'TOTAL ETUDES ET REALISATION TVA':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = ((sdv.etudes?sdv.etudes.mnt:0) * .19) + ((sdv.etudes?sdv.total_mnt:0) * sdv.tva);
          l.lot           = l.matching;
          break;

        case 'TOTAL ETUDES ET REALISATION TTC':
          l.deb_sec       = null;
          l.kv            = null;
          l.mnt           = (sdv.etudes?sdv.etudes.mnt:0) + ((sdv.etudes?sdv.etudes.mnt:0) * .19) + sdv.total_mnt + (sdv.total_mnt * sdv.tva);
          l.lot           = l.matching;
          break;

        case 'TOTAL REALISATION HT':
          l.deb_sec       = null;
          l.kv            = null;

          l.mnt           = sdv.total_mnt;
          l.lot           = l.matching;
          break;

        default:
          l.deb_sec       = parseFloat( l.deb_sec || 0 );
          l.qte           = parseFloat( l.qte || 0 );
          l.mnt_sec       = l.deb_sec * l.qte
          l.kv            = 1;
          l.mnt           = l.mnt_sec * l.kv;
          l.lot           = tmp.lot;

          tmp.tlot       += l.mnt;
          tmp.tslot      += l.mnt;
          sdv.total_mnt_sec += l.mnt_sec;
          sdv.total_mnt  += l.mnt;

          if ( !l.matching || l.matching == "" ){
            sdv.nack_task++;
          }

          if ( close_lot == 0 && close_slot == 0 && l.matching && l.matching.indexOf("TOTAL") ){
            $scope.msg_error_recap.push('Tâche [ '+l.designation+' ] n\'appartenant pas à un LOT ou un SOUS LOT');
          }

      }
    });

    //$scope.sub_devis.format = angular.copy(svd);
    console.log($scope.sub_devis);

    if (!fresh)
      $('.ui.modal.recap').modal('show');


  }


/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

  $scope.auto = function(type_ai){
    $scope.sub_devis.xls
      .forEach(function(z){
        $scope.auto_loading = 'loading';
        var ai0=null ,ai1 = null;
        var v = {};
        $scope.tache_list.forEach(function(e){
          if (z.nArticle !== null && z.designation !== null)
          {
            //ai0 = natural.JaroWinklerDistance(e.nom_tache,z.designation);
            ai0 = distance(e.nom_tache,z.designation);
            
            if (ai0>ai1){
              ai1=ai0;
              v.num_tache = e.num_tache;
              v.um_cos    = e.um;
              v.nom_tache = e.nom_tache;
              v.deb_sec   = e.deb_sec;
            }
          }
        });
        if (ai1 > 0.73)
        {
          z.num_tache = v.num_tache;
          z.um_cos    = v.um_cos;
          z.matching  = v.nom_tache;
          z.deb_sec   = v.deb_sec;
          z.mnt       = z.qte * z.deb_sec;
          console.log(z);
          //console.log(ai1,':',z.designation, " = ", z.num_tache,'/',z.matching);
        }
        $scope.auto_loading = '';
      });
  }
  
  $scope.init = function(){
    $scope.sub_devis.xls.forEach(function(e){
      e.num_tache = null;
      e.um_cos    = null;
      e.matching  = null;
      e.deb_sec   = null;
      e.mnt       = null;
    });
  }

  $scope.dbl_this = function(){
    if (this.ele.matching == 'COMPOSITION TACHE')
    {
      $scope.compose = this.$index;
      $('.ui.modal.composition').modal('show');

    }else if (this.ele.matching == 'ESTIMATION DES REGARDS'){
      $scope.regard = this.$index;
      $scope.calc_cas_niveau();

    }else{
      //this.ele.num_tache = null;
      //this.ele.um_cos    = null;
      //this.ele.matching  = null;
      //this.ele.deb_sec   = null;
      //this.ele.mnt       = null;
    }
  }




/*

 .oooooo..o                                        oooo
d8P'    `Y8                                        `888
Y88bo.       .ooooo.   .oooo.   oooo d8b  .ooooo.   888 .oo.
 `"Y8888o.  d88' `88b `P  )88b  `888""8P d88' `"Y8  888P"Y88b
     `"Y88b 888ooo888  .oP"888   888     888        888   888
oo     .d8P 888    .o d8(  888   888     888   .o8  888   888
8""88888P'  `Y8bod8P' `Y888""8o d888b    `Y8bod8P' o888o o888o


*/


  $scope.search_complete = function(){
    console.log(this);
    if (this.ele.matching.toUpperCase() == 'COMPOSITION TACHE')
    {
      $scope.compose = this.$index;
      
      if (!$scope.sub_devis.comp_mnt)
        $scope.sub_devis.comp_mnt = [];
      
      if (!$scope.sub_devis.comp_mnt[$scope.compose])
        $scope.sub_devis.comp_mnt[$scope.compose] = 0;
      
      if (!$scope.sub_devis.comp)
        $scope.sub_devis.comp=[];
      
      if (!$scope.sub_devis.comp[$scope.compose])
        $scope.sub_devis.comp[$scope.compose] = [];
      
      if ($scope.sub_devis.comp[$scope.compose].length == 0)
        $scope.ele_comp();

      $('.ui.modal.composition').modal('show');
      console.log('composition ');

    }
    else if (this.ele.matching.toUpperCase() == 'ESTIMATION DES REGARDS')
    {
      $scope.regard = this.$index;

      if (!$scope.sub_devis.regard)
        $scope.sub_devis.regard = [];
      
      if (!$scope.sub_devis.regard[$scope.regard])
        $scope.sub_devis.regard[$scope.regard] = {
          ratioAcier: 0,
          X: 0,
          Y: 0,
          H: 0,
          epV: 0,
          epB: 0,
          epT: 0,
          prB: 0,
          prA: 0,
          prC: 0,
          deb_sec: 0
        };
              
      $scope.calc_cas_niveau();
    }
    else if (this.ele.matching.toUpperCase() == 'NOTIFICATION TACHE')
    {
      // compose mail
      //$scope.mail = {to:'CET', obj:'Tâche non Trouvée "'+this.ele.nArticle+' '+this.ele.designation+'"' ,msg:null };
      //$('.ui.modal.mail').modal('show');
    }
    else
    {
      var srh = $filter('filter')($scope.tache_list, {nom_tache: this.ele.matching }, true);
      //console.log(this.ele.des, srh);
      if (srh.length == 1)
      {
        console.log(srh);
        this.ele.num_tache = srh[0].num_tache;
        this.ele.um_cos    = srh[0].um;
        this.ele.deb_sec   = parseFloat(srh[0].deb_sec); //  * parseFloat(this.ele.qte);

      }else{
        this.ele.num_tache = null;
        this.ele.um_cos    = null;
        this.ele.deb_sec   = 0;

      }
    }
    $scope.recap(true);
  }






  $scope.fileNameChanged = function (ele) {
    if (ele.files[0].name)
      $scope.fileIsReady = 'Charger le fichier';
    else
      $scope.fileIsReady = false;

    $scope.$apply(); //   <- this to refraich attach buttom color to red 
    console.log($scope.fileIsReady);
  }

  $scope.ele_comp=function(){
    $scope.sub_devis.comp[$scope.compose].push(angular.copy(th_cmp));
  }

  $scope.calc_comp=function(){
    $scope.sub_devis.comp_mnt[$scope.compose] = 0;
    $scope.sub_devis.comp[$scope.compose].forEach( function(element, index) {
      $scope.sub_devis.comp_mnt[$scope.compose] += element.mnt; 
    });
  }

  $scope.srh_comp = function(){
    console.log(this);
    var srh = $filter('filter')($scope.tache_list, {nom_tache: this.ele.tch }, true);
    if (srh.length == 1)
    {
      console.log(srh);
      this.ele.pru = srh[0].deb_sec;
      this.ele.qte = 1;
      this.ele.mnt = this.ele.pru * this.ele.qte;
      $scope.calc_comp();
    }else{
      this.ele.pru = 0;
      this.ele.qte = 0;
      this.ele.mnt = 0;
    }
  }

  $scope.del_comp = function(){
    console.log($scope.sub_devis.comp[$scope.compose]);
    if (confirm("Voulez vous supprimer cette element ?")){
      $scope.sub_devis.comp[$scope.compose].splice(this.$index, 1);
      $scope.calc_comp();
    }
  }

  $scope.set_comp = function(){
    $scope.sub_devis.xls[$scope.compose].deb_sec = $scope.sub_devis.comp_mnt[$scope.compose];
    $scope.recap(true);
  }



  $scope.exportToPrint = function(obj){
    console.log('print ',obj);
    var html = '';
    html += '<!DOCTYPE html><html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><link rel="stylesheet" href="css/semantic-ui/semantic.min.css"><link rel="stylesheet" href="css/print.css"></head><body>';
    html += '<div style="border-bottom:solid 2px #000;width:100%;padding:.2em 0;height:80px;"><img src="img/logo.svg" style="float:left;height:70px;"><img src="img/iso.svg" style="float:right;height:70px;"></div><p></p>';
    html += $(obj).html();
    html += '<script>print();close();<\/script></body></html>';

    w=window.open();
    w.document.write(html);
  }

  $scope.chr = function(v){
    return String.fromCharCode(65 + v);
  }

  $scope.initReadyToSave = function(){
    var timeoutId;
    //$scope.loading = 0;
    //console.log('test');



    $('input').blur(function () {
        if (timeoutId) clearTimeout(timeoutId);
        timeoutId = setTimeout(function () {
            console.log('auto save');
            $scope.save();
        }, 1000);
    });
  }

  $scope.printElement = function(obj,phf){
    if (phf){
      $('html').css('padding','100px 0 0 0');
      $('.PAPER_HEADER').removeClass('print_ignore');
    }
    
    $(obj).removeClass('print_ignore');
    window.print();
    $(obj).addClass('print_ignore');
    $('html').css('padding','0');
    $('.PAPER_HEADER').addClass('print_ignore');
  }

  $scope.copy = function (ele) {
    var ele_val = null;
    if (ele == 'COMPOSITION TACHE')
      ele_val = angular.copy($scope.sub_devis.comp[$scope.compose]);
    else if (ele == 'ESTIMATION DES REGARDS')
      ele_val = angular.copy($scope.sub_devis.regard[$scope.regard]);

    if (ele_val){
      localStorage.setItem(ele, JSON.stringify(ele_val));
      toastr.options.extendedTimeOut = 3000;
      toastr.options.timeOut = 3000;
      toastr.options.onclick = null;
      toastr.options.positionClass = "toast-top-right";
      toastr["info"](
        ele+" à ete copyer."
      );
    }
    console.log("copy ",ele,ele_val);
  }

  $scope.paste = function (ele) {
    var ele_val = localStorage.getItem(ele);
    if (ele == 'COMPOSITION TACHE'){
      $scope.sub_devis.comp[$scope.compose] = JSON.parse(ele_val);
      $scope.calc_comp($scope.compose);

    }else if (ele == 'ESTIMATION DES REGARDS'){
      $scope.est_reg = JSON.parse(ele_val);
      
    }


    toastr.options.extendedTimeOut = 3000;
    toastr.options.timeOut = 3000;
    toastr.options.onclick = null;
    toastr.options.positionClass = "toast-top-right";
    toastr["info"](
      ele+" à ete Coller."
    );
    console.log("paste ",ele,ele_val);
  }

  $scope.dbg=function(){console.log($scope);$http.post('api/?draft=tmp&save',$scope.devis)}

}); </script>