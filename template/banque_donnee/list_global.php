<div ng-controller="TodoCtrl" >

<div class="ui borderless blue inverted menu print_ignore">
  <a class="item" ng-click="printList()">
    <i class="print icon"></i>&nbsp;
    IMPRIMER LES TACHE
  </a>
  <a class="item" ng-click="printElement('#all_list')">
    <i class="print icon"></i>&nbsp;
    IMPRIMER LA LIST
  </a>
  <a class="right item">
    <div class="ui loading mini search inline" ng-class="{loading:!loaded}">
      <div class="ui icon input">
        <input class="prompt" type="text" placeholder="Recherche..." ng-model="srh">
        <i class="search icon"></i>
      </div>
      <div class="results"></div>
    </div>
  </a>
</div>




  <!--div class="ui clearing basic  segment print_ignore">
    <div class="ui loading search inline" ng-class="{loading:!loaded}">
      <div class="ui icon input">
        <input class="prompt" type="text" placeholder="Recherche..." ng-model="srh">
        <i class="search icon"></i>
      </div>
    </div>
    <div class="ui right floated  button" ng-click="printList()">
      <i class="print icon"></i> IMPRIMER LES TACHE
    </div>
    <div class="ui right floated  button" ng-click="printElement('#all_list')">
      <i class="print icon"></i> IMPRIMER LA LIST
    </div>
  </div-->

  <div id="all_list">
    <table class='ui olive selectable striped very compact table' style="page-break-inside: always;">
      
      <thead>
        <tr>
          <th class="print_ignore">
            <i class="icon link" ng-class="{check:!tog_checkbox, times:tog_checkbox}" ng-click="tog_all()">
          </th>
          <!--th ng-click="searchby_('num_tache')" class="link"><i class="filter icon" ng-if="searchby__=='num_tache'"></i> Code</th>
          <th ng-click="searchby_('nom_tache')" class="link"><i class="filter icon" ng-if="searchby__=='nom_tache'"></i> Designation </th-->
          <th >Code</th>
          <th >Designation</th>
          <th class="center aligned">UM</th>
          <th class="center aligned">TU</th>
          <th class="right  aligned">Capacité Journaliere</th>
          <th class="right  aligned">Cout/H MO</th>
          <th class="right  aligned">Cout MO</th>
          <th class="right  aligned">Cout/H Materiel</th>
          <th class="right  aligned">Cout Materiel</th>
          <th class="right  aligned">Prix Fourn</th>
          <th class="right  aligned">Debourse Sec</th>
          <th>
            <div class="ui scrolling dropdown">
              <input type="hidden" name="slot">
              <div class="default text">Lot</div>
              <div class="menu">
                <div class="item" data-value="" ng-click="selectSlot('')">Lot</div>
                <div class="item" ng-repeat="(key,tache) in tache_list|groupBy:'num_slot'" ng-click="selectSlot(key)">{{key}}</div>
              </div>
            </div>
          </th>
          <th>
            <div class="ui scrolling dropdown">
              <input type="hidden" name="slot">
              <div class="default text">B.D</div>
              <div class="menu">
                <div class="item" data-value="" ng-click="selectLot('')">B.D</div>
                <div class="item" ng-repeat="(key,tache) in tache_list|groupBy:'num_lot'" ng-click="selectLot(key)">{{key}}</div>
              </div>
            </div>
          </th>
          <th class="print_ignore" ></th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="tache in tache_list | filter:{nom_tache:srh}| filter:{num_lot:lot}| filter:{num_slot:slot}"
            ng-init="$last && loaded_ok()"
            ng-class="{'print_ignore': !tache.checked}"
            >
            
          <td class="print_ignore" >

            <i class="icon" ng-class="{'olive check':tache.checked,'grey genderless':!tache.checked}" ng-click="tache.checked=!tache.checked"></i>

          </td>
          <td class="uppercase"><a ng-click="tache_view(tache.num_tache,true)" href=""><b>{{tache.num_tache}}</b></a></td>
          <td>{{tache.nom_tache}}</td>
          <td class="center aligned">{{tache.um}}</td>
          <td class="center aligned">{{tache.tmp_u    | number:2}}</td>
          <td class="right aligned">{{tache.vol_h_mo  | number:2}}</td>
          <td class="right aligned">{{tache.prx_h_mo  | number:2}}</td>
          <td class="right aligned">{{tache.cout_mo   | number:2}}</td>
          <td class="right aligned">{{tache.prx_h_mat | number:2}}</td>
          <td class="right aligned">{{tache.cout_mat  | number:2}}</td>
          <td class="right aligned">{{tache.prx_fourn | number:2}}</td>
          <td class="right aligned">{{tache.deb_sec   | number:2}}</td>
          <td class="uppercase">{{tache.num_slot}}</td>
          <td class="uppercase">{{tache.num_lot}}</td>
          <td class="print_ignore" ><button class="ui mini icon button" ng-class="{loading:tache.loading}" ng-click="tache_view(tache.num_tache, true)"><i class="icon eye"></i></button></td>
        </tr>
      </tbody>
    </table>
  </div>




  <!--  Sous detail tache  -->
  <div class="ui modal tache_view">
    <div class="content" id="result"></div>
  </div>

</div>

<!--  Java Script -->
<script language="javascript"> app.controller('TodoCtrl', function($scope, $http, $templateRequest, $sce, $compile, $filter, $timeout) {

  const html_header = '<!DOCTYPE html><html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><link rel="stylesheet" href="css/semantic-ui/semantic.min.css"><link rel="stylesheet" href="css/print.css"></head><body>';
  const html_footer = '<script>print();close();<\/script></body></html>';


  // LOAD FLATE TACHE LIST ///////
  $http.get('api/?get=3d442f')
    .then(function(res){
      $scope.tache_list = res.data;
    });

  // LOAD FULL DETAILLE OF TACHE ///////
  $http.get('api/?tache_view')
    .then(function(res){
      $scope.tache_det = res.data;
      $scope.listTache=[$scope.tache_det[0]];
    });

  // LOAD TEMPLATE TACHE ///////
  var templateUrl = $sce.getTrustedResourceUrl('template/tache_view.html');
  $templateRequest(templateUrl).then(function(template) {
      $compile($("#result").html('<div ng-repeat="tache in listTache">'+template+'</div>').contents())($scope);
  });

  $scope.tache_view = function(num_tache, view){
    var srh = $filter('filter')($scope.tache_det, {num_tache: num_tache }, true);
    $scope.listTache=[srh[0]];
    $('.ui.modal.tache_view').modal('show');
  }

  $scope.toggle = function () {
    this.tache.checked = !this.tache.checked;
  }

  $scope.tog_all = function () {
    $scope.loaded = false;
      $scope.tog_checkbox = !$scope.tog_checkbox;
      // | filter:srh| filter:{num_lot:lot}| filter:{num_slot:slot}
      var selected_list = $filter('filter')($scope.tache_list, {nom_tache:$scope.srh,num_lot:$scope.lot, num_slot:$scope.slot});
      console.log(selected_list);

      selected_list.forEach(element => {
        element.checked = $scope.tog_checkbox;
      });
      $scope.loaded = true;
  }
       
  $scope.printElement = function(obj,phf){
    console.log('print one',obj);
    w=window.open();
    w.document.write(html_header);
    w.document.write($(obj).html());
    w.document.write(html_footer);
  }

  $scope.printList = function () {
    console.log('print list');
    
    $scope.listTache =[];
    $scope.tache_list.forEach(element => {
      if (element.checked){
        let srh = $filter('filter')($scope.tache_det, {num_tache: element.num_tache }, true);
        $scope.listTache.push(srh[0]);
      }
    });
    
    $timeout(function(){
      $scope.$apply()
    })
    .then(function(){

      w=window.open();
      w.document.write(html_header);
      w.document.write($("#result").html());
      w.document.write(html_footer);
    });
    
  }

  $scope.searchby_ = function(container) {
    $scope.searchby__ = container;        
  };
  $scope.searchby_('nom_tache');

  $scope.loaded_ok = function(){
    $scope.loaded = true;
    $('.ui.checkbox').checkbox();
  }
  $scope.selectLot = function(container) {
    $scope.lot = container;        
  };
  $scope.selectSlot = function(container) {
    $scope.slot = container;        
  };

}); </script>