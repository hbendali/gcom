<div class="ui raised very padded text container piled red segment">
  <h2 class="ui header">Ajouter un LOT</h2> 

  <form method="post" action="ctrl/lot_add.php" id="traget_form" class="ui form">

    <div class="required field">
      <label>CODE LOT</label>
      <div class="ui right input">
        <input type="text"   name="num_lot" ng-model="num_lot" required>
      </div>
    </div>

    <div class="required field">
      <label>Libellé du LOT</label>
      <div class="ui right input">
        <input type="text"   name="nom_lot" ng-model="nom_lot" required>
      </div>
    </div>

    <button class="ui teal button">
      Ajouter
    </button>

    <hr class="style3"></hr>

    <div ng-init="list=[<?php
    $result = $fw->fetchAll("SELECT * FROM lot");
    foreach ($result as $ele) {
      echo "'".str_replace("'","\'",$ele->num_lot)."/".str_replace("'","\'",$ele->nom_lot)."',";
    }
    ?>]">
      <div class="ui label" ng-repeat="ele in list | filter:champs" ng-cloak>
        {{ele}}
      </div>
    </div>
  </form>
</div>