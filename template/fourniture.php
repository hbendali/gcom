<?php
  if(!isallow("admin") && !isallow("cet") && !isallow("tc") ) {
    echo "<script type='text/javascript'>location='?p=err&err=accès non autoriser&msg=Vous n\'avez pas l\'autorisation d\'accéder a cette rubrique, contactez l\'administrateur';</script>";
    exit;
  }
?>

<div ng-controller="TodoCtrl" >
  <h2>FOURNITURES</h2>

  <div class="ui action input">
    <input type="text" placeholder="Recherche ..." ng-model="srh_fourn">
    <button class="ui icon button">
      <i class="search icon"></i>
    </button>
  </div>

  <table class="ui compact selectable red table">
    <thead>
      <tr>
        <th width="5%">Code</th>
        <th width="30%">Designation</th>
        <th class="right center">UM</th>
        <th class="right center">Origine</th>
        <th class="right aligned">Prix achat</th>
        <th class="right aligned">Transport</th>
        <th class="right aligned">Dechargement</th>
        <th class="right aligned">Perte</th>
        <th class="right aligned">Cout Revient</th>
        <th>
        </th>
        <th>
          <div class='ui mini red icon button'
            data-tooltip="Ajouter une Fourniture"
            data-inverted=""
            data-position="left center"
            ng-click="detail();"
            >
            <i class='add icon'></i>
          </div>          
        </th>
      </tr>
    </thead>
    <tbody>
      <tr
        ng-repeat="l_fourn in fourniture | filter:srh_fourn"
        ng-cloak
        >
        <td style="border-left: 5px solid {{l_fourn.color || '#ffffffff'}};">{{l_fourn.code_fourniture}}</td>
        <td>{{l_fourn.libelle}}</td>


        <td>{{l_fourn.um}}</td>
        <td>{{l_fourn.origine}}</td>
        <td class="right aligned">{{l_fourn.prix_achat|number:2}}</td>
        <td class="right aligned">{{l_fourn.transport|number:2}}</td>
        <td class="right aligned">{{l_fourn.dechargement|number:2}}</td>
        <td class="right aligned">{{l_fourn.perte|number:2}}</td>
        <td class="right aligned">{{l_fourn.cout_revient|number:2}}</td>
        <td class="right aligned">
          <div
            class="ui mini basic icon button"
            data-tooltip="Supprimer {{l_fourn.code_fourniture}}"
            data-inverted=""
            data-position="left center"
            ng-click="delete(this.l_fourn);"
            >
            <i class='trash icon'></i>
          </div>
        </td>
        <td class="right aligned">
          <div
            class="ui mini basic icon button"
            data-tooltip="Modifier {{l_fourn.code_fourniture}}"
            data-inverted=""
            data-position="left center"
            ng-click="detail(this.l_fourn);"
            >
            <i class='write icon'></i>
          </div>
        </td>
      </tr>
    </tbody>
  </table>





  <!-- Ajouter / Modifier Fourniture -->
  <form method="post" action="" autocomplete="off">
    <div class="ui longer modal Fourniture">
      <div class="header">
        {{comp_fourn.titel}} FOURNITURE
      </div>

      <div class="ui form scrolling content">
        
        <div class="equal width fields">

          <div class="three wide field" ng-class="{'error':!comp_fourn.code_fourniture}">
            <label>Code</label>
            <input type="text" ng-model="comp_fourn.code_fourniture" class="uppercase">
          </div>

          <div class="field" ng-class="{'error':!comp_fourn.libelle}">
            <label>Designation</label>
            <input type="text" ng-model="comp_fourn.libelle">
          </div>

          <div class="six wide field" ng-class="{'error':!comp_fourn.cout_revient}">
            <label>Cout revient</label>
            <div class="ui right labeled input">
              <input
                type="number" min="0" step="0.01" toNumber
                ng-model="comp_fourn.cout_revient">
              <div class="ui basic label">
                DA
              </div>
            </div>
          </div>
          
          <div class="one wide field">
            <label>&nbsp;</label>
            <button class="ui icon button" 
                    ng-click="comp_fourn.show = !comp_fourn.show"
                    data-tooltip="Détail" data-inverted=""
                    >
              <i class="options icon"></i>
            </button>
          </div>

        </div>

        <div class="equal width fields">

          <div class="three wide field">
            <label>Famille</label>
            <input type="text" list="fam" ng-model="comp_fourn.fam">
          </div>

          <div class="field">
            <label>Origine</label>
            <input type="text" ng-model="comp_fourn.origine">
          </div>
          
          <div class="three wide field">
            <label>U Mesure</label>
            <input type="text" list="um" ng-model="comp_fourn.um">
          </div>


        </div>

        <div ng-show="comp_fourn.show">
          <hr class="style3"></hr>

          <div class="equal width fields">
            <div class="three wide field">
              <label>Prix achat</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_fourn.prix_achat">
                <div class="ui basic label">
                  DA
                </div>
              </div>
            </div>

            <div class="three wide field">
              <label>Transport</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_fourn.transport">
                <div class="ui basic label">
                  DA
                </div>
              </div>
            </div>

            <div class="three wide field">
              <label>Dechargement</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_fourn.dechargement">
                <div class="ui basic label">
                  DA
                </div>
              </div>
            </div>

            <div class="three wide field">
              <label>Perte</label>
              <div class="ui right labeled input">
                <input
                  type="number" min="0" step="0.01" toNumber
                  ng-model="comp_fourn.perte">
                <div class="ui basic label">
                  %
                </div>
              </div>
            </div>

          </div>
          <button class="ui button" ng-click="calculer()">
            <i class="calculator icon"></i>
            Calculer
          </button>

        </div>
      </div>

      <div class="actions">
        <div
          class="ui deny icon button"
          ng-show="comp_fourn.titel=='Modifier'"
          ng-click="delete(comp_fourn)"
          ><i class="trash icon"></i>
        </div>
        <div class="ui deny button">
          Annuler
        </div>
        <div
          class="ui green right button"
          ng-click="update(comp_fourn)"
          ng-disabled=""
          ng-class="{ 'disabled' : !comp_fourn.code_fourniture || !comp_fourn.libelle || !comp_fourn.cout_revient }"
          >{{comp_fourn.titel}}
        </div>
      </div>

    </div>
  </form>



<datalist id="fam"><option value="{{ele.fam}}" ng-repeat="ele in fam">{{ele.libelle}}</datalist>
<datalist id="um"><option value="{{ele.um}}" ng-repeat="ele in um"></datalist>

</div>

<script language="javascript"> app.controller('TodoCtrl', function($scope, $http) {

  $http.get('api/?list=fourniture&j=fam&w=fam&s=fourniture.*,fam.color')
    .then(function(res){
      angular.forEach(res.data, function(ele) {
        
        ele.cout_revient = parseFloat(ele.cout_revient);
        ele.prix_achat   = parseFloat(ele.prix_achat);
        ele.transport    = parseFloat(ele.transport);
        ele.dechargement = parseFloat(ele.dechargement);
        ele.perte        = parseFloat(ele.perte);

      });
      $scope.fourniture = res.data;
      console.log('fourniture',$scope.fourniture);
    });

  $http.get('api/?list=um')
    .then(function(res){
      $scope.um = res.data;
      console.log('um',$scope.um);
    });
  
  $http.get('api/?list=fam')
    .then(function(res){
      $scope.fam = res.data;
      console.log('fam',$scope.fam);
    });
  
  $scope.detail = function(fourn){
    console.log('detail',fourn);
    if (fourn)
    {
      $scope.comp_fourn = angular.copy(fourn) ;
      $scope.comp_fourn.titel = "Modifier";
    }
    else
      $scope.comp_fourn = {titel:"Ajouter", code_fourniture:null,libelle:null, cout_revient:0, origine:null, um:null, prix_achat:0, transport:0, dechargement:0, perte:0};
    $('.ui.modal.Fourniture').modal('show');
  }

  $scope.update = function(fourn){
    console.log('update',fourn);
    $http.post('api/?update=fourniture',fourn)
    .then(function(r){
      console.log('result',r);
      if (r.data.res = "done!" && r.data.pdo.sqlState == "00000")
        location.reload();
    });
  }

  $scope.delete = function(fourn){
    console.log('delete',fourn);
    if (confirm("Etre vous sure de vouloir le supprimer"))
    {
      $http.post('api/?delete=fourniture&w=code_fourniture&e='+fourn.code_fourniture)
      .then(function(r){
        console.log('result',r);
        if (r.data.res = "done!" && r.data.pdo.sqlState == "00000")
          location.reload();
      });
    }
  }

  $scope.calculer = function(){
    var v = $scope.comp_fourn;

    var prix_achat   = v.prix_achat;
    var transport    = v.transport;
    var dechargement = v.dechargement;
    var perte        = v.perte;

    var cout_revient = parseFloat(((prix_achat+(prix_achat*(perte/100))+transport+dechargement) * 100)/100).toFixed(2);

    $scope.comp_fourn.cout_revient = cout_revient *1; // *1 <- pour converter string to integer
  }

}); </script>