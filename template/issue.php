<?php
/*
  // Lib for MARKDOWN 
  require_once('bin/parsedown.php');
  $parsedown = new Parsedown();
  $parsedown->setSafeMode(true);
  echo $parsedown->text(file_get_contents("version.md"));
*/
?>

<div class="ui container" ng-controller="TodoCtrl" >

  <a href="http://192.168.5.81:81/COSIDER.CONSTRUCTION/GCom/issues/new" target="Gitea" class="ui button green right floated">New Issue</a>
  
  <div class="ui middle aligned divided list" ng-cloak>
    <div class="item" ng-repeat="issue in issues">
      <br/>
      <div class="ui blue label left floated">#{{issue.number}}</div>
      <div class="content">
        <a href="http://192.168.5.81:81/COSIDER.CONSTRUCTION/GCom/issues/{{issue.number}}" target="Gitea">
          <div class="header" >{{issue.title}}</div>
          <div class="description">{{issue.created_at}}.</div>
        </a>
      </div>
      <br/>
    </div>
  </div>

</div>



<script language="javascript"> app.controller('TodoCtrl', function ($scope, $http, $templateCache, $sce ) {
    
  
    $http.get('api/?proxy_gitea=/repos/COSIDER.CONSTRUCTION/GCom/issues').then(
      function(r) {
        $scope.issues = r.data
      }
    );
    
    $http.get('api/?proxy_gitea=/repos/COSIDER.CONSTRUCTION/GCom/commits').then(
      function(r) {
        $scope.commits = r.data
      }
    );
    

}); </script>