<?php
// Include FireWorks lib
require_once('bin/fw.php');

if (!$fw->signupdate())
{
  header('Location: bin/login.php');
  exit;
}

$page = "template/accueil.php";
if( isset ($_GET['p']))
{
  if (($_GET['p'] != "") && (file_exists("template/$_GET[p].php")))
    $page = "template/$_GET[p].php";
  else
  {
    $_GET['err']='Erreur 404';
    $_GET['msg']='Page not found';
    $page = "template/err.php";
  }
}

?>
<!DOCTYPE HTML>
<html lang="fr-FR" dir="ltr" ng-app="App">
<head>
  <title>COSIDER/CONSTRUCTION</title>
    
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
</script>
<![endif]-->
  
  <!-- CSS -->
  <link rel="shortcut icon" href="img/cosider.ico">
  <link rel="stylesheet"    href="css/semantic-ui/semantic.css">
  <link rel="stylesheet"    href="css/default.css">
  <link rel="stylesheet"    href="css/line-awesome-font-awesome.css">
  
  <!-- JS -->
  <script src="js/jquery.min.js"></script>
  <script src="js/angular/angular.min.js"></script>
  <script src="js/angular/angular-filter.min.js"></script>
  <script src="js/angular/angular-sanitize.min.js"></script>
  <script src="js/angular/angular-resource.min.js"></script>
  <script src="js/angular/angular-animate.min.js"></script>
  <script src="js/angular/i18n/angular-locale_fr-dz.js"></script>
  <script src="js/showdown.min.js"></script>
  <script src="js/markdown.js"></script>
  <script src="js/NumberToLetter.js"></script>
  <script src="js/polyfiller.js"></script>
  <!--script src="js/natural.js"></script-->
  <script src="js/jaro-winkler.js"></script>
  <script src="css/semantic-ui/semantic.js"></script>
  
  <!-- jQuery Toastr popup notifier -->
  <link rel="stylesheet"    href="css/toastr.css">
  <script src="js/toastr.js"></script>

  <script language="javascript">
    var app = angular
    
    .module('App', ['angular.filter', 'ngSanitize', 'ngAnimate', 'btford.markdown', 'ngResource'])

    .directive('toNumber', function () {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
          ngModel.$parsers.push(function(value) {
            return '' + value;
          });
          ngModel.$formatters.push(function(value) {
            return parseFloat(value);
          });
        }
      };
    })

    .run(function($rootScope) {
      $rootScope.typeOf = function(value) {
        return typeof value;
      };
      $rootScope.toNumber = function(value) {
        return parseFloat(value || 0);
      };
    })

    .controller('main_container', function($scope, $interval, $filter, $http) {
      toastr.options = {
        "closeButton": false,
        "debug": true,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-bottom-right print_ignore",
        "preventDuplicates": true,
        "onclick": function(e){
          console.log(this);
          location.assign("?p=notification&id="+this.mail);
        },
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "300000",
        "extendedTimeOut": "300000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "slideDown",
        "iconClasses": {
            "error":        'toast-error',
            "info":         'toast-info',
            "success":      'toast-success',
            "warning":      'toast-warning',
            "mail":         'toast-mail',
            "notification": 'toast-notification'
        },
        iconClass: 'toast-notification',
      };      
      
      nbr_of_topics = 0;
      $scope.loadTopics = function(){
        $http.get('api/?topics&news')
          .then(function(res){
            $scope.list_of_posts = res.data;
            //console.log('list_of_posts',$scope.list_of_posts);

            let stored_posts_list = JSON.parse(localStorage.getItem("posts_notification"));
            if (stored_posts_list || ( stored_posts_list && JSON.stringify(stored_posts_list) !== JSON.stringify($scope.list_of_posts) ))
            {
              let new_posts = $scope.list_of_posts.length - stored_posts_list.length;

              if (new_posts > 0){
                $scope.new_posts = new_posts;
                $scope.last_posts_id = $scope.list_of_posts[0].topic_id;
                
              }else if (new_posts < 0){
                $scope.new_posts = 0;
                $scope.last_posts_id = '';
              }
              
              //localStorage.setItem("posts_notification", JSON.stringify($scope.list_of_posts));
              //toastr["mail"](
              //  "NEW MESSAGE",
              //  "FROM ",
              //  "XXX"
              //);
              
            }

            // if (!$scope.length_of_topics || $scope.list_of_topics.length>$scope.length_of_topics){
            //   $scope.length_of_topics = $scope.list_of_topics.length;
            //   toastr["mail"](
            //     $scope.list_of_topics[$scope.length_of_topics-1].topic_subject,
            //     $scope.list_of_topics[$scope.length_of_topics-1].fullname,
            //     $scope.new_topic={
            //       sender: $scope.list_of_topics[$scope.length_of_topics-1].fullname,
            //       header: $scope.list_of_topics[$scope.length_of_topics-1].topic_subject,
            //       core: "test"
            //     }
            //   );
            // }

          });
      };

      $scope.notification_read = function(){
        localStorage.setItem("posts_notification", JSON.stringify($scope.list_of_posts));
      }

      $scope.loadTopics();
      $interval(function(){
        $scope.loadTopics();
      },5000,0);


      // $http.get('api/?mail&got')
      //   .then(function(r){
      //     //console.log('mail',r);
      //     r.data.forEach( function(element, index) {
      //       toastr.options.mail = element;
      //       toastr["mail"](element.objet,element.fullname);
      //     });
      //   });

    })
    ;

    $(document).ready(function(){
      $('.ui.dropdown').dropdown();
      $('.message .close').on('click', function() {
          $(this).closest('.message').transition('fade');
        })
      ;
      $('img')
        .visibility({
          type       : 'image',
          transition : 'fade in',
          duration   : 1000
        })
      ;

      $('.ui.fixed.bottom .sticky').sticky('refresh');
      $('form,input,select,textarea').attr('autocomplete', 'off');
      $('input.autocompleteOff').attr('autocomplete','off');
    });

    function printElement(id) {
      console.log(id);
      var printHtml = document.getElementById(id).outerHTML;
      var currentPage = document.body.innerHTML;
      var elementPage = '<html><head><title></title></head><body>' + printHtml + '</body>';
      //change the body
      document.body.innerHTML = elementPage;
      //print
      window.print();
      //go back to the original
      document.body.innerHTML = currentPage;
    }

    function go(url){
      location.assign(url);
    }

    function todoDone(id){
      if (confirm('Supprimer cette tache !'))
        $.post( "api/?todo="+id, function( data ) {
          if (data.pdo.sqlState == "00000")
            location.reload();
        });
    }

    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }


  </script>

</head>

<body style="background-color: #eee" ng-controller="main_container">
  <?php include 'template/nav.php'; ?>

  <div class="ui segment basic" id="main_container">
    <?php include $page; ?>
  </div>

  <div class="ui large text loader print_ignore">Loading</div>

  <div class="ui mini modal">
    <div class="header">{{new_topic.header}}</div>
    <div class="content">
      <p>
        {{new_topic.core}}
      </p>
      <div class="">{{new_topic.sender}}</div>
    </div>
    <div class="actions">
      <div class="ui green cancel button">Ok</div>
    </div>
  </div>

  <!--iframe src="http://192.168.5.81:3000/hbendali/gcom/issues" style="width:100%; height:100%;" class="ui modal Gogs">
  </iframe-->

</body>
</html>
