<?php
  // Include FireWorks lib
  require_once('../bin/fw.php');

  if (!isset($_SESSION["user"]))
    header("location:javascript://history.go(-1)");

  $fam     = sql_inj($_POST['fam']);
  $libelle = sql_inj($_POST['libelle']);
  $color   = sql_inj($_POST['color'],"#fff");

  if($fam && $libelle)
  {
    $r = $fw->fetchAll("INSERT INTO fam (fam, libelle, color) values ('$fam','$libelle','$color')",true,true);
    if($r['errorCode'] == null)
      header("Location:../?p=fam");
    else
      header("Location:../?p=err&err=Erreur insertion une FAMILLE&msg=$r[message]");
  }
  else if ($fam)
  {
    header("Location:../?p=err&err=Erreur&msg=CODE FAMILLE non definie");
  }
  else if ($libelle)
  {
    header("Location:../?p=err&err=Erreur&msg=LIBELLE non definie");
  }
  else
  {
    header("Location:../?p=err&err=Erreur&msg=Erreur non definie");
  }



