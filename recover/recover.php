<?php 

	$csv_file	="th_mo.csv";
	$tables		="tache_mo";
	$field		=[
		"num_tache"=>0,
		"code_poste"=>1,
		"nombre"=>2,
		"amplitude"=>3
	];

/*
	$csv_file	="th_mo.csv";
	$tables		="tache_mo";
	$field		=[
		"num_tache"=>0,
		"code_poste"=>1,
		"nombre"=>3,
		"amplitude"=>4
	];
*/


	//--------------------------------
	//--------------------------------
	//--------------------------------
	echo "<pre>";
	// open csv file 
	$handle = utf8_fopen_read($csv_file);

	// skip 1st line
	$csv_line = fgetcsv($handle, 1000, ",");
	//--------------------------------
	$sql = "INSERT INTO `$tables` (";
	foreach ($field as $ky => $vl) {
		$sql .= "`$ky`,";
	}
	$sql = substr($sql, 0, -1).") VALUES\r\n";
	//--------------------------------
	while (($csv_line = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$sql .= "(";
		foreach ($field as $ky => $vl) {
			$csv_line[$vl] = format_valu($csv_line[$vl]);
			$sql .= "'$csv_line[$vl]',";
		}
		$sql = substr($sql, 0, -1)."),\r\n";
    }
	//--------------------------------
    $sql = substr($sql, 0, -3).";";
    echo $sql;
	//--------------------------------
    function utf8_fopen_read($fileName){ 
		//$fc = iconv('windows-1250', 'utf-8', file_get_contents($fileName)); 
		$fc = file_get_contents($fileName);
		$handle=fopen("php://memory", "rw"); 
		fwrite($handle, $fc); 
		fseek($handle, 0); 
		return $handle; 
	}
	//--------------------------------
	function format_valu($value){
        $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
        $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
        return str_replace($search, $replace, $value);
    }
	//--------------------------------
	//--------------------------------
	//--------------------------------