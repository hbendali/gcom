# LOGICIEL DE CALCUL DE PRIX ET ESTIMATION DES OFFRES

## Install

```
mkdir uploads \
      uploads/draft \
      uploads/repo \
      uploads/bpu \
      uploads/img \
      uploads/xls \
      uploads/tmp
chmod 777 \
      uploads \
      uploads/*
```

### setting webserver

activer l'affichage des erreur en mode de develepement

> /etc/php/php.ini

```
display_errors = On
```

augmenter la taille des fichier uploader

> /etc/nginx/nginx.conf

```
  client_max_body_size 100M;
```

> /etc/php/php.ini
```
  upload_max_filesize = 100M
```

augmenter la memoire pour le traitement des fichier excel

> /etc/php/php.ini

```
  memory_limit = 2048M
```

### Requirement and Installation

* php
* php7.0-zip pour l'utilisation de PhpSpreadsheet
* verifier le dossier uploads et les sous dossier xls, draft, bpu, img, repo.
* verifier les droit d'access des sous dossier uploads ``` chmod 777 ```
* creatée fichier de connection au bin/db-connect.php

> <?php $fw = new FireWorks ('host|username|password|bdd');

